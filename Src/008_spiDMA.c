/*
 * 008_spiDMA.c
 *
 *  Created on: 3 maj 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

SPI_Handle_t SPI2handle;
DMA_Handle_t hdma1;
uint8_t tmp = 0;
char user_data[] = "Hello world";

void SPI2_GPIOInit(void){

	GPIO_Handle_t SPIgpio;

	SPIgpio.GPIO_PinConfig.pGPIOx = GPIOB;
	SPIgpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	SPIgpio.GPIO_PinConfig.GPIO_PinAltFunMode = 5;
	SPIgpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	SPIgpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;
	SPIgpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	SPIgpio.numberOfPinsToConfig = 4;
	SPIgpio.PinNumber[0] = GPIO_PIN_15;
	SPIgpio.PinNumber[1] = GPIO_PIN_14;
	SPIgpio.PinNumber[2] = GPIO_PIN_13;
	SPIgpio.PinNumber[3] = GPIO_PIN_12;

	GPIO_Init(&SPIgpio);
}

void SPI2_Init(void){

	SPI2handle.pSPIx = SPI2;
	SPI2handle.SPIConfig.SPI_BusConfig = SPI_BUS_FULLDUPLEX;
	SPI2handle.SPIConfig.SPI_DeviceMode = SPI_DEVICE_MODE_MASTER;
	SPI2handle.SPIConfig.SPI_SclkSpeed = SPI_SPEED_DIV8; // 8 MHz SPI_SCK
	SPI2handle.SPIConfig.SPI_DataSize = SPI_DFF_8BITS;
	SPI2handle.SPIConfig.SPI_CPOL = SPI_CPOL_LOW;
	SPI2handle.SPIConfig.SPI_CPHA = SPI_CPHA_LOW;
	SPI2handle.SPIConfig.SPI_SSM = SPI_SSM_DI; // Hardware slave managment for NSS pin

	SPI_Init(&SPI2handle);
}

void DMAInit(){

	memset(&hdma1, 0, sizeof(hdma1));

	DMA_PeriphClockControl(DMA1, ENABLE);
	NVIC_IRQInterruptConfig(DMA1_Channel5_IRQn, ENABLE);
	NVIC_IRQPriorityConfig(DMA1_Channel5_IRQn, 15);

	hdma1.DMAx = DMA1;
	hdma1.DMAChannel = DMA1_Channel5;
	hdma1.dmaInterruptsEnable.transferComplete = ENABLE;
	hdma1.dmaInterruptsEnable.transferError = ENABLE;
	hdma1.transferConfig.memoryAddress = (uint32_t)user_data;
	hdma1.transferConfig.peripheralAddress = (uint32_t)&(SPI2->DR);
	hdma1.transferConfig.numberOfDataToTransfer = strlen(user_data);
	hdma1.DMA_Config.direction = DMA_DIRECTION_MEMORY_TO_PERIPHERAL;
	hdma1.DMA_Config.peripheralIncrementMode = DISABLE;
	hdma1.DMA_Config.memoryIncrementMode = ENABLE;
	hdma1.DMA_Config.transferMode = DMA_MODE_CIRCULAR;

	DMA_Init(&hdma1);

}

void delay(){

	for(uint32_t i=0; i<50000; i++);
}

int main(void){

	DMA_PeriphClockControl(DMA1, ENABLE);

	SPI2_GPIOInit();
	SPI2_Init();
	DMAInit();

	SPI_SSOEConfig(SPI2, ENABLE);

	//SPI_StartSendingDataDMA(&SPI2handle, SPI2_DMA_TX_CHANNEL);
	SPI_SendDataDMA(&SPI2handle, (uint8_t *)user_data, strlen(user_data), SPI2_DMA_TX_CHANNEL);

	return 0;
}

void DMA1_CH5_IRQHandler(void){

	DMA_IRQHandling(&hdma1);
}
void DMA_ApplicationTransferCompleteCallback(DMA_Handle_t *pDMAh){
	tmp = 1;
}
void DMA_ApplicationTransferErrorCallback(DMA_Handle_t *pDMAh){
	tmp = 2;
}
