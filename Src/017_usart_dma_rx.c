/*
 * 017_usart_dma_rx.c
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */
/*
 * 016_usart_dma.c
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

USART_Handle_t usart2;
DMA_Handle_t hdma1;

uint8_t user_data[] = "Hello world";
uint8_t received_data[5];

// For belwo pins alternate function mode: AF7
//PA2 --> USART2_TX
//PA3 --> USART2_RX


void USART2_GPIOInit(void){
	GPIO_Handle_t USART2gpio;

	USART2gpio.GPIO_PinConfig.pGPIOx = GPIOA;
	USART2gpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	USART2gpio.GPIO_PinConfig.GPIO_PinAltFunMode = 7;
	USART2gpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USART2gpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	USART2gpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	USART2gpio.numberOfPinsToConfig = 2;
	//TX
	USART2gpio.PinNumber[0] = GPIO_PIN_2;
	//RX
	USART2gpio.PinNumber[1] = GPIO_PIN_3;
	GPIO_Init(&USART2gpio);
}

void USART2_INIT(){

	usart2.pUSARTx = USART2;
	usart2.USART_Config.USART_Baud = USART_STD_BAUD_115200;
	usart2.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	usart2.USART_Config.USART_Mode = USART_MODE_TXRX;
	usart2.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	usart2.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	usart2.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	USART_Init(&usart2);
}

void DMAInit(){

	memset(&hdma1, 0, sizeof(hdma1));

	DMA_PeriphClockControl(DMA1, ENABLE);
	NVIC_IRQInterruptConfig(DMA1_Channel6_IRQn, ENABLE);
	NVIC_IRQPriorityConfig(DMA1_Channel6_IRQn, 15);

	hdma1.DMAx = DMA1;
	hdma1.DMAChannel = DMA1_Channel6;
	hdma1.dmaInterruptsEnable.transferComplete = ENABLE;
	hdma1.dmaInterruptsEnable.transferError = ENABLE;
	//hdma1.transferConfig.memoryAddress = (uint32_t)received_data;
	hdma1.transferConfig.peripheralAddress = (uint32_t)&(USART2->RDR);
	//hdma1.transferConfig.numberOfDataToTransfer = 5;
	hdma1.DMA_Config.direction = DMA_DIRECTION_PRIPHERAL_TO_MEMORY;
	hdma1.DMA_Config.peripheralIncrementMode = DISABLE;
	hdma1.DMA_Config.memoryIncrementMode = ENABLE;
	hdma1.DMA_Config.transferMode = DMA_MODE_CIRCULAR;

	DMA_Init(&hdma1);

}

int main(void){

	USART2_GPIOInit();
	USART2_INIT();
	DMAInit();

	//USART_StartReceivingDataDMA(&usart2, DMA1_Channel6);
	USART_ReceiveDataDMA(&usart2, received_data, 5, DMA1_Channel6);
	while(1){


	}

}

void DMA1_CH6_IRQHandler(void){

	DMA_IRQHandling(&hdma1);
}






