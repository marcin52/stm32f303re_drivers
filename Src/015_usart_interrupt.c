/*
 * 015_usart_interrupt.c
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

USART_Handle_t usart2;
uint8_t user_data[] = "Hello world";
uint8_t received_data[2];

// For belwo pins alternate function mode: AF7
//PA2 --> USART2_TX
//PA3 --> USART2_RX


void USART2_GPIOInit(void){
	GPIO_Handle_t USART2gpio;

	USART2gpio.GPIO_PinConfig.pGPIOx = GPIOA;
	USART2gpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	USART2gpio.GPIO_PinConfig.GPIO_PinAltFunMode = 7;
	USART2gpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USART2gpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	USART2gpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	USART2gpio.numberOfPinsToConfig = 2;
	//TX
	USART2gpio.PinNumber[0] = GPIO_PIN_2;
	//RX
	USART2gpio.PinNumber[1] = GPIO_PIN_3;
	GPIO_Init(&USART2gpio);
}

void USART2_INIT(){

	usart2.pUSARTx = USART2;
	usart2.USART_Config.USART_Baud = USART_STD_BAUD_115200;
	usart2.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	usart2.USART_Config.USART_Mode = USART_MODE_TXRX;
	usart2.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	usart2.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	usart2.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	NVIC_IRQPriorityConfig(USART2_IRQn, NVIC_IRQ_PRI15);
	NVIC_IRQInterruptConfig(USART2_IRQn, ENABLE);

	USART_Init(&usart2);
}

int main(void){

	USART2_GPIOInit();
	USART2_INIT();
	USART_PeripheralControl(USART2, ENABLE);
	USART_SendDataIT(&usart2, user_data, strlen(user_data));
	USART_SendDataIT(&usart2, user_data, strlen(user_data));
	USART_ReceiveDataIT(&usart2, received_data, 2);
	while(1){

	}

}

void USART2_EXTI26_IRQHandler(void) {
	USART_IRQHandling(&usart2);
}
void USART_ReadDataRegisterEmptyCallback(USART_Handle_t *pUSARTHandle){
	USART_ReceiveDataIT(&usart2, received_data, 2);
}



