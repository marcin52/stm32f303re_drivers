/*
 * 003_button_interrupt.c
 *
 *  Created on: 4 kwi 2020
 *      Author: marci
 */

#include <string.h>
#include "stm32f303xx.h"

void delay(){

	for(uint32_t i=0; i<100000; i++){
	}
}

int main(void){

	GPIO_Handle_t gpioled;
	GPIO_Handle_t button;

	memset(&gpioled, 0, sizeof(gpioled));
	memset(&button, 0, sizeof(button));

	gpioled.GPIO_PinConfig.pGPIOx = GPIOA;
	gpioled.numberOfPinsToConfig = 1;
	gpioled.PinNumber[0] = GPIO_PIN_5;
	gpioled.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUT;
	gpioled.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_LOW;
	gpioled.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	gpioled.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;

	GPIO_PeriphClockControl(GPIOA, ENABLE);

	GPIO_Init(&gpioled);

	button.GPIO_PinConfig.pGPIOx = GPIOC;
	button.numberOfPinsToConfig = 1;
	button.PinNumber[0] = GPIO_PIN_13;
	button.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IT_RAISING_FALLING_EDGE;
	button.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;

	GPIO_PeriphClockControl(GPIOC, ENABLE);

	GPIO_Init(&button);

	NVIC_IRQPriorityConfig(EXTI15_10_IRQn, NVIC_IRQ_PRI15);
	NVIC_IRQInterruptConfig(EXTI15_10_IRQn, ENABLE);

	while(1){

	}

	return 0;
}
void EXTI15_10_IRQHandler(void){

	//delay due to resolve button debouncing problem
	delay();
	GPIO_IRQHandling(GPIO_PIN_13);
	GPIO_TogglePin(GPIOA, GPIO_PIN_5);
}

