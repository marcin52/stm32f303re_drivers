/*
 * 019_nextion_screen_test.c
 *
 *  Created on: 27 lip 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

USART_Handle_t usart2;
uint8_t user_data[] = "Hello world";
uint8_t received_data[2];

// For belwo pins alternate function mode: AF7
//PA9 --> USART1_TX
//PA10 --> USART1_RX


void USART2_GPIOInit(void){
	GPIO_Handle_t USART2gpio;

	USART2gpio.GPIO_PinConfig.pGPIOx = GPIOC;
	USART2gpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	USART2gpio.GPIO_PinConfig.GPIO_PinAltFunMode = 7;
	USART2gpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USART2gpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	USART2gpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	USART2gpio.numberOfPinsToConfig = 2;
	//TX
	USART2gpio.PinNumber[0] = GPIO_PIN_4;
	//RX
	USART2gpio.PinNumber[1] = GPIO_PIN_10;
	GPIO_Init(&USART2gpio);
}

void USART2_INIT(){

	usart2.pUSARTx = USART1;
	usart2.USART_Config.USART_Baud = USART_STD_BAUD_115200;
	usart2.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	usart2.USART_Config.USART_Mode = USART_MODE_TXRX;
	usart2.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	usart2.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	usart2.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	NVIC_IRQPriorityConfig(USART1_IRQn, NVIC_IRQ_PRI15);
	NVIC_IRQInterruptConfig(USART1_IRQn, ENABLE);

	USART_Init(&usart2);
}
/*
void Nextion_send_cmd(uint8_t * cmd, uint8_t temp){

	uint8_t tempTens = (temp / 10);
	uint8_t tempUnits =( temp - tempTens*10) + 48;
	tempTens += 48;
	cmd[17] = tempTens;
	cmd[18] = tempUnits;
	USART_SendDataIT(&usart2, cmd, 23);
}*/

const volatile uint8_t nextion_cmd_end[3] = {0xff, 0xff, 0xff};

void Nextion_send_cmd(uint8_t * cmd){

	uint8_t cmdlen = strlen(cmd);
	uint8_t len = cmdlen + 3;
	uint8_t data[len];
	for(int i =0; i< cmdlen; i++){
		data[i] = cmd[i];
	}
	for(int i = cmdlen; i < len; i++){
		data[i] = nextion_cmd_end[i - cmdlen];
	}
	//strcat(cmd, nextion_cmd_end);
	USART_SendDataIT(&usart2, data, 23);
}
char data[20] = {'t','e','m','p','e','r','a','t','u','r','a','.','t','x','t','=','"','2','6','"'};
char data2[20] = {'t','e','m','p','e','r','a','t','u','r','a','.','t','x','t','=','"','2','6','"','ÿ', 'ÿ', 'ÿ', '\0'};
uint8_t data3[23]={0x74, 0x65, 0x6D, 0x70, 0x65, 0x72, 0x61, 0x74, 0x75, 0x72, 0x61, 0x2E, 0x74, 0x78, 0x74, 0x3D, 0x22, 0x32, 0x38, 0x22, 0xff, 0xff, 0xff};
int main(void){

	USART2_GPIOInit();
	USART2_INIT();
	USART_PeripheralControl(USART1, ENABLE);
	//USART_SendDataIT(&usart2, user_data, strlen(user_data));
	//USART_SendDataIT(&usart2, user_data, strlen(user_data));
	//USART_ReceiveDataIT(&usart2, received_data, 2);
	Nextion_send_cmd((uint8_t *)"temperatura.txt=\"32\"");
	//Nextion_send_cmd(data3, 76);
	//USART_SendDataIT(&usart2, data2, strlen(data2));
	//USART_SendDataIT(&usart2, data3, 23);
	while(1){

	}

}

void USART1_EXTI25_IRQHandler(void) {
	USART_IRQHandling(&usart2);
}
void USART_ReadDataRegisterEmptyCallback(USART_Handle_t *pUSARTHandle){
	USART_ReceiveDataIT(&usart2, received_data, 2);
}

//void USART_TransferCpltCallback(USART_Handle_t *pUSARTHandle){
//
	//
//}


