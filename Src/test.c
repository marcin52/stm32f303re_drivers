/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32l0xx.h"
#include "stm32l0xx_nucleo.h"


uint8_t g_lastStableState = 1;
uint8_t g_lastState = 1;
uint8_t g_lastStateCount = 0;
const uint8_t COUNT_MAX = 10;
uint8_t was_vtc_changed = 0;

int COS[512] = {9999,9997,9993,9988,9981,9973,9963,9952,9939,9925,9909,9892,9873,9853,9831,9808,9783,9757,9729,9700,9670,9638,9604,9569,9533,9495,9456,9415,9373,9330,9285,9239,9191,9142,9092,9040,8987,8932,8876,8819,8761,8701,8640,8577,8514,8449,8382,8315,8246,8176,8105,8032,7958,7883,7807,7730,7652,7572,7491,7410,7327,7242,7157,7071,6984,6895,6806,6716,6624,6532,6438,6344,6249,6152,6055,5957,5858,5758,5657,5556,5453,5350,5246,5141,5035,4929,4822,4714,4605,4496,4386,4276,4164,4052,3940,3827,3713,3599,3484,3369,3253,3137,3020,2903,2785,2667,2549,2430,2311,2191,2071,1951,1830,1710,1589,1467,1346,1224,1102,980,858,736,613,491,368,245,123,0,-123,-245,-368,-491,-613,-736,-858,-980,-1102,-1224,-1346,-1467,-1589,-1710,-1830,-1951,-2071,-2191,-2311,-2430,-2549,-2667,-2785,-2903,-3020,-3137,-3253,-3369,-3484,-3599,-3713,-3827,-3940,-4052,-4164,-4276,-4386,-4496,-4605,-4714,-4822,-4929,-5035,-5141,-5246,-5350,-5453,-5556,-5657,-5758,-5858,-5957,-6055,-6152,-6249,-6344,-6438,-6532,-6624,-6716,-6806,-6895,-6984,-7071,-7157,-7242,-7327,-7410,-7491,-7572,-7652,-7730,-7807,-7883,-7958,-8032,-8105,-8176,-8246,-8315,-8382,-8449,-8514,-8577,-8640,-8701,-8761,-8819,-8876,-8932,-8987,-9040,-9092,-9142,-9191,-9239,-9285,-9330,-9373,-9415,-9456,-9495,-9533,-9569,-9604,-9638,-9670,-9700,-9729,-9757,-9783,-9808,-9831,-9853,-9873,-9892,-9909,-9925,-9939,-9952,-9963,-9973,-9981,-9988,-9993,-9997,-9999,-10000,-9999,-9997,-9993,-9988,-9981,-9973,-9963,-9952,-9939,-9925,-9909,-9892,-9873,-9853,-9831,-9808,-9783,-9757,-9729,-9700,-9670,-9638,-9604,-9569,-9533,-9495,-9456,-9415,-9373,-9330,-9285,-9239,-9191,-9142,-9092,-9040,-8987,-8932,-8876,-8819,-8761,-8701,-8640,-8577,-8514,-8449,-8382,-8315,-8246,-8176,-8105,-8032,-7958,-7883,-7807,-7730,-7652,-7572,-7491,-7410,-7327,-7242,-7157,-7071,-6984,-6895,-6806,-6716,-6624,-6532,-6438,-6344,-6249,-6152,-6055,-5957,-5858,-5758,-5657,-5556,-5453,-5350,-5246,-5141,-5035,-4929,-4822,-4714,-4605,-4496,-4386,-4276,-4164,-4052,-3940,-3827,-3713,-3599,-3484,-3369,-3253,-3137,-3020,-2903,-2785,-2667,-2549,-2430,-2311,-2191,-2071,-1951,-1830,-1710,-1589,-1467,-1346,-1224,-1102,-980,-858,-736,-613,-491,-368,-245,-123,-0,123,245,368,491,613,736,858,980,1102,1224,1346,1467,1589,1710,1830,1951,2071,2191,2311,2430,2549,2667,2785,2903,3020,3137,3253,3369,3484,3599,3713,3827,3940,4052,4164,4276,4386,4496,4605,4714,4822,4929,5035,5141,5246,5350,5453,5556,5657,5758,5858,5957,6055,6152,6249,6344,6438,6532,6624,6716,6806,6895,6984,7071,7157,7242,7327,7410,7491,7572,7652,7730,7807,7883,7958,8032,8105,8176,8246,8315,8382,8449,8514,8577,8640,8701,8761,8819,8876,8932,8987,9040,9092,9142,9191,9239,9285,9330,9373,9415,9456,9495,9533,9569,9604,9638,9670,9700,9729,9757,9783,9808,9831,9853,9873,9892,9909,9925,9939,9952,9963,9973,9981,9988,9993,9997,9999,10000};

void setPWM_value( uint16_t value ){
	 TIM2->CCR1 = value;

}

uint16_t tabel_place = 0; //aktualne miejsce w tabeli

void next_PWM_value(){
	setPWM_value(COS[tabel_place]);
	tabel_place++;
	if(tabel_place==512){
		tabel_place=0;
	}
}


uint8_t counter = 0;
uint8_t VALUE_TO_COUNT = 16;
uint8_t x_0 = 0;

void TIM22_IRQHandler(){
	if (0 != (TIM_SR_UIF & TIM22->SR)) { // check update interrupt flag //sprawdzamy czy na pewno TIM2 wywolal funkcje

		TIM22->SR &= ~TIM_SR_UIF; // clear interrupt flag

		if(x_0 == 0){
			setPWM_value(10000);
		}
		else{
			counter++;

			if(VALUE_TO_COUNT<=counter){
				next_PWM_value();
				counter=0;
			}
		}
	}
}

void change_value_to_count(){

	if(x_0==0){
		x_0=1;
		VALUE_TO_COUNT = 16;
	}
	else{

		if(VALUE_TO_COUNT!=1){
			VALUE_TO_COUNT=VALUE_TO_COUNT/2;
		}

	}
}

uint16_t reset_counter = 0;

void reset_value_to_count(){
	reset_counter++;

	if(reset_counter == 199){
		x0=0;
		reset_counter = 0;
	}
}

void TIM21_IRQHandler(void) // periodic @ 1ms //w momencie przerwania TIM2 wywoluje ta funkcje
{
	if (0 != (TIM_SR_UIF & TIM21->SR)) { // check update interrupt flag //sprawdzamy czy na pewno TIM2 wywolal funkcje
		TIM21->SR &= ~TIM_SR_UIF; // clear interrupt flag

		uint8_t currentState = (GPIO_IDR_ID13 & GPIOC->IDR)?(1):(0); //if (GPIO_IDR_ID13 & GPIOC->IDR) to zmienna 1, else 0

		if (currentState != g_lastState) {
			g_lastState = currentState;
			g_lastStateCount = 0;
			was_vtc_changed = 0;
			reset_counter = 0;

		} else /* (currentState == g_lastState) */ {
			if (COUNT_MAX >= g_lastStateCount) {
				g_lastStateCount++;
			}

			if (COUNT_MAX == g_lastStateCount) {
				uint8_t currentStableState = currentState;

				if ((1 == g_lastStableState) && (0 == currentStableState)) {

					if(was_vtc_changed == 0){
						change_value_to_count();
						was_vtc_changed = 1;
					}
					else{
						reset_value_to_count();
					}
				}

				g_lastStableState = currentStableState;
			}
		}
	}
}


int main(void)
{

	// ------------- RCC -------------
	RCC->CR |= RCC_CR_HSION; // enable HSI
	while (0 == (RCC->CR  & RCC_CR_HSIRDY)) { // wait until RDY bit is set
		// empty
	}
	RCC->CFGR |= RCC_CFGR_SW_0; // set SYSCLK to HSI16

	RCC->IOPENR |= RCC_IOPENR_GPIOAEN; // enable GPIOA clock
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; // enable TIM2 clock
	RCC->IOPENR |= RCC_IOPENR_GPIOCEN; // enable GPIOC clock //taktowanie od GPIOC (Guzik)
	RCC->APB2ENR |= RCC_APB2ENR_TIM21EN; // enable TIM21 clock
	RCC->APB2ENR |= RCC_APB2ENR_TIM22EN; // enable TIM22 clock

	// ------------- GPIOC (pin 13 - Button) -------------
	GPIOC->MODER &= ~(GPIO_MODER_MODE13_1 | GPIO_MODER_MODE13_0);//zerowanie bitów, zostaje nam input mode

	GPIOC->OSPEEDR |= (GPIO_OSPEEDER_OSPEED13_1 | GPIO_OSPEEDER_OSPEED13_0); //prędkosc przelaczania


	// ------------- GPIOA (pin 5 - LED) -------------
	// Alternate function AF = 5 dla TIM2_CH1 //oddajemy pin pod wladanie innemu modulowi, led jest podlaczony do pin PA5

	GPIOA->MODER &= ~(GPIO_MODER_MODE5_1 | GPIO_MODER_MODE5_0);
	GPIOA->MODER |= 2 << GPIO_MODER_MODE5_Pos;
	GPIOA->OSPEEDR |= ( 3 << GPIO_OSPEEDER_OSPEED5_Pos ); // w systemie jednostkowym zapisujemy 3
	GPIOA->AFR[0] |= ( 5 << GPIO_AFRL_AFSEL5_Pos ); //dwa rejestry AFR, jeden od 0-7, drugi 8-15

	// ------------- TIM2 PWM_-------------
	TIM2->PSC = 16 - 1;
	TIM2->ARR = 10000 - 1; //maksymalne wypelnienie PWM

	TIM2->CCMR1 |= ( 6 << TIM_CCMR1_OC1M_Pos ); // 6 = 110 = PWM mode 1
	TIM2->CCMR1 |= ( 1 << TIM_CCMR1_OC1PE_Pos ); // Pozwala na aktualizacje wypelnienia, mozliwa zmiana wartosci w CCR1
	// Timer 2 channel 1 domyĹ›lnie skonfigurowany jako output
	TIM2->CCER |= ( 1 << TIM_CCER_CC1E_Pos ); // Sprawia ze sygnal PWM jest przekazywany do pinu

	TIM2->EGR |= ( 1 << TIM_EGR_UG_Pos ); // przepisanie konfiguracji do shadow registers modulu PWM
	// TIM2 enable



	// ------------- TIM21 -------------
	TIM21->DIER |= TIM_DIER_UIE; // umozliwiam timerowi generowanie przerwania

	//1600000/125 = 128000;   128000/128 = 1000
	TIM21->PSC = 125 - 1; //co ile taktów timer zwieksza licznik
	TIM21->ARR = 128 - 1; // 1000 razy na sekunde przerwanie, ARR - do jakiej wartosci timer liczy, -1 po to bo liczenie startuje od 0

	NVIC_EnableIRQ(TIM21_IRQn); // nvic - kontroler przerwan, informuje nvica o wykonaniu przerwan

	// ------------- TIM22 -------------
	TIM22->DIER |= TIM_DIER_UIE; // umozliwiam timerowi generowanie przerwania

	//1600000/125 = 128000;   128000/128 = 1000
	TIM22->PSC = 125 - 1; //co ile taktów timer zwieksza licznik
	TIM22->ARR = 128 - 1; // 1000 razy na sekunde przerwanie, ARR - do jakiej wartosci timer liczy, -1 po to bo liczenie startuje od 0

	NVIC_EnableIRQ(TIM22_IRQn); // nvic - kontroler przerwan, informuje nvica o wykonaniu przerwan


	// ------------- final configuration -------------

	// TIM22 enable
	TIM22->CR1 |= TIM_CR1_CEN; // count enable

	// TIM21 enable
	TIM21->CR1 |= TIM_CR1_CEN; // count enable

	// TIM2 enable
	TIM2->CR1 |= TIM_CR1_CEN; // count enable

	for(;;)
	{
	}

}
