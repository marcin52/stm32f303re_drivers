/*
 * 004_spi_tx_blocking.c
 *
 *  Created on: 8 kwi 2020
 *      Author: marci
 */
#include <string.h>
#include "stm32f303xx.h"

// For belwo pins alternate function mode: AF5
//PB15 --> SPI2_MOSI
//PB14 --> SPI2_MISO
//PB13 --> SPI2_SCK
//PB12 --> SPI_NSS

void SPI2_GPIOInit(void){
	GPIO_Handle_t SPIgpio;

	SPIgpio.GPIO_PinConfig.pGPIOx = GPIOB;
	SPIgpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	SPIgpio.GPIO_PinConfig.GPIO_PinAltFunMode = 5;
	SPIgpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	SPIgpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;
	SPIgpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	SPIgpio.numberOfPinsToConfig = 2;
	//MOSI
	SPIgpio.PinNumber[0] = GPIO_PIN_15;
	//MISO
	//SPIgpio.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_14;
	//GPIO_Init(&SPIgpio);
	//SCK
	SPIgpio.PinNumber[1] = GPIO_PIN_13;
	GPIO_Init(&SPIgpio);
	//NSS
	//SPIgpio.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_12;
	//GPIO_Init(&SPIgpio);
}

void SPI2_Init(void){

	SPI_Handle_t SPI2handle;

	SPI2handle.pSPIx = SPI2;
	SPI2handle.SPIConfig.SPI_BusConfig = SPI_BUS_FULLDUPLEX;
	SPI2handle.SPIConfig.SPI_DeviceMode = SPI_DEVICE_MODE_MASTER;
	SPI2handle.SPIConfig.SPI_SclkSpeed = SPI_SPEED_DIV2;
	SPI2handle.SPIConfig.SPI_DataSize = SPI_DFF_8BITS;
	SPI2handle.SPIConfig.SPI_CPOL = SPI_CPOL_LOW;
	SPI2handle.SPIConfig.SPI_CPHA = SPI_CPHA_LOW;
	SPI2handle.SPIConfig.SPI_SSM = SPI_SSM_EN; // Software slave managment for NSS pin

	SPI_Init(&SPI2handle);
}

void delay(){

	for(uint32_t i=0; i<5000000; i++);
}

int main(void){

	char user_data[] = "Hello world";

	SPI2_GPIOInit();

	SPI2_Init();

	// this makes NSS signal internally high and avoids MODF error
	SPI_SSIConfig(SPI2, ENABLE);

	// enable SPI peripheral
	SPI_PeripheralControl(SPI2, ENABLE);
	while(1){
		SPI_SendData(SPI2, (uint8_t*)user_data, strlen(user_data));
	}
	// disable SPI peripheral
	SPI_PeripheralControl(SPI2, DISABLE);

	while(1);
	return 0;
}
