/*
 * 018_rtc_init_calendar.c
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

// For belwo pins alternate function mode: AF7
//PA2 --> USART2_TX
//PA3 --> USART2_RX

USART_Handle_t usart2;

uint8_t user_data[] = "WakeUp!!\n";
uint8_t user_dataA[] = "ALARM_A!!\n";
uint8_t user_dataB[] = "ALARM_B!!\n";
uint8_t user_data2[] = "init\n";

void USART2_GPIOInit(void){
	GPIO_Handle_t USART2gpio;

	USART2gpio.GPIO_PinConfig.pGPIOx = GPIOA;
	USART2gpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	USART2gpio.GPIO_PinConfig.GPIO_PinAltFunMode = 7;
	USART2gpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USART2gpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	USART2gpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	USART2gpio.numberOfPinsToConfig = 2;
	//TX
	USART2gpio.PinNumber[0] = GPIO_PIN_2;
	//RX
	USART2gpio.PinNumber[1] = GPIO_PIN_3;
	GPIO_Init(&USART2gpio);
}

void USART2_INIT(){

	usart2.pUSARTx = USART2;
	usart2.USART_Config.USART_Baud = USART_STD_BAUD_115200;
	usart2.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	usart2.USART_Config.USART_Mode = USART_MODE_TXRX;
	usart2.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	usart2.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	usart2.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	NVIC_IRQPriorityConfig(USART2_IRQn, NVIC_IRQ_PRI15);
	NVIC_IRQInterruptConfig(USART2_IRQn, ENABLE);

	USART_Init(&usart2);
}


volatile RTC_time_t timeAfter;
volatile RTC_date_t dateAfter;


int main(void){

	volatile RTC_time_t time;
	volatile RTC_date_t date;
	volatile RTC_alarm_t alarmA;
	volatile RTC_alarm_t alarmB;

	time.hourFormat = RTC_24_HOUR_FORMAT;
	time.hours = 23;
	time.minutes = 59;
	time.seconds = 58;


	date.day = 26;
	date.month = 7;
	date.year = 25;
	date.weekDay= 7;


	alarmA.hoursMaskEnable = DISABLE;
	alarmA.minutesMaskEnable = DISABLE;
	alarmA.secondsMaskEnable = DISABLE;
	alarmA.dateMaskEnable = ENABLE;
	alarmA.time.hours = 00;
	alarmA.time.minutes = 00;
	alarmA.time.seconds = 1;
	alarmA.time.hourFormat = RTC_24_HOUR_FORMAT;


	alarmB.hoursMaskEnable = DISABLE;
	alarmB.minutesMaskEnable = DISABLE;
	alarmB.secondsMaskEnable = DISABLE;
	alarmB.dateMaskEnable = ENABLE;
	alarmB.time.hours = 00;
	alarmB.time.minutes = 00;
	alarmB.time.seconds = 5;
	alarmB.time.hourFormat = RTC_24_HOUR_FORMAT;

	NVIC_EnableIRQ(RTC_Alarm_IRQn);
	NVIC_IRQPriorityConfig(RTC_Alarm_IRQn, NVIC_IRQ_PRI14);

	USART2_GPIOInit();
	USART2_INIT();
	USART_PeripheralControl(USART2, ENABLE);
	USART_SendDataIT(&usart2, user_data2, strlen(user_data2));
	// RTC CONFIG
	// KOLEJNOSC JEST WAZNA!!!
	RCC_LSI_Init();
	PWR_PCKL_EN();
	PWR_RTC_DomainWriteProtectionControl(DISABLE);
	RCC_SelectRTC_ClockSource(RCC_RTC_CLOCKSOURCE_LSI);
	RTC_PeriphClockControl(ENABLE);
	RTC_BackUpDomainResetWriteProtectionControl(DISABLE);
	RTC_StartInitMode();
	RTC_SetPrescalers(125, 290);

	RTC_SetTime(&time);
	RTC_SetDate(&date);
	RTC_SetAlarm(&alarmA, RTC_ALARM_A);
	RTC_SetAlarm(&alarmB, RTC_ALARM_B);
	RTC_WakeupTimerInit(RTC_ck_spre, 3);
	RTC_StopInitMode();
	//RTC_PeriphClockControl(ENABLE);


	while(1){
		RTC_GetTime(&timeAfter);
		RTC_GetDate(&dateAfter);
	}

	return 0;
}

void RTCAlarm_IRQHandler(void){

	RTC_IRQHandler();
}

void RTC_AlarmA_Callback(){

	USART_SendDataIT(&usart2, user_dataA, strlen(user_dataA));
}

void RTC_AlarmB_Callback(){

	USART_SendDataIT(&usart2, user_dataB, strlen(user_dataB));

	RTC_alarm_t alarmA;
	alarmA.hoursMaskEnable = DISABLE;
	alarmA.minutesMaskEnable = DISABLE;
	alarmA.secondsMaskEnable = DISABLE;
	alarmA.dateMaskEnable = ENABLE;
	alarmA.time.hours = 00;
	alarmA.time.minutes = 00;
	alarmA.time.seconds = 8;
	alarmA.time.hourFormat = RTC_24_HOUR_FORMAT;

	RTC_alarm_t alarmB;
	alarmB.hoursMaskEnable = DISABLE;
	alarmB.minutesMaskEnable = DISABLE;
	alarmB.secondsMaskEnable = DISABLE;
	alarmB.dateMaskEnable = ENABLE;
	alarmB.time.hours = 00;
	alarmB.time.minutes = 00;
	alarmB.time.seconds = 11;
	alarmB.time.hourFormat = RTC_24_HOUR_FORMAT;

}

void USART2_EXTI26_IRQHandler(void) {
	USART_IRQHandling(&usart2);
}

void RTC_WKUP_IRQHandler(void){

	RTC_IRQHandler();

}

void RTC_WakeUp_Callback(){

	USART_SendDataIT(&usart2, user_data, strlen(user_data));
}

