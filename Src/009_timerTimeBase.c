
/*
 * 009_timerTimeBase.c
 *
 *  Created on: 3 maj 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

int i=0;
TIM_TimeBaseInit_t htim7;

int main(void){

	// 100 ms timebase generation

	TIM_PeriphClockControl(TIM7, ENABLE);

	NVIC_IRQPriorityConfig(TIM7_IRQn, 15);
	NVIC_IRQInterruptConfig(TIM7_IRQn, ENABLE);

	memset(&htim7, 0, sizeof(htim7));

	htim7.prescaler = 799;
	htim7.autoReloadRegister = 999;
	htim7.direction = TIM_DIRECTION_DOWN_COUNTING;

	TIM_TimeBaseInit(TIM7, &htim7);
	TIM_StartCounting(TIM7);

	while(1);

}
void TIM7_IRQHandler(void){

	i++;
	TIM_IRQHandling(TIM7);
}

