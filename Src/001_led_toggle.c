/*
 * 001_led_toggle.c
 *
 *  Created on: 2 kwi 2020
 *      Author: marci
 */

#include "stm32f303xx.h"

void delay(){

	for(uint32_t i=0; i<1000000; i++);
}

int main(void){

	GPIO_Handle_t gpioled;

	// Settings to config PA5 as a PushPull output
	gpioled.GPIO_PinConfig.pGPIOx = GPIOA;
	gpioled.numberOfPinsToConfig = 1;
	gpioled.PinNumber[0] = GPIO_PIN_5;
	gpioled.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUT;
	gpioled.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_LOW;
	gpioled.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	gpioled.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;

	// GPIO Clock enable - this is not neccessary when calling GPIO_Init() since this call contains Clock enable
	GPIO_PeriphClockControl(GPIOA, ENABLE);

	// Initialization of GPIO
	GPIO_Init(&gpioled);

	while(1){
		GPIO_TogglePin(GPIOA, GPIO_PIN_5);
		delay();
	}

	return 0;
}
