/*
 * 020_rtc_wakeup_timer.c
 *
 *  Created on: 5 sie 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

// For belwo pins alternate function mode: AF7
//PA2 --> USART2_TX
//PA3 --> USART2_RX

USART_Handle_t usart2;
uint8_t user_data[] = "WakeUp!!\n";
uint8_t user_data2[] = "init\n";

void USART2_GPIOInit(void){
	GPIO_Handle_t USART2gpio;

	USART2gpio.GPIO_PinConfig.pGPIOx = GPIOA;
	USART2gpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	USART2gpio.GPIO_PinConfig.GPIO_PinAltFunMode = 7;
	USART2gpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USART2gpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	USART2gpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	USART2gpio.numberOfPinsToConfig = 2;
	//TX
	USART2gpio.PinNumber[0] = GPIO_PIN_2;
	//RX
	USART2gpio.PinNumber[1] = GPIO_PIN_3;
	GPIO_Init(&USART2gpio);
}

void USART2_INIT(){

	usart2.pUSARTx = USART2;
	usart2.USART_Config.USART_Baud = USART_STD_BAUD_115200;
	usart2.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	usart2.USART_Config.USART_Mode = USART_MODE_TXRX;
	usart2.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	usart2.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	usart2.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	NVIC_IRQPriorityConfig(USART2_IRQn, NVIC_IRQ_PRI15);
	NVIC_IRQInterruptConfig(USART2_IRQn, ENABLE);

	USART_Init(&usart2);
}

int main(void){

	RTC_time_t time;
	time.hourFormat = RTC_24_HOUR_FORMAT;
	time.hours = 21;
	time.minutes = 30;
	time.seconds = 43;

	RTC_date_t date;
	date.day = 26;
	date.month = 7;
	date.year = 20;
	date.weekDay= 7;

	NVIC_EnableIRQ(RTC_WKUP_IRQn);
	NVIC_IRQPriorityConfig(RTC_WKUP_IRQn, NVIC_IRQ_PRI14);

	USART2_GPIOInit();
	USART2_INIT();
	USART_PeripheralControl(USART2, ENABLE);
	USART_SendDataIT(&usart2, user_data2, strlen(user_data2));

	// RTC CONFIG
	// KOLEJNOSC JEST WAZNA!!!
	RCC_LSI_Init();
	PWR_PCKL_EN();
	PWR_RTC_DomainWriteProtectionControl(DISABLE);
	RCC_SelectRTC_ClockSource(RCC_RTC_CLOCKSOURCE_LSI);
	RTC_PeriphClockControl(ENABLE);
	RTC_BackUpDomainResetWriteProtectionControl(DISABLE);
	RTC_StartInitMode();
	RTC_ProgramPrescalers(125, 290);

	RTC_SetTime(&time);
	RTC_SetDate(&date);
	RTC_WakeupTimerInit(RTC_ck_spre, 3);
	RTC_StopInitMode();
	RTC_PeriphClockControl(ENABLE);


	while(1){

	}

	return 0;
}

void RTC_WKUP_IRQHandler(void){

	RTC_IRQHandler();

}

void RTC_WakeUp_Callback(){

	USART_SendDataIT(&usart2, user_data, strlen(user_data));
}

void USART2_EXTI26_IRQHandler(void) {
	USART_IRQHandling(&usart2);
}



