/*
 * 004_spi_tx_blocking.c
 *
 *  Created on: 8 kwi 2020
 *      Author: marci
 */
#include <string.h>
#include "stm32f303xx.h"

// For belwo pins alternate function mode: AF5
//PB15 --> SPI2_MOSI
//PB14 --> SPI2_MISO
//PB13 --> SPI2_SCK
//PB12 --> SPI2_NSS

void SPI2_GPIOInit(void){
	GPIO_Handle_t SPIgpio;

	SPIgpio.GPIO_PinConfig.pGPIOx = GPIOB;
	SPIgpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	SPIgpio.GPIO_PinConfig.GPIO_PinAltFunMode = 5;
	SPIgpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	SPIgpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;
	SPIgpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	SPIgpio.numberOfPinsToConfig = 4;
	SPIgpio.PinNumber[0] = GPIO_PIN_15;
	SPIgpio.PinNumber[1] = GPIO_PIN_14;
	SPIgpio.PinNumber[2] = GPIO_PIN_13;
	SPIgpio.PinNumber[3] = GPIO_PIN_12;

	GPIO_Init(&SPIgpio);
}

void SPI2_Init(void){

	SPI_Handle_t SPI2handle;

	SPI2handle.pSPIx = SPI2;
	SPI2handle.SPIConfig.SPI_BusConfig = SPI_BUS_FULLDUPLEX;
	SPI2handle.SPIConfig.SPI_DeviceMode = SPI_DEVICE_MODE_MASTER;
	SPI2handle.SPIConfig.SPI_SclkSpeed = SPI_SPEED_DIV8; // 8 MHz SPI_SCK
	SPI2handle.SPIConfig.SPI_DataSize = SPI_DFF_8BITS;
	SPI2handle.SPIConfig.SPI_CPOL = SPI_CPOL_LOW;
	SPI2handle.SPIConfig.SPI_CPHA = SPI_CPHA_LOW;
	SPI2handle.SPIConfig.SPI_SSM = SPI_SSM_DI; // Hardware slave managment for NSS pin

	SPI_Init(&SPI2handle);
}

void delay(){

	for(uint32_t i=0; i<50000; i++);
}

void GPIO_ButtonInit(){

	GPIO_Handle_t button;

	button.GPIO_PinConfig.pGPIOx = GPIOC;
	button.PinNumber[0] = GPIO_PIN_13;
	button.numberOfPinsToConfig = 1;
	button.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	button.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;

	GPIO_Init(&button);
}

int main(void){

	char user_data[] = "Hello world";

	SPI2_GPIOInit();
	GPIO_ButtonInit();

	SPI2_Init();

	/*
	 * making SSOE 1 does NSS output enable
	 * The NSS pin is automatically managed by the hardware
	 * i. 3 when SPE = 1, NSS will be pulled to low
	 * and NSS pin will be high when SPE = 0
	 */

	SPI_SSOEConfig(SPI2, ENABLE);

	while(1){
		while( ! GPIO_ReadPin(GPIOC, GPIO_PIN_13)){

			delay();
			delay();
			// enable SPI peripheral
			SPI_PeripheralControl(SPI2, ENABLE);

			// First send length information
			uint8_t datalen = strlen(user_data);
			SPI_SendData(SPI2, &datalen, sizeof(datalen));
			//delay();
			SPI_SendData(SPI2, (uint8_t*)user_data, datalen);
			delay();

			// Confirm that SPI is not busy
			while(SPI_GetFlagStatus(SPI2, SPI_BSY_FLAG));

			// disable SPI peripheral
			SPI_PeripheralControl(SPI2, DISABLE);
		}
	}

	return 0;
}
