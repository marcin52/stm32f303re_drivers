
/*
 * 009_timerTimeBase.c
 *
 *  Created on: 13 maj 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

int i=0;
TIM_OutputCompareInit_t htim1;

// TIM1_CHANNEL1 --> PA8 (ALTERNATE 6)

int main(void){

	GPIO_Handle_t pwmGpio;

	pwmGpio.PinNumber[0] = GPIO_PIN_8;
	pwmGpio.numberOfPinsToConfig = 1;
	pwmGpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	pwmGpio.GPIO_PinConfig.GPIO_PinAltFunMode = 6;
	pwmGpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;
	pwmGpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	pwmGpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	pwmGpio.GPIO_PinConfig.pGPIOx = GPIOA;

	GPIO_Init(&pwmGpio);

	TIM_PeriphClockControl(TIM1, ENABLE);

	NVIC_IRQPriorityConfig(TIM1_UP_TIM16_IRQn, 15);
	NVIC_IRQInterruptConfig(TIM1_UP_TIM16_IRQn, ENABLE);

	memset(&htim1, 0, sizeof(htim1));

	htim1.timeBase.prescaler = 799;
	htim1.timeBase.autoReloadRegister = 999;
	htim1.timeBase.direction = TIM_DIRECTION_UP_COUNTING;
	htim1.outputMode = TIM_MODE_PWM_1;
	htim1.timChannel = TIM_CHANNEL_1;

	TIM_OutputModeInit(TIM1, &htim1);
	TIM_SetCompareValue(TIM1, TIM_CHANNEL_1, 900);
	TIM_StartCounting(TIM1);

	while(1);

}
void TIM1_IRQHandler(void){

	i++;
	TIM_IRQHandling(TIM1);
}

