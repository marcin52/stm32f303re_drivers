/*
 * 001_led_toggle.c
 *
 *  Created on: 2 kwi 2020
 *      Author: marci
 */

#include "stm32f303xx.h"

void delay(){

	for(uint32_t i=0; i<50000; i++);
}

int main(void){

	GPIO_Handle_t gpioled;
	GPIO_Handle_t button;

	gpioled.GPIO_PinConfig.pGPIOx = GPIOA;
	gpioled.numberOfPinsToConfig = 1;
	gpioled.PinNumber[0] = GPIO_PIN_5;
	gpioled.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUT;
	gpioled.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_LOW;
	gpioled.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	gpioled.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;

	GPIO_PeriphClockControl(GPIOA, ENABLE);

	GPIO_Init(&gpioled);

	button.GPIO_PinConfig.pGPIOx = GPIOC;
	button.numberOfPinsToConfig = 1;
	button.PinNumber[0] = GPIO_PIN_13;
	button.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	button.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PULL;

	GPIO_PeriphClockControl(GPIOC, ENABLE);

	GPIO_Init(&button);

	while(1){
		while(!GPIO_ReadPin(GPIOC, GPIO_PIN_13)){
			GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
		}
		GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);

	}

	return 0;
}
