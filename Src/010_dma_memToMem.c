/*
 * 010_dma_memToMem.c
 *
 *  Created on: 3 maj 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

uint8_t source[1000];
uint8_t destination[1000];

int tmp = 0;
DMA_Handle_t hdma1;

int main(void){

	for(int i = 0; i< 1000; i++){
		source[i] = (uint8_t)i;
	}

	memset(&hdma1, 0, sizeof(hdma1));

	DMA_PeriphClockControl(DMA2, ENABLE);
	NVIC_IRQInterruptConfig(DMA2_Channel2_IRQn, ENABLE);
	NVIC_IRQPriorityConfig(DMA2_Channel2_IRQn, 15);

	hdma1.DMAx = DMA2;
	hdma1.DMAChannel = DMA2_Channel2;
	hdma1.dmaInterruptsEnable.transferComplete = ENABLE;
	hdma1.dmaInterruptsEnable.transferError = ENABLE;
	hdma1.transferConfig.memoryAddress = (uint32_t)destination;
	hdma1.transferConfig.peripheralAddress = (uint32_t)source;
	hdma1.transferConfig.numberOfDataToTransfer = 1000;
	hdma1.DMA_Config.peripheralIncrementMode = ENABLE;
	hdma1.DMA_Config.memoryIncrementMode = ENABLE;
	hdma1.DMA_Config.transferMode = DMA_MODE_MEMORY_TO_MEMORY;

	DMA_Init(&hdma1);
	DMA_StartTransfer(&hdma1);

	while(1);

}

void DMA2_CH2_IRQHandler(void){

	DMA_IRQHandling(&hdma1);
}
void DMA_ApplicationTransferCompleteCallback(DMA_Handle_t *pDMAh){
	tmp = 1;
}
void DMA_ApplicationTransferErrorCallback(DMA_Handle_t *pDMAh){
	tmp = 2;
}
