/*
 * 021_i2c_master_send.c
 *
 *  Created on: 13 sie 2020
 *      Author: marci
 */

#include "stm32f303xx.h"
#include <string.h>

I2C_Handle_t hi2c1;
uint8_t user_data[] = "Hello world";
uint8_t received_data;

// For belwo pins alternate function mode: AF4
//PB7 --> I2C1_SDA
//PB6 --> I2C1_SCL


void I2C1_GPIOInit(void){
	GPIO_Handle_t I2Cgpio;

	I2Cgpio.GPIO_PinConfig.pGPIOx = GPIOB;
	I2Cgpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	I2Cgpio.GPIO_PinConfig.GPIO_PinAltFunMode = 4;
	I2Cgpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_OD;
	I2Cgpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	I2Cgpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	I2Cgpio.numberOfPinsToConfig = 2;
	//SDA
	I2Cgpio.PinNumber[0] = GPIO_PIN_7;
	//SCL
	I2Cgpio.PinNumber[1] = GPIO_PIN_6;
	GPIO_Init(&I2Cgpio);
}

void I2C1_INIT(){

	I2C_PeriphClockControl(I2C1, ENABLE);

	hi2c1.pI2Cx = I2C1;
	//hi2c1.I2C_Config.I2C_AckControl = I2C_ACK_ENABLE;
	hi2c1.I2C_Config.I2C_AddressingMode = I2C_ADDRESSING_MODE_7BIT;
	hi2c1.I2C_Config.I2C_AnalogFilter = ENABLE;
	hi2c1.I2C_Config.I2C_DigitalFilter = 0;
	hi2c1.I2C_Config.I2C_AutoEnd = ENABLE;
	hi2c1.I2C_Config.I2C_Mode = I2C_MODE_STANDARD;

	I2C_MasterInit(&hi2c1);

}

int main(void){

	I2C1_GPIOInit();
	I2C1_INIT();

	I2C_MasterSendData(&hi2c1, user_data, strlen(user_data), 0x68);


	while(1){


	}

}




