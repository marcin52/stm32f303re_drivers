/*
 *
 *  Created on: 8 kwi 2020
 *      Author: marci
 */
#include <string.h>
#include "stm32f303xx.h"

// For belwo pins alternate function mode: AF5
//PB15 --> SPI2_MOSI
//PB14 --> SPI2_MISO
//PB13 --> SPI2_SCK
//PB12 --> SPI2_NSS

//command codes
#define COMMAND_LED_CTRL      		0x50
#define COMMAND_SENSOR_READ      	0x51
#define COMMAND_LED_READ      		0x52
#define COMMAND_PRINT      			0x53
#define COMMAND_ID_READ      		0x54

#define LED_ON     1
#define LED_OFF    0

//arduino analog pins
#define ANALOG_PIN0 	0
#define ANALOG_PIN1 	1
#define ANALOG_PIN2 	2
#define ANALOG_PIN3 	3
#define ANALOG_PIN4 	4

//arduino led

#define LED_PIN  9

void SPI2_GPIOInit(void){
	GPIO_Handle_t SPIgpio;

	SPIgpio.pGPIOx = GPIOB;
	SPIgpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	SPIgpio.GPIO_PinConfig.GPIO_PinAltFunMode = 5;
	SPIgpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	SPIgpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PU;
	SPIgpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;

	//MOSI
	SPIgpio.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_15;
	GPIO_Init(&SPIgpio);
	//MISO
	SPIgpio.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_14;
	GPIO_Init(&SPIgpio);
	//SCK
	SPIgpio.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_13;
	GPIO_Init(&SPIgpio);
	//NSS
	SPIgpio.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_12;
	GPIO_Init(&SPIgpio);
}

void SPI2_Init(void){

	SPI_Handle_t SPI2handle;

	SPI2handle.pSPIx = SPI2;
	SPI2handle.SPIConfig.SPI_BusConfig = SPI_BUS_FULLDUPLEX;
	SPI2handle.SPIConfig.SPI_DeviceMode = SPI_DEVICE_MODE_MASTER;
	SPI2handle.SPIConfig.SPI_SclkSpeed = SPI_SPEED_DIV8; // 2 MHz SPI_SCK
	SPI2handle.SPIConfig.SPI_DFF = SPI_DFF_8BITS;
	SPI2handle.SPIConfig.SPI_CPOL = SPI_CPOL_LOW;
	SPI2handle.SPIConfig.SPI_CPHA = SPI_CPHA_LOW;
	SPI2handle.SPIConfig.SPI_SSM = SPI_SSM_DI; // Hardware slave managment for NSS pin



	SPI_Init(&SPI2handle);
}

void delay(){

	for(uint32_t i=0; i<50000; i++);
}

uint8_t SPI_verifyResponse(uint8_t ackbyte){

	if(ackbyte == 0xF5){
		return 1;
	}
	return 0;
}

void GPIO_ButtonInit(){

	GPIO_Handle_t button;

	button.pGPIOx = GPIOC;
	button.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_13;
	button.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IN;
	button.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_NO_PUPD;

	GPIO_Init(&button);
}

uint8_t commandcode = COMMAND_LED_CTRL;
uint8_t ackbyte;
uint8_t args[2];
uint8_t dummy_byte = 0xff;
uint8_t dummy_read;


int main(void){

	SPI2_GPIOInit();
	GPIO_ButtonInit();

	SPI2_Init();

	/*
	 * making SSOE 1 does NSS output enable
	 * The NSS pin is automatically managed by the hardware
	 * i. 3 when SPE = 1, NSS will be pulled to low
	 * and NSS pin will be high when SPE = 0
	 */

	SPI_SSOEConfig(SPI2, ENABLE);


	while(1){
		while( ! GPIO_ReadPin(GPIOC, GPIO_PIN_13)){

			delay();
			delay();
			// enable SPI peripheral
			SPI_PeripheralControl(SPI2, ENABLE);

			// 1.CMD_LED_CTRL <pin no[1]>	<value(1)>

			// send command
			SPI_SendData(SPI2, &commandcode, 1);

			//do dummy read to clear the RXNE
			SPI_ReceiveData(SPI2, &dummy_read, 1);

			//send some dummy bits (1byte) to fetch the response from the slave
			SPI_SendData(SPI2, &dummy_byte, 1);
			//read the ack byte received
			SPI_ReceiveData(SPI2, &ackbyte, 1);

			if(SPI_verifyResponse(ackbyte)){
				//send arguments
				args[0]= LED_PIN;
				args[1]= LED_ON;

				//send arguments
				SPI_SendData(SPI2, args, 2);
			}


			// Confirm that SPI is not busy
			while(SPI_GetFlagStatus(SPI2, SPI_BSY_FLAG));

			// disable SPI peripheral
			SPI_PeripheralControl(SPI2, DISABLE);
		}
	}





	return 0;
}
