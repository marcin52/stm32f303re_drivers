/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f303xx.h"

void togglePin(){
	GPIOA->ODR ^= GPIO_ODR_5;
};

void TIM2_IRQHandler(void){
	if (0 != (TIM_SR_UIF & TIM2->SR)){
		TIM2->SR &= ~TIM_SR_UIF;
		uint8_t state=0;
		if (( GPIOC->IDR & GPIO_IDR_13 ) != 0)
			state=1;
		if (( GPIOC->IDR & GPIO_IDR_13 ) == 0 && state == 1){
			state=0;
			togglePin();
		}
	}
};

int main(void)
{
	uint32_t temp;
	//RCC->CR |= RCC_CR_HSION;
	while ((RCC->CR & RCC_CR_HSIRDY) == 0){

	}

//	RCC->CFGR |= RCC_CFGR_SW_0;
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	TIM2->DIER |= TIM_DIER_UIE;
	TIM2->CR1 |= TIM_CR1_URS;
	TIM2->CR1 |= TIM_CR1_ARPE;
	TIM2->ARR = 64-1;
	TIM2->PSC = 15625-1;
	TIM2->CR1 |= TIM_CR1_CEN;

	temp = GPIOA->MODER;
	temp = temp & ( ~(GPIO_MODER_MODER5_1 | GPIO_MODER_MODER5_0) );
	temp = temp | GPIO_MODER_MODER5_0;			//output mode
	GPIOA->MODER = temp;

	temp = GPIOA->OTYPER;
	temp = temp & (~GPIO_OTYPER_OT_5);
	GPIOA->OTYPER = temp;		//push-pull

	temp = GPIOA->OSPEEDR;
	temp = temp & (~GPIO_OSPEEDER_OSPEEDR5_Msk);
	temp = temp | GPIO_OSPEEDER_OSPEEDR5_1;
	GPIOA->OSPEEDR = temp; 			//medium speed

	temp = GPIOC->MODER;
	temp = temp & ( ~(GPIO_MODER_MODER13_1 | GPIO_MODER_MODER13_0) );
	GPIOC->MODER = temp;

	NVIC_IRQInterruptConfig(TIM2_IRQn, ENABLE);

	for(;;);
}
