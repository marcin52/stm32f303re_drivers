/*
 * stm32f303xx_rcc.c
 *
 *  Created on: Apr 18, 2020
 *      Author: marci
 */

#include "stm32f303cc_rcc.h"

/******************************************************************************************
 *		APIs to oscillators start, stop and wait until ready
 ******************************************************************************************/

inline void RCC_StartHSI(){
	RCC->CR |= RCC_CR_HSION;
}

inline void RCC_StopHSI(){
	RCC->CR &= ~(RCC_CR_HSION);
}

inline void RCC_StartHSE(){
	RCC->CR |= RCC_CR_HSEON;
}

inline void RCC_StopHSE(){
	RCC->CR &= ~(RCC_CR_HSEON);
}

inline void RCC_StartHSE_BYPASS(){
	RCC->CR |= RCC_CR_HSEBYP;
	RCC_StartHSE();
}

inline void RCC_StopHSE_BYPASS(){
	RCC->CR &= ~(RCC_CR_HSEBYP);
	RCC_StopHSE();
}

inline void RCC_StartLSI(){
	RCC->CSR |= RCC_CSR_LSION;
}

inline void RCC_StopLSI(){
	RCC->CSR &= ~(RCC_CSR_LSION);
}

inline void RCC_StartLSE(){
	RCC->BDCR |= RCC_BDCR_LSE;
}

inline void RCC_StopLSE(){
	RCC->BDCR &= ~(RCC_BDCR_LSE);
}

inline void RCC_StartLSE_BYPASS(){
	RCC->BDCR |= RCC_BDCR_LSEBYP;
	RCC_StartHSE();
}

inline void RCC_StopLSE_BYPASS(){
	RCC->BDCR &= ~(RCC_BDCR_LSEBYP);
	RCC_StopLSE();
}

inline void RCC_StartPLL(){
	RCC->CR |= RCC_CR_PLLON;
}

inline void RCC_StopPLL(){
	RCC->CR &= ~(RCC_CR_PLLON);
}

inline void RCC_WaitUntilHSI_Ready(){

	while(!(RCC->CR & RCC_FLAG_HSIRDY)){
		//yet only infinite no operation is implemented
		//here should be a counter
	}
}

inline void RCC_WaitUntilHSE_Ready(){

	while(!(RCC->CR & RCC_FLAG_HSERDY)){
		//yet only infinite no operation is implemented
		//here should be a counter
	}
}
inline void RCC_WaitUntilPLL_Ready(){

	while(!(RCC->CR & RCC_FLAG_PLLRDY)){
		//yet only infinite no operation is implemented
		//here should be a counter
	}
}

inline void RCC_WaitUntilLSI_Ready(){

	while(!(RCC->CSR & RCC_FLAG_LSIRDY)){
		//yet only infinite no operation is implemented
		//here should be a counter
	}
}

inline void RCC_WaitUntilLSE_Ready(){

	while(!(RCC->BDCR & RCC_FLAG_LSERDY)){
		//yet only infinite no operation is implemented
		//here should be a counter
	}
}

/******************************************************************************************
 *				Initialization of oscillators
 ******************************************************************************************/

void RCC_HSI_Init(){
	//RCC_ConfigureHSI_TrimmingValue(0x0F);
	RCC_StartHSI();
	RCC_WaitUntilHSI_Ready();
}

void RCC_HSE_Init(){
	RCC_StartHSE();
	RCC_WaitUntilHSE_Ready();
}

void RCC_HSE_BYPASS_Init(){
	RCC_StartHSE_BYPASS();
	RCC_WaitUntilHSE_Ready();
}

void RCC_PLL_Init(uint8_t PLL_Multiplier, uint8_t PLLClockSource){

	RCC_ConfigurePLL(PLL_Multiplier, PLLClockSource);
	RCC_StartPLL();
	RCC_WaitUntilPLL_Ready();
}

void RCC_LSE_Init(){
	RCC_StartLSE();
	RCC_WaitUntilLSE_Ready();
}

void RCC_LSE_BYPASS_Init(){
	RCC_StartLSE_BYPASS();
	RCC_WaitUntilLSE_Ready();
}

void RCC_LSI_Init(){
	RCC_StartLSI();
	RCC_WaitUntilLSI_Ready();
}

/******************************************************************************************
 *				APIs to configure oscillators
 ******************************************************************************************/

/*	@fn - RCC_ConfigurePLL											*//*
 * 	@param[in] PLL_Multiplier - Can be the value of @PLL_Multiplier
 * 	@param[in] PLLClockSource - Can be the value of @PLLSRC
 *
 * 	@note that this function should be not used after enabling PLL
 * 																	*//*
 */
void RCC_ConfigurePLL(uint8_t PLL_Multiplier, uint8_t PLLClockSource){

	// By default the multiplier equals 2
	RCC->CFGR &= ~(RCC_CFGR_PLLMUL_Msk);
	if(PLL_Multiplier <= PLL_MULTIPLIER_16){
		RCC->CFGR |= (PLL_Multiplier << RCC_CFGR_PLLMUL_Pos);
	}
	// By default PLL clock source is HSI divided by 2
	RCC->CFGR &= ~(RCC_CFGR_PLLSRC_Msk);
	RCC->CFGR2 &= ~(RCC_CFGR2_PREDIV_Msk);

	if(PLLClockSource == PLLSRC_HSI){
		RCC->CFGR |= (PLLClockSource << RCC_CFGR_PLLSRC_Pos);
	} else if(PLLClockSource <= PLLSRC_HSE_DIVIDED_BY_15){
		RCC->CFGR2 |= ((PLLClockSource-PLLSRC_NUMBER_TO_SUBTRACT)<<RCC_CFGR2_PREDIV_Pos);
	}

}

/*	@fn - RCC_ConfigureHSI_Calibration								*//*
 * 	@param[in] calibrationValue - Can be the value between 0x0 and 0xFF
 *
 * 	@note that this function should be not used after enabling HSI
 * 	@note HSI is automatically calibrated during processor startup
 * 																	*//*
 */
inline void RCC_ConfigureHSI_Calibration(uint8_t calibrationVaule){
	RCC->CR &= ~(RCC_CR_HSICAL_Msk);
	RCC->CR |= (calibrationVaule << RCC_CR_HSICAL_Pos);
}
/*	@fn - RCC_ConfigureHSI_TrimmingValue							*//*
 * 	@param[in] calibrationValue - Can be the value between 0x0 and 0x3F
 *
 * 	@note that this function should be not used after enabling HSI
 * 																	*//*
 */
inline void RCC_ConfigureHSI_TrimmingValue(uint8_t trimmingValue){
	RCC->CR &= ~(RCC_CR_HSITRIM_Msk);
	RCC->CR |= (trimmingValue << RCC_CR_HSITRIM_Pos);
}

/******************************************************************************************
 *				System clocks configuration and source switching
 ******************************************************************************************/

/*	@fn - RCC_SetSystemClockSwitch									*//*
 * 	@param[in] clock - Can be the value between of @SYSTEM_CLOCK_SWITCH
 * 																	*//*
 */
void RCC_SetSystemClockSwitch(uint8_t clock){

	// By default HSI is selected as System Clock
	RCC->CFGR &= ~(RCC_CFGR_SW_Msk);
	if(clock <= SYSCLK_PLL){
		RCC->CFGR |= (clock << RCC_CFGR_SW_Pos);
	}
}

void RCC_EnableCSS(uint8_t EnableOrDisable){
	// By default disable CSS
	RCC->CR &= ~(RCC_CR_CSSON);
	if(EnableOrDisable == ENABLE){
		RCC->CR |= RCC_CR_CSSON;
	}
}

/*	@fn - RCC_ConfigureAPB1_Prescaler								*//*
 * 	@param[in] clock - Can be the value between @APB1_PRESCALER
 * 																	*//*
 */
void RCC_ConfigureAPB1_Prescaler(uint8_t APB1_Prescaler){
	// By default sysclk not divided
	RCC->CFGR &= ~(RCC_CFGR_PPRE1_Msk);
	if(APB1_Prescaler <= PPRE1_SYSCLK_DIV_16){
		RCC->CFGR |= (APB1_Prescaler << RCC_CFGR_PPRE1_Pos);
	}

}
/*	@fn - RCC_ConfigureAPB2_Prescaler								*//*
 * 	@param[in] clock - Can be the value between @APB2_PRESCALER
 * 																	*//*
 */
void RCC_ConfigureAPB2_Prescaler(uint8_t APB2_Prescaler){
	// By default sysclk not divided
	RCC->CFGR &= ~(RCC_CFGR_PPRE2_Msk);
	if(APB2_Prescaler <= PPRE2_SYSCLK_DIV_16){
		RCC->CFGR |= (APB2_Prescaler << RCC_CFGR_PPRE2_Pos);
	}
}
/*	@fn - RCC_ConfigureHCLK_Prescaler								*//*
 * 	@param[in] clock - Can be the value between @HCLK_PRESCALER
 * 																	*//*
 */
void RCC_ConfigureHCLK_Prescaler(uint8_t HCLK_Prescaler){
	// By default sysclk not divided
	RCC->CFGR &= ~(RCC_CFGR_HPRE_Msk);
	if(HCLK_Prescaler <= HPRE_SYSCLK_DIV_512){
		RCC->CFGR |= (HCLK_Prescaler << RCC_CFGR_HPRE_Pos);
	}
}
/******************************************************************************************
 *				Peripheral clocks configuration
 ******************************************************************************************/

void RCC_ConfigurePeripheralClock(PeriperalClockSource_t *periphClock);
void RCC_SelectRTC_ClockSource(uint8_t RTC_ClockSource){

	//By default the clock source is none
	RCC->BDCR &= ~(RCC_BDCR_RTCSEL_Msk);

	if(RTC_ClockSource <= RCC_RTC_CLOCKSOURCE_HSE_DIV32){
		RCC->BDCR |= (RTC_ClockSource << RCC_BDCR_RTCSEL_Pos);
	}
}

/******************************************************************************************
 *				Getters of clock values
 ******************************************************************************************/


uint8_t RCC_GetSystemClockSwitch(void){
	uint32_t tempReg;
	tempReg = (RCC->CFGR & RCC_CFGR_SWS_Msk) >> RCC_CFGR_SWS_Pos;
	return (uint8_t) tempReg;
}

uint16_t RCC_GetHCLKPrescaler(void){
	uint32_t tempReg;
	tempReg = (RCC->CFGR & RCC_CFGR_HPRE_Msk);
	if(tempReg == RCC_CFGR_HPRE_DIV512){
		return 512;
	} else if(tempReg == RCC_CFGR_HPRE_DIV256){
		return 256;
	}else if(tempReg == RCC_CFGR_HPRE_DIV128){
		return 128;
	}else if(tempReg == RCC_CFGR_HPRE_DIV64){
		return 64;
	}else if(tempReg == RCC_CFGR_HPRE_DIV16){
		return 16;
	}else if(tempReg == RCC_CFGR_HPRE_DIV8){
		return 8;
	}else if(tempReg == RCC_CFGR_HPRE_DIV4){
		return 4;
	}else if(tempReg == RCC_CFGR_HPRE_DIV2){
		return 2;
	} else{
		return 1;
	}
}

uint8_t RCC_GetAPB1Prescaler(void){
	uint32_t tempReg;
	tempReg = (RCC->CFGR & RCC_CFGR_PPRE1_Msk);
	if(tempReg == RCC_CFGR_PPRE1_DIV16){
		return 16;
	} else if(tempReg == RCC_CFGR_PPRE1_DIV8){
		return 8;
	}else if(tempReg == RCC_CFGR_PPRE1_DIV4){
		return 4;
	}else if(tempReg == RCC_CFGR_PPRE1_DIV2){
		return 2;
	} else{
		return 1;
	}
}

uint8_t RCC_GetAPB2Prescaler(void){
	uint32_t tempReg;
	tempReg = (RCC->CFGR & RCC_CFGR_PPRE2_Msk);
	if(tempReg == RCC_CFGR_PPRE2_DIV16){
		return 16;
	} else if(tempReg == RCC_CFGR_PPRE2_DIV8){
		return 8;
	}else if(tempReg == RCC_CFGR_PPRE2_DIV4){
		return 4;
	}else if(tempReg == RCC_CFGR_PPRE2_DIV2){
		return 2;
	} else{
		return 1;
	}
}

uint32_t RCC_GetSYSCLKValue(void){

	uint32_t sysclk;

	uint8_t clksrc = RCC_GetSystemClockSwitch();

	if(clksrc == SYSCLK_STATUS_HSI){
		sysclk = HSI_FREQUENCY;
	}else if(clksrc == SYSCLK_STATUS_HSE){
		sysclk = HSE_FREQUENCY;
	}else if(clksrc == SYSCLK_STATUS_PLL){
		sysclk = RCC_GetPLLOutput();
	}
	return sysclk;
}

uint32_t RCC_GetHCLKValue(void){

	uint32_t hclk;
	uint32_t sysclk = RCC_GetSYSCLKValue();
	uint8_t prescaler = RCC_GetHCLKPrescaler();
	hclk = sysclk / prescaler;

	return hclk;
}

uint32_t RCC_GetPCLK1Value(void){

	uint32_t pclk1;
	uint32_t sysclk = RCC_GetSYSCLKValue();
	uint8_t prescaler = RCC_GetAPB1Prescaler();
	pclk1 = sysclk / prescaler;

	return pclk1;

}

uint32_t RCC_GetPCLK2Value(void){

	uint32_t pclk1;
	uint32_t sysclk = RCC_GetSYSCLKValue();
	uint8_t prescaler = RCC_GetAPB1Prescaler();
	pclk1 = sysclk / prescaler;

	return pclk1;
}

uint32_t RCC_GetPLLOutput(){

	uint32_t pllOutput;
	uint32_t pllSrc = RCC_GetPLLSource();
	uint8_t pllMultiplier = RCC_GetPLLMultiplier();
	pllOutput = pllSrc * pllMultiplier;

	return pllOutput;

}

uint32_t RCC_GetPLLSource(){

	uint32_t pllSrc;

	//first check if HSI is a PLL source
	uint32_t tempReg = (RCC->CFGR & RCC_CFGR_PLLSRC_Msk) >> RCC_CFGR_PLLSRC_Pos;
	if(tempReg == PLLSRC_HSI){
		pllSrc = HSI_FREQUENCY;
	}else if(tempReg == PLLSRC_HSI_DIVIDED_BY_2){
		pllSrc = HSI_DIV2_FREQUENCY;
	}else{ // Then HSE is a PLL source
		tempReg = (RCC->CFGR2 & RCC_CFGR2_PREDIV_Msk);
		pllSrc = HSE_FREQUENCY / (tempReg + 1);
	}

	return pllSrc;
}

uint8_t RCC_GetPLLMultiplier(){
	uint32_t tempReg;
	tempReg = (RCC->CFGR & RCC_CFGR_PLLMUL_Msk) >> RCC_CFGR_PLLMUL_Pos;
	return (uint8_t) tempReg + 2;
}

/******************************************************************************************
 *				IRQ Configuration and ISR handling
 ******************************************************************************************/

void RCC_EnableInterrupt();
void RCC_IRQHandler();
