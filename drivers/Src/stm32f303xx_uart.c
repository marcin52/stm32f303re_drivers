/*
 * stm32f303xx_uart.c
 *
 *  Created on: 12 lip 2020
 *      Author: marci
 */

#include "stm32f303xx_uart.h"
#include "stm32f303cc_rcc.h"

/******************************************************************************************
 *								Static helper global variables
 ******************************************************************************************/
// theese variables are only implemented to reduce the consumption of static memory
uint8_t interruptEnableStatus;
uint8_t flagStatus;

/******************************************************************************************
 *								Static helper functions implemented by this driver
 ******************************************************************************************/
static void USART_DMA_SendingControl(USART_RegDef_t *pUSARTx, uint8_t enableOrDisable);
static void USART_DMA_ReceivingControl(USART_RegDef_t *pUSARTx, uint8_t enableOrDisable);

static void USART_EnableInterrupt(USART_RegDef_t *pUSARTx, uint8_t interrupt);
static uint8_t USART_CheckInterruptEnableStatus(USART_RegDef_t *pUSARTx, uint8_t interrupt);
static uint8_t USART_CheckInterruptEnableStatusCR3(USART_RegDef_t *pUSARTx, uint8_t interrupt);
static uint8_t USART_GetWordLenght(USART_RegDef_t *pUSARTx);
static void USART_ConfigMode(USART_RegDef_t *pUSARTx, uint8_t usartMode);
static void USART_ConfigWordLength(USART_RegDef_t *pUSARTx, uint8_t wordLength);
static void USART_ConfigParity(USART_RegDef_t *pUSARTx, uint8_t parity);
static void USART_ConfigStopBits(USART_RegDef_t *pUSARTx, uint8_t numberOfStopBits);
static void USART_ConfigHardwareFlowControl(USART_RegDef_t *pUSARTx, uint8_t HF_control);

static void USART_TC_Interrupt(USART_Handle_t *pUSARTHandle);
static void USART_TXE_Interrupt(USART_Handle_t *pUSARTHandle);
static void USART_RXNE_Interrupt(USART_Handle_t *pUSARTHandle);
static void USART_CTS_Interrupt(USART_Handle_t *pUSARTHandle);
static void USART_IDLE_Interrupt(USART_Handle_t *pUSARTHandle);
static void USART_OVERRUN_Interrupt(USART_Handle_t *pUSARTHandle);

static void USART_CheckForTC_Flag(USART_Handle_t *pUSARTHandle);
static void USART_CheckForTXE_Flag(USART_Handle_t *pUSARTHandle);
static void USART_CheckForRXNE_Flag(USART_Handle_t *pUSARTHandle);
static void USART_CheckForCTS_Flag(USART_Handle_t *pUSARTHandle);
static void USART_CheckForIDLE_Flag(USART_Handle_t *pUSARTHandle);
static void USART_CheckForOVERRUN_Flag(USART_Handle_t *pUSARTHandle);
static void USART_CheckForERROR_Flag(USART_Handle_t *pUSARTHandle);

static uint8_t* USART_Send_9bits(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer);
static uint8_t* USART_Send_8bits(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer);
static uint8_t* USART_Receive_8bits(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer);
static uint8_t* USART_Receive_9bits(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer);



/******************************************************************************************
 *								Peripheral Clock setup
 ******************************************************************************************/
void USART_PeriphClockControl(USART_RegDef_t *pUSARTx, uint8_t EnableOrDisable){

	if(EnableOrDisable == ENABLE){
		if(pUSARTx == USART1){
			USART1_PCLK_EN();
		}else if(pUSARTx == USART2){
			USART2_PCLK_EN();
		}else if(pUSARTx == USART3){
			USART3_PCLK_EN();
		}else if(pUSARTx == UART4){
			UART4_PCLK_EN();
		}else if(pUSARTx == UART5){
			UART5_PCLK_EN();
		}

	}else{
		if(pUSARTx == USART1){
			USART1_PCLK_DI();
		}else if(pUSARTx == USART2){
			USART2_PCLK_DI();
		}else if(pUSARTx == USART3){
			USART3_PCLK_DI();
		}else if(pUSARTx == UART4){
			UART4_PCLK_DI();
		}else if(pUSARTx == UART5){
			UART5_PCLK_DI();
		}
	}
}

/******************************************************************************************
 *								Init and Deinit UART
 ******************************************************************************************/

void USART_Init(USART_Handle_t *pUSARTHandle){

	USART_PeriphClockControl(pUSARTHandle->pUSARTx, ENABLE);

	USART_ConfigMode(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_Mode);
	USART_ConfigWordLength(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_WordLength);
	USART_ConfigParity(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_ParityControl);

	USART_ConfigStopBits(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_NoOfStopBits);
	USART_ConfigHardwareFlowControl(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_HWFlowControl);

	USART_SetBaudRate(pUSARTHandle->pUSARTx, pUSARTHandle->USART_Config.USART_Baud);
}
void USART_DeInit(USART_Handle_t *pUSARTHandle);

/******************************************************************************************
 *								Data Send and Receive in polling mode
 ******************************************************************************************/
void USART_SendData(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer, uint32_t Len){

    //Loop over until "Len" number of bytes are transferred
	for(uint32_t i = 0 ; i < Len; i++)
	{
		while(! USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_TXE));

		//Check the USART_WordLength item for 9BIT or 8BIT in a frame
		if(USART_GetWordLenght(pUSARTHandle->pUSARTx) == USART_WORDLEN_9BITS)
		{
			pTxBuffer = USART_Send_9bits(pUSARTHandle, pTxBuffer);
		}
		else
		{
			pTxBuffer = USART_Send_8bits(pUSARTHandle, pTxBuffer);
		}
	}

	// wait till TC flag is set in the ISR
	while( ! USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_TC));
}

void  USART_ReceiveData(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer, uint32_t Len){

	//Loop over until "Len" number of bytes are transferred
	for(uint32_t i = 0 ; i < Len; i++)
	{
		while(! USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_RXNE));

		//Check the USART_WordLength to decide whether we are going to receive 9bit of data in a frame or 8 bit
		if(USART_GetWordLenght(pUSARTHandle->pUSARTx) == USART_WORDLEN_9BITS)
		{
			pRxBuffer = USART_Receive_9bits(pUSARTHandle, pRxBuffer);
		}
		else
		{
			pRxBuffer = USART_Receive_8bits(pUSARTHandle, pRxBuffer);
		}
	}
}
/******************************************************************************************
 *								Data Send and Receive in interrupt mode
 ******************************************************************************************/

uint8_t USART_SendDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t Len){

	uint8_t txstate = pUSARTHandle->TxBusyState;

	if(pUSARTHandle->TxBusyState != USART_BUSY_IN_TX)
	{
		pUSARTHandle->TxLen = Len;
		pUSARTHandle->pTxBuffer = pTxBuffer;
		pUSARTHandle->TxBusyState = USART_BUSY_IN_TX;

		USART_EnableInterrupt(pUSARTHandle->pUSARTx, USART_CR1_TXEIE);
		USART_EnableInterrupt(pUSARTHandle->pUSARTx, USART_CR1_TCIE);
	}
	return txstate;
}
uint8_t USART_ReceiveDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer, uint32_t Len){

	uint8_t rxstate = pUSARTHandle->RxBusyState;

	if(rxstate != USART_BUSY_IN_RX)
	{
		pUSARTHandle->RxLen = Len;
		pUSARTHandle->pRxBuffer = pRxBuffer;
		pUSARTHandle->RxBusyState = USART_BUSY_IN_RX;

		(void)pUSARTHandle->pUSARTx->RDR;
		USART_EnableInterrupt(pUSARTHandle->pUSARTx, USART_CR1_RXNEIE);
	}
	return rxstate;
}
/******************************************************************************************
 *								Data Send and Receive in DMA mode
 ******************************************************************************************/

static void USART_DMA_SendingControl(USART_RegDef_t *pUSARTx, uint8_t enableOrDisable){

	if(enableOrDisable == ENABLE)
		pUSARTx->CR3 |= ( ENABLE << USART_CR3_DMAT_Pos);
	else
		pUSARTx->CR3 &= ~( ENABLE << USART_CR3_DMAT_Pos);

}

static void USART_DMA_ReceivingControl(USART_RegDef_t *pUSARTx, uint8_t enableOrDisable){

	if(enableOrDisable == ENABLE)
		pUSARTx->CR3 |= ( ENABLE << USART_CR3_DMAR_Pos);
	else
		pUSARTx->CR3 &= ~( ENABLE << USART_CR3_DMAR_Pos);

}

uint8_t USART_StartSendingDataDMA(USART_Handle_t *pUSARTHandle, DMA_Channel_RegDef_t * dmaChannel){

	uint8_t txstate = pUSARTHandle->TxBusyState;

	if(txstate != USART_BUSY_IN_TX)
	{

		pUSARTHandle->TxBusyState = USART_BUSY_IN_TX;
		DMA_ChannelControl(dmaChannel, ENABLE);
		USART_DMA_SendingControl(pUSARTHandle->pUSARTx, ENABLE);
		USART_PeripheralControl(pUSARTHandle->pUSARTx, ENABLE);

	}
	return txstate;
}

uint8_t USART_StartReceivingDataDMA(USART_Handle_t *pUSARTHandle, DMA_Channel_RegDef_t * dmaChannel){

	uint8_t rxstate = pUSARTHandle->RxBusyState;

	if(rxstate != USART_BUSY_IN_RX)
	{

		pUSARTHandle->RxBusyState = USART_BUSY_IN_RX;
		DMA_ChannelControl(dmaChannel, ENABLE);
		USART_DMA_ReceivingControl(pUSARTHandle->pUSARTx, ENABLE);
		USART_PeripheralControl(pUSARTHandle->pUSARTx, ENABLE);

	}
	return rxstate;
}
uint8_t USART_SendDataDMA(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel){

	DMA_USART_Config(len, (uint32_t)&(pUSARTHandle->pUSARTx->TDR), (uint32_t)pTxBuffer, dmaChannel);
	uint8_t txstate	= USART_StartSendingDataDMA(pUSARTHandle, dmaChannel);
	return txstate;
}

uint8_t USART_ReceiveDataDMA(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel){

	DMA_USART_Config(len, (uint32_t)&(pUSARTHandle->pUSARTx->RDR), (uint32_t)pTxBuffer, dmaChannel);
	uint8_t txstate	= USART_StartReceivingDataDMA(pUSARTHandle, dmaChannel);
	return txstate;
}

/******************************************************************************************
 *								IRQ Configuration and ISR handling
 ******************************************************************************************/
void USART_IRQHandling(USART_Handle_t *pUSARTHandle){

	USART_CheckForTC_Flag(pUSARTHandle);
	USART_CheckForTXE_Flag(pUSARTHandle);
	USART_CheckForRXNE_Flag(pUSARTHandle);
	USART_CheckForCTS_Flag(pUSARTHandle);
	USART_CheckForIDLE_Flag(pUSARTHandle);
	USART_CheckForOVERRUN_Flag(pUSARTHandle);
	USART_CheckForERROR_Flag(pUSARTHandle);
}


/******************************************************************************************
 *								Other Peripheral Control APIs
 ******************************************************************************************/
uint8_t inline USART_GetFlagStatus(USART_RegDef_t *pUSARTx, uint8_t StatusFlagName){

	if(pUSARTx->ISR & StatusFlagName)
	{
		return SET;
	}
   return RESET;
}

void inline USART_ClearFlag(USART_RegDef_t *pUSARTx, uint16_t StatusFlagName){

	pUSARTx->ICR |= StatusFlagName;
}

void USART_PeripheralControl(USART_RegDef_t *pUSARTx, uint8_t EnableOrDisable){

	if(EnableOrDisable == ENABLE)
	{
		pUSARTx->CR1 |= (1 << USART_CR1_UE_Pos);
	}else
	{
		pUSARTx->CR1 &= ~(1 << USART_CR1_UE_Pos);
	}
}

void USART_SetBaudRate(USART_RegDef_t *pUSARTx, uint32_t BaudRate){

	uint16_t BRR_Reg_value = 0;
	uint32_t APBclock;

	// Check oversampling setting
	if( (1 << USART_CR1_OVER8_Pos) & pUSARTx->CR1){

		// oversampling by 8
		uint16_t tmpVal;

		if( pUSARTx == USART1)
			APBclock = RCC_GetPCLK2Value();
		else
			APBclock = RCC_GetPCLK1Value();

		tmpVal = (uint16_t) (APBclock / BaudRate);
		BRR_Reg_value = (0xFFF0 & tmpVal);
		BRR_Reg_value |= ((0xF & tmpVal) >> 1);

	} else {

		// oversampling by 16

		if( pUSARTx == USART1)
			APBclock = RCC_GetPCLK2Value();
		else
			APBclock = RCC_GetPCLK1Value();

		BRR_Reg_value = (uint16_t) (APBclock / BaudRate);
		pUSARTx->BRR |= BRR_Reg_value;
	}
}

/******************************************************************************************
 *								Application Callbacks
 ******************************************************************************************/
__weak void USART_TransferCpltCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_TransferDataRegisterEmptyCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_ReadDataRegisterEmptyCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_CTS_EventCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_IdleEventCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_OverrunErrorCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_NoiseErrorCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}
__weak void USART_FramingErrorCallback(USART_Handle_t *pUSARTHandle){
	//This is a weak implementation. The application may override this function
}

/******************************************************************************************
 *								Static helper functions
 ******************************************************************************************/
static inline void USART_EnableInterrupt(USART_RegDef_t *pUSARTx, uint8_t interrupt){
	pUSARTx->CR1 |= interrupt;
}
static inline uint8_t USART_CheckInterruptEnableStatus(USART_RegDef_t *pUSARTx, uint8_t interrupt){
	return pUSARTx->CR1 & interrupt;
}

static inline uint8_t USART_CheckInterruptEnableStatusCR3(USART_RegDef_t *pUSARTx, uint8_t interrupt){
	return pUSARTx->CR3 & interrupt;
}

static inline uint8_t USART_GetWordLenght(USART_RegDef_t *pUSARTx){

	uint8_t wordLenght = ((pUSARTx->CR1 & USART_CR1_M1_Msk) >> USART_CR1_M1_Pos);
	return wordLenght;
}

static void USART_ConfigMode(USART_RegDef_t *pUSARTx, uint8_t usartMode){

	if ( usartMode == USART_MODE_ONLY_RX)
	{
		pUSARTx->CR1 |= (1 << USART_CR1_RE_Pos);

	}else if (usartMode == USART_MODE_ONLY_TX)
	{
		pUSARTx->CR1 |= ( 1 << USART_CR1_TE_Pos );

	}else if (usartMode == USART_MODE_TXRX)
	{
		pUSARTx->CR1 |= ( ( 1 << USART_CR1_RE_Pos) | ( 1 << USART_CR1_TE_Pos) );
	}
}

static inline void USART_ConfigWordLength(USART_RegDef_t *pUSARTx, uint8_t wordLength){

	if( wordLength <= USART_WORDLEN_7BITS){
		pUSARTx->CR1 |= (wordLength << USART_CR1_M1_Pos);
	}
}

static void USART_ConfigParity(USART_RegDef_t *pUSARTx, uint8_t parity){

	if ( parity == USART_PARITY_EN_EVEN)
	{
		// By default the parity is even so CR1_PS bit field configuration is not required
		pUSARTx->CR1 |= ( 1 << USART_CR1_PCE_Pos);

	}else if (parity == USART_PARITY_EN_ODD )
	{
		pUSARTx->CR1 |= ( 1 << USART_CR1_PCE_Pos);

		pUSARTx->CR1 |= ( 1 << USART_CR1_PS_Pos);

	}
}

static inline void USART_ConfigStopBits(USART_RegDef_t *pUSARTx, uint8_t numberOfStopBits){

	if(numberOfStopBits <= USART_STOPBITS_1_5){
		pUSARTx->CR2 |= (numberOfStopBits << USART_CR2_STOP_Pos);
	}
}

static void USART_ConfigHardwareFlowControl(USART_RegDef_t *pUSARTx, uint8_t HF_control){

	if ( HF_control == USART_HW_FLOW_CTRL_CTS)
	{
		pUSARTx->CR3 |= ( 1 << USART_CR3_CTSE_Pos);


	}else if (HF_control == USART_HW_FLOW_CTRL_RTS)
	{
		pUSARTx->CR3 |= ( 1 << USART_CR3_RTSE_Pos);

	}else if (HF_control == USART_HW_FLOW_CTRL_CTS_RTS)
	{
		pUSARTx->CR3 |= (( 1 << USART_CR3_CTSE_Pos) | ( 1 << USART_CR3_RTSE_Pos));
	}
}

/** Static helper functions to handle interrupts */

static void USART_TC_Interrupt(USART_Handle_t *pUSARTHandle){

	//close transmission and call application callback if TxLen is zero
	if ( pUSARTHandle->TxBusyState == USART_BUSY_IN_TX)
	{
		//Check the TxLen . If it is zero then close the data transmission
		if( ! pUSARTHandle->TxLen )
		{
			//Implement the code to clear the TC flag
			USART_ClearFlag(pUSARTHandle->pUSARTx, USART_FLAG_TC);

			//clear the TCIE control bit
			pUSARTHandle->pUSARTx->CR1 &= (~(USART_CR1_TCIE));

			//Reset the application state
			pUSARTHandle->TxBusyState = USART_READY;

			//Reset Buffer address to NULL
			pUSARTHandle->pTxBuffer = NULL;

			//Reset the length to zero
			pUSARTHandle->TxLen = 0;

			//Call the application call back with event USART_EVENT_TX_CMPLT
			USART_TransferCpltCallback(pUSARTHandle);
		}
	}
}
static void USART_TXE_Interrupt(USART_Handle_t *pUSARTHandle){

	if(pUSARTHandle->TxBusyState == USART_BUSY_IN_TX)
	{
		//Keep sending data until Txlen reaches to zero
		if(pUSARTHandle->TxLen > 0)
		{
			//Check the USART_WordLength item for 9BIT or 8BIT in a frame
			if(USART_GetWordLenght(pUSARTHandle->pUSARTx) == USART_WORDLEN_9BITS)
			{
				//if 9BIT load the DR with 2bytes masking  the bits other than first 9 bits
				uint16_t *pdata = (uint16_t*) pUSARTHandle->pTxBuffer;
				pUSARTHandle->pUSARTx->TDR = (*pdata & (uint16_t)0x01FF);

				//check for USART_ParityControl
				if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
				{
					//No parity is used in this transfer , so 9bits of user data will be sent
					//Implement the code to increment pTxBuffer twice
					pUSARTHandle->pTxBuffer++;
					pUSARTHandle->pTxBuffer++;
					pUSARTHandle->TxLen-=2;
				}
				else
				{
					//Parity bit is used in this transfer . so 8bits of user data will be sent
					//The 9th bit will be replaced by parity bit by the hardware
					pUSARTHandle->pTxBuffer++;
					pUSARTHandle->TxLen-=1;
				}
			}
			else
			{
				//This is 8bit data transfer
				pUSARTHandle->pUSARTx->TDR = (*pUSARTHandle->pTxBuffer  & (uint8_t)0xFF);

				//Implement the code to increment the buffer address
				pUSARTHandle->pTxBuffer++;
				pUSARTHandle->TxLen-=1;
			}

		}
		if (pUSARTHandle->TxLen == 0 )
		{
			//TxLen is zero
			//Implement the code to clear the TXEIE bit (disable interrupt for TXE flag )
			pUSARTHandle->pUSARTx->CR1 &= ~( 1 << USART_CR1_TXEIE_Pos);
		}
	}
}
static void USART_RXNE_Interrupt(USART_Handle_t *pUSARTHandle){

	if(pUSARTHandle->RxBusyState == USART_BUSY_IN_RX)
	{
		if(pUSARTHandle->RxLen > 0)
		{
			//Check the USART_WordLength to decide whether we are going to receive 9bit of data in a frame or 8 bit
			if(pUSARTHandle->USART_Config.USART_WordLength == USART_WORDLEN_9BITS)
			{
				//We are going to receive 9bit data in a frame

				//Now, check are we using USART_ParityControl control or not
				if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
				{
					//No parity is used , so all 9bits will be of user data

					//read only first 9 bits so mask the DR with 0x01FF
					*((uint16_t*) pUSARTHandle->pRxBuffer) = (pUSARTHandle->pUSARTx->RDR  & (uint16_t)0x01FF);

					//Now increment the pRxBuffer two times
					pUSARTHandle->pRxBuffer++;
					pUSARTHandle->pRxBuffer++;
					pUSARTHandle->RxLen-=2;
				}
				else
				{
					//Parity is used, so 8bits will be of user data and 1 bit is parity
					 *pUSARTHandle->pRxBuffer = (pUSARTHandle->pUSARTx->RDR  & (uint8_t)0xFF);
					 pUSARTHandle->pRxBuffer++;
					 pUSARTHandle->RxLen-=1;
				}
			}
			else
			{
				//We are going to receive 8bit data in a frame

				//Now, check are we using USART_ParityControl control or not
				if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
				{
					//No parity is used , so all 8bits will be of user data

					//read 8 bits from DR
					 *pUSARTHandle->pRxBuffer = (uint8_t) (pUSARTHandle->pUSARTx->RDR  & (uint8_t)0xFF);

				}

				else
				{
					//Parity is used, so , 7 bits will be of user data and 1 bit is parity

					//read only 7 bits , hence mask the DR with 0X7F
					 *pUSARTHandle->pRxBuffer = (uint8_t) (pUSARTHandle->pUSARTx->RDR  & (uint8_t)0x7F);

				}

				//Now , increment the pRxBuffer
				pUSARTHandle->pRxBuffer++;
				 pUSARTHandle->RxLen-=1;
			}


		}//if of >0

		if(! pUSARTHandle->RxLen)
		{
			//disable the rxne
			pUSARTHandle->pUSARTx->CR1 &= ~( 1 << USART_CR1_RXNEIE_Pos );
			pUSARTHandle->RxBusyState = USART_READY;
			USART_ReadDataRegisterEmptyCallback(pUSARTHandle);
		}
	}
}

static inline void USART_CTS_Interrupt(USART_Handle_t *pUSARTHandle){

	//clear the CTS flag in SR
	pUSARTHandle->pUSARTx->ISR &=  ~( 1 << USART_ISR_CTSIF_Pos);

	USART_CTS_EventCallback(pUSARTHandle);
}

static inline void USART_IDLE_Interrupt(USART_Handle_t *pUSARTHandle){

	//clear the IDLE flag
	pUSARTHandle->pUSARTx->ICR |= ( 1 << USART_ICR_IDLECF_Pos);

	USART_IdleEventCallback(pUSARTHandle);
}
static inline void USART_OVERRUN_Interrupt(USART_Handle_t *pUSARTHandle){

	//clear the ORE flag
	pUSARTHandle->pUSARTx->ICR |= ( 1 << USART_ICR_ORECF_Pos);

	USART_OverrunErrorCallback(pUSARTHandle);
}

static inline void USART_CheckForTC_Flag(USART_Handle_t *pUSARTHandle){

	flagStatus = USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_TC);
	interruptEnableStatus = USART_CheckInterruptEnableStatus(pUSARTHandle->pUSARTx, USART_CR1_TCIE_Pos);

	if(flagStatus && interruptEnableStatus )
	{
		USART_TC_Interrupt(pUSARTHandle);
	}
}
static inline void USART_CheckForTXE_Flag(USART_Handle_t *pUSARTHandle){

	flagStatus = USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_TXE);
	interruptEnableStatus = USART_CheckInterruptEnableStatus(pUSARTHandle->pUSARTx, USART_CR1_TXEIE);

	if(flagStatus && interruptEnableStatus )
	{
		USART_TXE_Interrupt(pUSARTHandle);
	}
}
static inline void USART_CheckForRXNE_Flag(USART_Handle_t *pUSARTHandle){

	flagStatus = USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_RXNE);
	interruptEnableStatus = USART_CheckInterruptEnableStatus(pUSARTHandle->pUSARTx, USART_CR1_RXNEIE);

	if(flagStatus && interruptEnableStatus )
	{
		USART_RXNE_Interrupt(pUSARTHandle);
	}
}

static inline void USART_CheckForCTS_Flag(USART_Handle_t *pUSARTHandle){
	//Note : CTS feature is not applicable for UART4 and UART5
	//code to check the state of CTSE bit in CR3
	uint8_t temp = pUSARTHandle->pUSARTx->CR3 & ( 1 << USART_CR3_CTSE_Pos);

	flagStatus = USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_CTS);
	interruptEnableStatus = USART_CheckInterruptEnableStatusCR3(pUSARTHandle->pUSARTx, USART_CR3_CTSIE);

	if(flagStatus  && interruptEnableStatus && temp)
	{
		USART_CTS_Interrupt(pUSARTHandle);
	}
}

static inline void USART_CheckForIDLE_Flag(USART_Handle_t *pUSARTHandle){

	flagStatus = USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_IDLE);
	interruptEnableStatus = USART_CheckInterruptEnableStatus(pUSARTHandle->pUSARTx, USART_CR1_IDLEIE);

	if(flagStatus && interruptEnableStatus)
	{
		USART_IDLE_Interrupt(pUSARTHandle);
	}
}

static inline void USART_CheckForOVERRUN_Flag(USART_Handle_t *pUSARTHandle){

	flagStatus = USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_FLAG_OVERRUN);
	interruptEnableStatus = USART_CheckInterruptEnableStatus(pUSARTHandle->pUSARTx, USART_CR1_RXNEIE);

	if(flagStatus  && interruptEnableStatus )
	{
		USART_OVERRUN_Interrupt(pUSARTHandle);
	}

}

static void USART_CheckForERROR_Flag(USART_Handle_t *pUSARTHandle){
//Noise Flag, Overrun error and Framing Error in multibuffer communication
//The below code will get executed in only if multibuffer mode is used.

	interruptEnableStatus = USART_CheckInterruptEnableStatusCR3(pUSARTHandle->pUSARTx, USART_CR1_RXNEIE);

	if( interruptEnableStatus )
	{
		if(USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_ISR_FE))
		{
			/*
				This bit is set by hardware when a de-synchronization, excessive noise or a break character
				is detected. It is cleared by a software sequence (an read to the USART_SR register
				followed by a read to the USART_DR register).
			*/
			USART_FramingErrorCallback(pUSARTHandle);
		}

		if(USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_ISR_NE))
		{
			/*
				This bit is set by hardware when noise is detected on a received frame. It is cleared by a
				software sequence (an read to the USART_SR register followed by a read to the
				USART_DR register).
			*/
			USART_NoiseErrorCallback(pUSARTHandle);
		}

		if(USART_GetFlagStatus(pUSARTHandle->pUSARTx, USART_ISR_ORE) )
		{
			USART_OverrunErrorCallback(pUSARTHandle);
		}
	}
}

/** Static helper functions to send and receive data */

static uint8_t* USART_Send_9bits(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer){

	//if 9BIT load the DR with 2bytes masking  the bits other than first 9 bits
	uint16_t* pdata = (uint16_t*) pTxBuffer;
	pUSARTHandle->pUSARTx->TDR = (*pdata & (uint16_t)0x01FF); // 0x01FF is a mask to mask 9 bit from uint16_t

	//check for USART_ParityControl
	if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
	{
		//No parity is used in this transfer , so 9bits of user data will be sent
		pTxBuffer++;
		pTxBuffer++;
	}
	else
	{
		//Parity bit is used in this transfer . so 8bits of user data will be sent
		//The 9th bit will be replaced by parity bit by the hardware
		pTxBuffer++;
	}
	return pTxBuffer;
}

static inline uint8_t* USART_Send_8bits(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer){

	pUSARTHandle->pUSARTx->TDR = (*pTxBuffer  & (uint8_t)0xFF);

	return ++pTxBuffer;
}
static uint8_t* USART_Receive_8bits(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer){

	//Now, check are we using USART_ParityControl control or not
	if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
	{
		//No parity is used , so all 8bits will be of user data
		 *pRxBuffer = (uint8_t) (pUSARTHandle->pUSARTx->RDR  & (uint8_t)0xFF);
	}

	else
	{
		//Parity is used, so , 7 bits will be of user data and 1 bit is parity
		//read only 7 bits , hence mask the DR with 0X7F
		 *pRxBuffer = (uint8_t) (pUSARTHandle->pUSARTx->RDR  & (uint8_t)0x7F);
	}
	//Now increment the pRxBuffer
	return ++pRxBuffer;
}
static uint8_t* USART_Receive_9bits(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer){

	//Now, check are we using USART_ParityControl control or not
	if(pUSARTHandle->USART_Config.USART_ParityControl == USART_PARITY_DISABLE)
	{
		//No parity is used , so all 9bits will be of user data
		//read only first 9 bits so mask the DR with 0x01FF
		*((uint16_t*) pRxBuffer) = (pUSARTHandle->pUSARTx->RDR  & (uint16_t)0x01FF);

		//Now increment the pRxBuffer two times
		pRxBuffer++;
		pRxBuffer++;
	}
	else
	{
		//Parity is used, so 8bits will be of user data and 1 bit is parity
		 *pRxBuffer = (pUSARTHandle->pUSARTx->RDR  & (uint8_t)0xFF);
		 pRxBuffer++;
	}
	return pRxBuffer;
}


