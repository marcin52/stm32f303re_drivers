/*
 * stm32f303xx_gpio.c
 *
 *  Created on: 2 kwi 2020
 *      Author: marci
 */

#include "stm32f303xx_gpio.h"

/*
 * Static helper functions implemented by this driver
 */
static void GPIO_InitSinglePin(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigureAlternate(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigureOutputSpeed(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigurePushPull(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigureOutputType(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigureMode_NO_Interrupt(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigureModeInterrupt(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfigureEXTICR_PortSelection(GPIO_PinConfig_t pinConfig, uint8_t pinNumber);
static void GPIO_ConfiggureEnable_EXTI_InterruptDeliveryIMR(uint8_t pinNumber);
static void GPIO_ConfigureEdgeTriggerDetection(uint8_t pinNumber, uint8_t RisingEdgeEnable, uint8_t FallingEdgeEnable);


/******************************************************************************************
 *								Peripheral Clock setup
 ******************************************************************************************/

void GPIO_PeriphClockControl(GPIO_RegDef_t *pGPIOx, uint8_t ENorDI){

	if(ENorDI == ENABLE){

		if(pGPIOx == GPIOA){
			GPIOA_PCLK_EN();
		}else if(pGPIOx == GPIOB){
			GPIOB_PCLK_EN();
		}else if(pGPIOx == GPIOC){
			GPIOC_PCLK_EN();
		}else if(pGPIOx == GPIOD){
			GPIOD_PCLK_EN();
		}else if(pGPIOx == GPIOE){
			GPIOE_PCLK_EN();
		}else if(pGPIOx == GPIOF){
			GPIOF_PCLK_EN();
		}else if(pGPIOx == GPIOG){
			GPIOG_PCLK_EN();
		}else if(pGPIOx == GPIOH){
			GPIOH_PCLK_EN();
		}

	}else{
		if(pGPIOx == GPIOA){
			GPIOA_PCLK_DI();
		}else if(pGPIOx == GPIOB){
			GPIOB_PCLK_DI();
		}else if(pGPIOx == GPIOC){
			GPIOC_PCLK_DI();
		}else if(pGPIOx == GPIOD){
			GPIOD_PCLK_DI();
		}else if(pGPIOx == GPIOE){
			GPIOE_PCLK_DI();
		}else if(pGPIOx == GPIOF){
			GPIOF_PCLK_DI();
		}else if(pGPIOx == GPIOG){
			GPIOG_PCLK_DI();
		}else if(pGPIOx == GPIOH){
			GPIOH_PCLK_DI();
		}
	}
}

/******************************************************************************************
 *								Init and Deinit GPIO
 ******************************************************************************************/

/*	@fn - GPIO_Init													 	*
 * 	@param[in] pGPIO - for information about initialization				*
 *						options refer to @GPIO_PinConfig_t structure	*
 * 																		*
 */
void GPIO_Init(GPIO_Handle_t *pGPIO){

	// Enable the peripheral clock by default
	GPIO_PeriphClockControl(pGPIO->GPIO_PinConfig.pGPIOx, ENABLE);

	// This for loop initialize pins from pinNumber table
	// depending on numberOfPinsToConfig value

	for(uint8_t i = 0; i < pGPIO->numberOfPinsToConfig; i++){

		GPIO_InitSinglePin(pGPIO->GPIO_PinConfig, pGPIO->PinNumber[i]);
	}

}

void GPIO_DeInit(GPIO_RegDef_t *pGPIOx){

	if(pGPIOx == GPIOA){
		GPIOA_REG_RESET();
	}else if(pGPIOx == GPIOB){
		GPIOB_REG_RESET();
	}else if(pGPIOx == GPIOC){
		GPIOC_REG_RESET();
	}else if(pGPIOx == GPIOD){
		GPIOD_REG_RESET();
	}else if(pGPIOx == GPIOE){
		GPIOE_REG_RESET();
	}else if(pGPIOx == GPIOF){
		GPIOF_REG_RESET();
	}else if(pGPIOx == GPIOG){
		GPIOG_REG_RESET();
	}else if(pGPIOx == GPIOH){
		GPIOH_REG_RESET();
	}
}

/******************************************************************************************
 *								Data read and write
 ******************************************************************************************/

uint8_t GPIO_ReadPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber){

	uint8_t value;
	value = (uint8_t)(pGPIOx->IDR >> PinNumber) & 0x00000001;
	return value;
}

uint16_t GPIO_ReadPort(GPIO_RegDef_t *pGPIOx){

	uint16_t value;
	value = (uint16_t)pGPIOx->IDR;
	return value;
}

void GPIO_WritePin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t value){

	if(value == GPIO_PIN_SET){
		// write 1 to output data register at the bit of corresponding pin number
		pGPIOx->ODR |= (0x1<<PinNumber);
	}else{
		// write 0
		pGPIOx->ODR &= ~(0x1<<PinNumber);
	}
}

void GPIO_WritePort(GPIO_RegDef_t *pGPIOx, uint16_t value){

	pGPIOx->ODR = value;
}

void GPIO_TogglePin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber){

	pGPIOx->ODR ^= ( 0x1 << PinNumber);
}

/******************************************************************************************
 *								IRQ Configuration and ISR handling
 ******************************************************************************************/

void GPIO_IRQHandling(uint8_t PinNumber){

	GPIO_ClearEXTI_InterruptPendingFlag(PinNumber);
}

void GPIO_ClearEXTI_InterruptPendingFlag(uint8_t PinNumber){

	if( EXTI->PR1 & ( 1 << PinNumber)){
		EXTI->PR1 |= ( 1 << PinNumber);
	}
}

/******************************************************************************************
 *								Static helper functions
 ******************************************************************************************/

static void GPIO_InitSinglePin(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	// Configure the mode of gpio pin
	if(pinConfig.GPIO_PinMode <= GPIO_MODE_ANALOG){
		GPIO_ConfigureMode_NO_Interrupt(pinConfig, pinNumber);
	}else{
		GPIO_ConfigureModeInterrupt(pinConfig, pinNumber);
	}

	GPIO_ConfigureOutputSpeed(pinConfig, pinNumber);
	GPIO_ConfigurePushPull(pinConfig, pinNumber);
	GPIO_ConfigureOutputType(pinConfig, pinNumber);

	// Configure alternate of the gpio pin
	if(pinConfig.GPIO_PinMode == GPIO_MODE_AF){
		GPIO_ConfigureAlternate(pinConfig, pinNumber);
	}
}

static void GPIO_ConfigureAlternate(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	uint8_t AFReg_selection, AFReg_bitShift; // note that in this microcontroller there are two AF registers

	AFReg_selection = pinNumber / 8;
	AFReg_bitShift = 4 * (pinNumber % 8);
	pinConfig.pGPIOx->AFR[AFReg_selection] &= ~(0xF << (AFReg_bitShift)); // clearing
	pinConfig.pGPIOx->AFR[AFReg_selection] |= (pinConfig.GPIO_PinAltFunMode << (AFReg_bitShift)); //setting
}

static void GPIO_ConfigureOutputSpeed(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	pinConfig.pGPIOx->OSPEEDR &= ~(0x3 << (2 * pinNumber)); //clearing
	pinConfig.pGPIOx->OSPEEDR |= (pinConfig.GPIO_PinSpeed << (2 * pinNumber));

}

static void GPIO_ConfigurePushPull(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	pinConfig.pGPIOx->PUPDR &= ~(0x3 << (2 * pinNumber)); //clearing
	pinConfig.pGPIOx->PUPDR |= (pinConfig.GPIO_PinPuPdControl << (2 * pinNumber));; //setting
}

static void GPIO_ConfigureOutputType(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	pinConfig.pGPIOx->OTYPER &= ~(0x1 << ( pinNumber)); //clearing
	pinConfig.pGPIOx->OTYPER |= (pinConfig.GPIO_PinOPType << ( pinNumber)); //setting
}

static void GPIO_ConfigureMode_NO_Interrupt(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	pinConfig.pGPIOx->MODER &= ~(0x3 << (2 * pinNumber)); //clearing
	pinConfig.pGPIOx->MODER |= (pinConfig.GPIO_PinMode << (2 * pinNumber));
}
static void GPIO_ConfigureModeInterrupt(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	if(pinConfig.GPIO_PinMode == GPIO_MODE_IT_FALLING_EDGE){

		GPIO_ConfigureEdgeTriggerDetection(pinNumber, DISABLE, ENABLE);

	}else if (pinConfig.GPIO_PinMode == GPIO_MODE_IT_RAISING_EDGE){

		GPIO_ConfigureEdgeTriggerDetection(pinNumber, ENABLE, DISABLE);

	}else if(pinConfig.GPIO_PinMode == GPIO_MODE_IT_RAISING_FALLING_EDGE){

		GPIO_ConfigureEdgeTriggerDetection(pinNumber, ENABLE, ENABLE);

	}

	GPIO_ConfigureEXTICR_PortSelection(pinConfig, pinNumber);
	GPIO_ConfiggureEnable_EXTI_InterruptDeliveryIMR(pinNumber);
}

static void GPIO_ConfigureEXTICR_PortSelection(GPIO_PinConfig_t pinConfig, uint8_t pinNumber){

	uint8_t EXTICRegSelection, EXTICRegBitShift;
	EXTICRegSelection = pinNumber / 4;
	EXTICRegBitShift = 4 * (pinNumber % 4);
	uint8_t portcode = GPIO_VALUE_TO_EXTICR(pinConfig.pGPIOx);
	SYSCFG_PCLK_EN();
	SYSCFG->EXTICR[EXTICRegSelection] |= ( portcode << (EXTICRegBitShift));
}

static void GPIO_ConfiggureEnable_EXTI_InterruptDeliveryIMR(uint8_t pinNumber){

	EXTI->IMR1 |= ( 1 << pinNumber);
}

/*	@fn - GPIO_ConfigureEdgeTriggerDetection								*
 * 	@param[in] pinNumber - number of corresponding GPIO pin (from 0 to 15)	*
 *																			*
 * 	@param[in] RisingEdgeEnable - if this parameter equals ENABLE this		*
 * 				function will activate rising edge detection. Otherwise		*
 *				rising edge detection will be deactivated					*
 *																			*
 *	@param[in] FallingEdgeEnable - if this parameter equals ENABLE this		*
 * 				function will activate falling edge detection. Otherwise	*
 *				falling edge detection will be deactivated					*
 * 																			*
 */
static void GPIO_ConfigureEdgeTriggerDetection(uint8_t pinNumber, uint8_t RisingEdgeEnable, uint8_t FallingEdgeEnable){

	//By default clear edge detection bits
	EXTI->FTSR1 &= ~( 1 << pinNumber);
	EXTI->RTSR1 &= ~( 1 << pinNumber);

	if(RisingEdgeEnable == ENABLE){
		EXTI->RTSR1 |= ( 1 << pinNumber);
	}
	if(FallingEdgeEnable == ENABLE){
		EXTI->FTSR1 |= ( 1 << pinNumber);
	}
}
