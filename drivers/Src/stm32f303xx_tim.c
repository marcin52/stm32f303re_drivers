/*
 * stm32f303xx_tim.c
 *
 *  Created on: 2 maj 2020
 *      Author: marci
 */

#include "stm32f303xx_tim.h"

/******************************************************************************************
 *								Timer periph clock control
 ******************************************************************************************/

void TIM_PeriphClockControl(TIM_RegDef_t *pTIMx, uint8_t EnableOrDisable){
	if(EnableOrDisable == ENABLE){

		if(pTIMx == TIM1){
			TIM1_PCLK_EN();
		}else if(pTIMx == TIM2){
			TIM2_PCLK_EN();
		}else if(pTIMx == TIM3){
			TIM3_PCLK_EN();
		}else if(pTIMx == TIM4){
			TIM4_PCLK_EN();
		}else if(pTIMx == TIM6){
			TIM6_PCLK_EN();
		}else if(pTIMx == TIM7){
			TIM7_PCLK_EN();
		}else if(pTIMx == TIM8){
			TIM8_PCLK_EN();
		}else if(pTIMx == TIM15){
			TIM15_PCLK_EN();
		}else if(pTIMx == TIM16){
			TIM16_PCLK_EN();
		}else if(pTIMx == TIM17){
			TIM17_PCLK_EN();
		}else if(pTIMx == TIM20){
			TIM20_PCLK_EN();
		}

	}else{
		if(pTIMx == TIM1){
			TIM1_PCLK_DI();
		}else if(pTIMx == TIM2){
			TIM2_PCLK_DI();
		}else if(pTIMx == TIM3){
			TIM3_PCLK_DI();
		}else if(pTIMx == TIM4){
			TIM4_PCLK_DI();
		}else if(pTIMx == TIM6){
			TIM6_PCLK_DI();
		}else if(pTIMx == TIM7){
			TIM7_PCLK_DI();
		}else if(pTIMx == TIM8){
			TIM8_PCLK_DI();
		}else if(pTIMx == TIM15){
			TIM15_PCLK_DI();
		}else if(pTIMx == TIM16){
			TIM16_PCLK_DI();
		}else if(pTIMx == TIM17){
			TIM17_PCLK_DI();
		}else if(pTIMx == TIM20){
			TIM20_PCLK_DI();
		}
	}
}

/******************************************************************************************
 *								Timer Init and Deinit
 ******************************************************************************************/

void TIM_TimeBaseInit(TIM_RegDef_t *Timer, TIM_TimeBaseInit_t *pTIMh){
	TIM_SetPrescaler(Timer, pTIMh->prescaler);
	TIM_SetAutoReload(Timer, pTIMh->autoReloadRegister);
	TIM_Configure_CountDirection(Timer, pTIMh->direction);
	TIM_Configure_SoftwareUpdateMode(Timer, pTIMh->softwareCounterUpdateGeneration);
	TIM_Configure_AutoReloadPreload(Timer, pTIMh->autoReloadRegister);
	TIM_Configure_DMARequest(Timer, pTIMh->TIM_DMARequestEnable);
	TIM_Configure_Interrupt(Timer, ENABLE);
}

void TIM_TimeBaseDeInit(TIM_RegDef_t *Timer){
	// TODO clear registers
	TIM_StopCounting(Timer);
}

void TIM_OutputModeInit(TIM_RegDef_t *Timer, TIM_OutputCompareInit_t *pTIMh){

	TIM_PeriphClockControl(Timer, ENABLE);

	TIM_TimeBaseInit(Timer, &(pTIMh->timeBase));

	TIM_ConfigureChannelAsOutput(Timer, pTIMh->timChannel);
	TIM_ConfigureOutputMode(Timer, pTIMh);
	TIM_EnableCaptureCompare(Timer, pTIMh->timChannel);
	TIM_Configure_AutoReloadPreload(TIM7, ENABLE);
	TIM_EnableOutputComparePreload(Timer, pTIMh->timChannel);
	TIM_EnableMainOutput(Timer);
	TIM_GenerateRegistersUpdate(Timer);

}

void TIM_OutputModeDeInit(TIM_RegDef_t *Timer);

inline void TIM_StartCounting(TIM_RegDef_t *Timer){
	Timer->CR1 |= TIM_CR1_CEN;
}

inline void TIM_StopCounting(TIM_RegDef_t *Timer){
	Timer->CR1 &= ~(TIM_CR1_CEN);
}

/******************************************************************************************
 *								Timer output compare configuration
 ******************************************************************************************/

inline void TIM_EnableMainOutput(TIM_RegDef_t *Timer){

	Timer->BDTR |= (1 << TIM_BDTR_MOE_Pos);
}

void TIM_ConfigureOutputMode(TIM_RegDef_t *Timer, TIM_OutputCompareInit_t *pTIMh){

	__vo uint32_t *channelConfigureReg = TIM_GET_CCMR_REG(pTIMh->timChannel, Timer);
	uint8_t channelBitShift = TIM_GET_CCMR_BIT_SHIFT(pTIMh->timChannel);
	uint8_t outputModeBitShift = channelBitShift + 4;

	if(outputModeBitShift != 36 && channelConfigureReg != 0){
		*channelConfigureReg &= ~(0x0111 << outputModeBitShift);
		*channelConfigureReg |= (pTIMh->outputMode << outputModeBitShift);
	}

}

void TIM_ConfigureChannelAsOutput(TIM_RegDef_t *Timer, uint8_t timChannel){

	__vo uint32_t *channelConfigureReg = TIM_GET_CCMR_REG(timChannel, Timer);
	uint8_t channelBitShift = TIM_GET_CCMR_BIT_SHIFT(timChannel);
	uint8_t outputConfigureBitShift = channelBitShift + 0;

	if(outputConfigureBitShift != 33 && channelConfigureReg != 0){
		*channelConfigureReg &= ~(0x1 << outputConfigureBitShift); //clearing channel config select bits
		*channelConfigureReg |= ( TIM_CAPTURE_COMPARE_SELECTION_OUTPUT << outputConfigureBitShift);
	}

}

void TIM_SetCompareValue(TIM_RegDef_t *Timer, uint8_t timChannel, uint16_t compareValue){

	__vo uint32_t* CCR_Register = TIM_GER_CCR_REG(timChannel, Timer);

	if(CCR_Register != 0 ){
		*CCR_Register &= (0xFFFF0000);  // clearing compareValue bits ( from 0 to 15)
		*CCR_Register |= compareValue;
	}
}

uint16_t TIM_GetCompareValue(TIM_RegDef_t *Timer, uint8_t timChannel){

	__vo uint32_t* CCR_Register = TIM_GER_CCR_REG(timChannel, Timer);
	uint16_t compareValue = 0;

	if(CCR_Register != 0 ){
		compareValue = (uint16_t) *CCR_Register;
	}

	return compareValue;
}

inline void TIM_EnableCaptureCompare(TIM_RegDef_t *Timer, uint8_t timChannel){

	uint8_t captureCompareEnableBitPosition = (timChannel - 1) * 4;

	Timer->CCER |= ( 1 << captureCompareEnableBitPosition);

}

void TIM_EnableOutputComparePreload(TIM_RegDef_t *Timer, uint8_t timChannel){

	__vo uint32_t *enablePreloadRegister = TIM_GET_CCMR_REG(timChannel, Timer);
	uint8_t channelBitShift = TIM_GET_CCMR_BIT_SHIFT(timChannel);
	uint8_t enablePreloadBitShift = channelBitShift + 3;

	if(enablePreloadBitShift != 36 && enablePreloadRegister != 0){
		*enablePreloadRegister |= (1 << enablePreloadBitShift);
	}
}

/******************************************************************************************
 *								Timer time base configuration
 ******************************************************************************************/

inline void TIM_SetPrescaler(TIM_RegDef_t *Timer, uint16_t prescaler){
	Timer->PSC = prescaler;
}

inline void TIM_SetAutoReload(TIM_RegDef_t *Timer, uint16_t autoReload){
	Timer->ARR = autoReload;
}

void TIM_Configure_OnePulseMode(TIM_RegDef_t *Timer, uint8_t EnableOrDisable){
	// By default disable one pulse mode
	Timer->CR1 &= ~(1 << TIM_CR1_OPM_Pos);
	if(EnableOrDisable == ENABLE){
		Timer->CR1 |= (1 << TIM_CR1_OPM_Pos);
	}
}

void TIM_Configure_SoftwareUpdateMode(TIM_RegDef_t *Timer, uint8_t EnableOrDisable){
	// By default software update event generation is disabled
	Timer->CR1 |= TIM_CR1_URS;
	if(EnableOrDisable == ENABLE){
		Timer->CR1 &= ~(TIM_CR1_URS);
	}
}

void TIM_Configure_DMARequest(TIM_RegDef_t *Timer, uint8_t EnableOrDisable){
	// By default DMA request is disabled
	Timer->DIER &= ~(TIM_DIER_UDE);
	if(EnableOrDisable == ENABLE){
		Timer->DIER |= TIM_DIER_UDE;
	}
}

void TIM_Configure_Interrupt(TIM_RegDef_t *Timer, uint8_t EnableOrDisable){
	// By default interrupt generation is disabled
	Timer->DIER &= ~(TIM_DIER_UIE);
	if(EnableOrDisable == ENABLE){
		Timer->DIER |= TIM_DIER_UIE;
	}
}

void TIM_Configure_AutoReloadPreload(TIM_RegDef_t *Timer, uint8_t EnableOrDisable){
	// By default Auto-reload preload is disabled
	Timer->CR1 &= ~(TIM_CR1_ARPE);
	if(EnableOrDisable == ENABLE){
		Timer->CR1 |= (TIM_CR1_ARPE);
	}
}

void TIM_Configure_CountDirection(TIM_RegDef_t *Timer, uint8_t countDirection){
	// By default direction is upcounting
	Timer->CR1 &= ~( 1 << TIM_CR1_DIR_Pos);
	if( countDirection <= TIM_DIRECTION_DOWN_COUNTING){
		Timer->CR1 |= ( countDirection << TIM_CR1_DIR_Pos);
	}

}

/******************************************************************************************
 *								IRQ Configuration and ISR handling
 ******************************************************************************************/

void TIM_IRQHandling(TIM_RegDef_t *Timer){
	TIM_ClearUpdateInterruptFlag(Timer);
	TimerUpdateEventCallback(Timer);
}

/******************************************************************************************
 *								Other Peripheral Control APIs
 ******************************************************************************************/

inline void TIM_GenerateRegistersUpdate(TIM_RegDef_t *Timer){

	Timer->EGR |= (1 << TIM_EGR_UG_Pos);
}

void TIM_SetCounterValue(TIM_RegDef_t *Timer, uint16_t counterValue){

	Timer->CNT = counterValue;
}

uint16_t TIM_GetCounterValue(TIM_RegDef_t *Timer){
	uint8_t counterValue;
	counterValue = Timer->CNT;
	return counterValue;
}

inline void TIM_ClearUpdateInterruptFlag(TIM_RegDef_t *Timer){
	Timer->SR &= ~(TIM_SR_UIF);
}

uint8_t TIM_GetUpdateStatus(TIM_RegDef_t *Timer){
	uint8_t status = (Timer->SR & TIM_SR_UIF);
	return status;
}

/******************************************************************************************
 *								Weak application callbacks
 ******************************************************************************************/

__weak void TimerUpdateEventCallback(TIM_RegDef_t *Timer){

	//This is a weak implementation. The application may override this function
}
