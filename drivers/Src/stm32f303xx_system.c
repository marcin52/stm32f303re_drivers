/*
 * stm32f303xx_system.c
 *
 *  Created on: 4 maj 2020
 *      Author: marci
 */

#include "stm32f303xx_system.h"

/******************************************************************************************
 *									SysTick Init
 ******************************************************************************************/

void SysTick_Init(void){

	uint32_t sysclk = RCC_GetSYSCLKValue();

	// Systick is configured to generete 1 ms timebase

	uint32_t reload = sysclk / SYST_1kHZ_FREQ;
	SysTick_Configure_ReloadValue(reload);
	SysTick_Configure_CalibrationValue(9000);
	SysTick_Configure_ClockSource(SYST_CLOCKSOURCE_PROCESSOR);
	SysTick_Configure_EnableSysTickException();

	NVIC_IRQInterruptConfig(SysTick_IRQn, ENABLE);

	SysTick_ClearCurrentTickValue();
	SysTick_StartTick();

}

void SysTick_StartTick(void){
	*SYST_CSR |= (1 << SYST_CSR_ENABLE_BIT_POS);
}
void SysTick_Configure_EnableSysTickException(){
	*SYST_CSR |= (1 << SYST_CSR_TICKINT_BIT_POS);
}


void SysTick_Configure_ClockSource(uint8_t clockSource){
	// By default clock source is the processor clock
	*SYST_CSR |= (1 << SYST_CSR_CLOCKSOURCE_BIT_POS);

	if(clockSource == DISABLE){
		*SYST_CSR &= ~(1 << SYST_CSR_CLOCKSOURCE_BIT_POS);
	}

}

void SysTick_Configure_ReloadValue(uint32_t reload){
	*SYST_RVR |= (reload << SYST_RVR_RELOAD_BIT_POS);
}

void SysTick_Configure_CalibrationValue(uint32_t calibrationValue){
	*SYST_CALIB |= (calibrationValue << SYST_CALIB_TENMS_BIT_POS);
}

/******************************************************************************************
 *								Other SysTick control APIs
 ******************************************************************************************/

void SysTick_ClearCurrentTickValue(void){
	*SYST_CVR &= ~(SYST_CVR_CURRENT_MASK);
}

uint32_t SysTick_GetCurrentTickValue(void){

	uint32_t currentTick;

	currentTick = *SYST_CVR;
	currentTick &= ~( SYST_CVR_RESERVED_MASK);

	return currentTick;
}

/******************************************************************************************
 *								IRQ Configuration and ISR handling
 ******************************************************************************************/

void NVIC_IRQInterruptConfig(uint8_t IRQNumber, uint8_t EnableOrDisable){
	if(EnableOrDisable == ENABLE)
	{
		if(IRQNumber <= 31)
		{
			//program ISER0 register
			*NVIC_ISER0 |= ( 1 << IRQNumber );

		}else if(IRQNumber > 31 && IRQNumber < 64 ) //32 to 63
		{
			//program ISER1 register
			*NVIC_ISER1 |= ( 1 << (IRQNumber % 32) );
		}
		else if(IRQNumber >= 64 && IRQNumber < 96 )
		{
			//program ISER2 register //64 to 95
			*NVIC_ISER2 |= ( 1 << (IRQNumber % 64) );
		}
	}else
	{
		if(IRQNumber <= 31)
		{
			//program ICER0 register
			*NVIC_ICER0 |= ( 1 << IRQNumber );
		}else if(IRQNumber > 31 && IRQNumber < 64 )
		{
			//program ICER1 register
			*NVIC_ICER1 |= ( 1 << (IRQNumber % 32) );
		}
		else if(IRQNumber >= 64 && IRQNumber < 96 )
		{
			//program ICER2 register
			*NVIC_ICER2 |= ( 1 << (IRQNumber % 64) );
		}
	}
}
void NVIC_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQpriority){
	// Find out the IPR register
	uint8_t iprx = IRQNumber / 4;
	uint8_t iprx_section = IRQNumber % 4;

	uint8_t shift_amount = ( 8 * iprx_section) + ( 8 - NO_PR_BITS_IMPEMENTED);

	*(NVIC_PR_BASE_ADDR + ( iprx * 4 )) &= ~(0xF << (shift_amount)); // clear bits

	*(NVIC_PR_BASE_ADDR +  iprx ) |= (IRQpriority << ( shift_amount)); // set IRQ number

}
void SysTick_IRQHandling(void){

}

/******************************************************************************************
 *								Floating Point Unit Control
 ******************************************************************************************/

inline void FPU_EnableFullControl(){
	SCB->CPACR |= ((3UL << 2*10)|(3UL << 2*11));
}
inline void FPU_DisableFullControl(){
	SCB->CPACR &= ~((3UL << 2*10)|(3UL << 2*11));
}
