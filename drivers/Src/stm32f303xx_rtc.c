/*
 * stm32f303xx_rtc.c
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#include "stm32f303xx_rtc.h"

static void RTC_WaitUntilStartOfInitMode();
static void RTC_WaitUntilWakeUpTimerAllowedToWrite();
static void RTC_WaitForRegisterSynchronization();

/*
 * Init and Deinit RTC
 */

void RTC_PeriphClockControl(uint8_t EnableOrDisable){

	if(EnableOrDisable == ENABLE){
		RTC_PCLK_EN();
	}else{
		RTC_PCLK_DI();
	}
}

void RTC_StartInitMode(){

	RTC->ISR |= ( 1 << RTC_ISR_INIT_Pos);
	RTC_WaitUntilStartOfInitMode();
}

void RTC_StopInitMode(){
	RTC->ISR &= ~( 1 << RTC_ISR_INIT_Pos);
}

void RTC_ProgramPrescalers(uint8_t APrescaler, uint16_t SPrescaler){
	// clear prescalers
	RTC->PRER &= ~(RTC_PRER_PREDIV_A_Msk | RTC_PRER_PREDIV_S_Msk);

	RTC->PRER |= APrescaler << RTC_PRER_PREDIV_A_Pos;
	RTC->PRER |= SPrescaler << RTC_PRER_PREDIV_S_Pos;
}

void RTC_Config_HourFormat(RTC_HourFormat_t HourFormat){

	//By default there is 24 hour format
	RTC->CR &= ~(1 << RTC_CR_FMT_Pos);

	if(HourFormat == RTC_AM_PM_HOUR_FORMAT){
		RTC->CR |= (1 << RTC_CR_FMT_Pos);
	}
}

/*
 * Seting time and date
 */
void RTC_Set_AM_PM(RTC_HOUR_AM_PM_t AM_or_PM){

	//By default the hour is AM
	RTC->TR &= ~(1 << RTC_TR_PM_Pos);

	if(AM_or_PM == RTC_HOUR_PM){
		RTC->TR |= (1 << RTC_TR_PM_Pos);
	}
}

void RTC_SetTime(RTC_time_t * time){

	uint8_t HourTens = time->hours / 10;
	uint8_t HourUnits = time->hours % 10;
	uint8_t MinutesTens = time->minutes / 10;
	uint8_t MinutesUnits = time->minutes % 10;
	uint8_t SecondsTens = time->seconds / 10;
	uint8_t SecondsUnits = time->seconds % 10;

	RTC_Config_HourFormat(time->hourFormat);
	RTC_Set_AM_PM(time->am_or_pm);

	// clear time informations
	//RTC->TR &= ~(RTC_TR_HT_Msk | RTC_TR_HU_Msk /
	//		RTC_TR_MNT_Msk | RTC_TR_MNU_Msk | RTC_TR_ST_Msk | RTC_TR_SU_Msk);

	uint32_t tempreg = 0;
	tempreg |= HourTens << RTC_TR_HT_Pos;
	tempreg |= HourUnits << RTC_TR_HU_Pos;
	tempreg |= MinutesTens << RTC_TR_MNT_Pos;
	tempreg |= MinutesUnits << RTC_TR_MNU_Pos;
	tempreg |= SecondsTens << RTC_TR_ST_Pos;
	tempreg |= SecondsUnits << RTC_TR_SU_Pos;

	RTC->TR = (uint32_t)(tempreg & RTC_TR_RESERVED_MASK);
}
void RTC_SetDate(RTC_date_t * date){

	uint8_t DayTens = date->day / 10;
	uint8_t DayUnits = date->day % 10;
	uint8_t MonthTens = date->month / 10;
	uint8_t MonthUnits = date->month % 10;
	uint8_t YearTens = date->year / 10;
	uint8_t YearUnits = date->year % 10;

	uint32_t tempreg = 0;
	tempreg |= DayTens << RTC_DR_DT_Pos;
	tempreg |= DayUnits << RTC_DR_DU_Pos;
	tempreg |= MonthTens << RTC_DR_MT_Pos;
	tempreg |= MonthUnits << RTC_DR_MU_Pos;
	tempreg |= YearTens << RTC_DR_YT_Pos;
	tempreg |= YearUnits << RTC_DR_YU_Pos;

	tempreg |= date->weekDay << RTC_DR_WDU_Pos;

	RTC->DR = (uint32_t)(tempreg & RTC_DR_RESERVED_MASK);
}

void RTC_GetTime(RTC_time_t * time){

	RTC_WaitForRegisterSynchronization();

	uint32_t tempReg = RTC->TR;

	uint8_t HourTens = (tempReg & RTC_TR_HT) >> RTC_TR_HT_Pos;
	uint8_t HourUnits = (tempReg & RTC_TR_HU) >> RTC_TR_HU_Pos;
	uint8_t MinutesTens = (tempReg & RTC_TR_MNT) >> RTC_TR_MNT_Pos;
	uint8_t MinutesUnits = (tempReg & RTC_TR_MNU) >> RTC_TR_MNU_Pos;
	uint8_t SecondsTens = (tempReg & RTC_TR_ST) >> RTC_TR_ST_Pos;
	uint8_t SecondsUnits = (tempReg & RTC_TR_SU) >> RTC_TR_SU_Pos;

	time->seconds = SecondsUnits + SecondsTens * 10;
	time->minutes = MinutesUnits + MinutesTens * 10;
	time->hours = HourUnits + HourTens * 10;

}
void RTC_GetDate(RTC_date_t * date){

	RTC_WaitForRegisterSynchronization();

	uint32_t tempReg = RTC->DR;

	uint8_t DayTens = (tempReg & RTC_DR_DT) >> RTC_DR_DT_Pos;
	uint8_t DayUnits = (tempReg & RTC_DR_DU) >> RTC_DR_DU_Pos;
	uint8_t MonthTens = (tempReg & RTC_DR_MT) >> RTC_DR_MT_Pos;
	uint8_t MonthUnits = (tempReg & RTC_DR_MU) >> RTC_DR_MU_Pos;
	uint8_t YearTens = (tempReg & RTC_DR_YT) >> RTC_DR_YT_Pos;
	uint8_t YearUnits = (tempReg & RTC_DR_YU) >> RTC_DR_YU_Pos;

	date->weekDay = (tempReg & RTC_DR_WDU) >> RTC_DR_WDU_Pos;
	date->day = DayUnits + DayTens * 10;
	date->month = MonthUnits + MonthTens * 10;
	date->year = YearUnits + YearTens * 10;

}

/*
 * Alarms control
 */

void RTC_SetAlarm(RTC_alarm_t * alarmConfig, RTC_alarms_t alarm){

	uint32_t tempReg = 0;
	if(alarmConfig->secondsMaskEnable == DISABLE){
		uint8_t SecondsTens = alarmConfig->time.seconds / 10;
		uint8_t SecondsUnits = alarmConfig->time.seconds % 10;
		tempReg |= SecondsTens << RTC_ALRMAR_ST_Pos;
		tempReg |= SecondsUnits << RTC_ALRMAR_SU_Pos;
	}else{
		// seconds dont care
		tempReg |= (1 << RTC_ALRMAR_MSK1_Pos);
	}
	if(alarmConfig->minutesMaskEnable == DISABLE){
		uint8_t MinutesTens = alarmConfig->time.minutes / 10;
		uint8_t MinutesUnits = alarmConfig->time.minutes % 10;
		tempReg |= MinutesTens << RTC_ALRMAR_MNT_Pos;
		tempReg |= MinutesUnits << RTC_ALRMAR_MNU_Pos;
	}else{
		// minutes dont care
		tempReg |= (1 << RTC_ALRMAR_MSK2_Pos);
	}
	if(alarmConfig->hoursMaskEnable == DISABLE){
		uint8_t HourTens = alarmConfig->time.hours / 10;
		uint8_t HourUnits = alarmConfig->time.hours % 10;
		tempReg |= HourTens << RTC_ALRMAR_HT_Pos;
		tempReg |= HourUnits << RTC_ALRMAR_HU_Pos;
		if( alarmConfig->time.hourFormat == RTC_AM_PM_HOUR_FORMAT){
			tempReg |= alarmConfig->time.am_or_pm;
		}
	}else{
		// hours dont care
		tempReg |= (1 << RTC_ALRMAR_MSK3_Pos);
	}
	if(alarmConfig->dateMaskEnable == DISABLE){
		// care about date
		uint8_t DayTens = alarmConfig->date.day / 10;
		uint8_t DayUnits = alarmConfig->date.day % 10;
		tempReg |= DayTens << RTC_ALRMAR_DT_Pos;
		tempReg |= DayUnits << RTC_ALRMAR_DU_Pos;
	} else if( alarmConfig->weekDayMaskEnable == DISABLE){
		// care about weekday
		tempReg |= alarmConfig->date.weekDay << RTC_ALRMAR_DU_Pos;
		tempReg |= (1 << RTC_ALRMAR_WDSEL_Pos);
	} else{
		// Date or weekday dont care
		tempReg |= (1 << RTC_ALRMAR_MSK4_Pos);
	}

	if(alarm == RTC_ALARM_A){
		RTC_AlarmA_Control(DISABLE);
		RTC_ClearFlag(RTC_FLAG_ALARM_A);
		RTC->ALRMAR = tempReg;
		RTC_AlarmA_Control(ENABLE);
		RTC_AlarmAinterrupt_Control(ENABLE);
	} else if( alarm == RTC_ALARM_B){
		RTC_AlarmB_Control(DISABLE);
		RTC_ClearFlag(RTC_FLAG_ALARM_B);
		RTC->ALRMBR = tempReg;
		RTC_AlarmB_Control(ENABLE);
		RTC_AlarmBinterrupt_Control(ENABLE);
	}
}
void RTC_AlarmA_Control(uint8_t enableOrDisable){
	//by default disable alarm
	RTC->CR &= ~(1 << RTC_CR_ALRAE_Pos);
	if(enableOrDisable == ENABLE){
		RTC->CR |= (1 << RTC_CR_ALRAE_Pos);
	}
}
void RTC_AlarmB_Control(uint8_t enableOrDisable){
	//by default disable alarm
	RTC->CR &= ~(1 << RTC_CR_ALRBE_Pos);
	if(enableOrDisable == ENABLE){
		RTC->CR |= (1 << RTC_CR_ALRBE_Pos);
	}
}

void RTC_AlarmAinterrupt_Control(uint8_t enableOrDisable){
	//by default disable interrupt
	RTC->CR &= ~(1 << RTC_CR_ALRAIE_Pos);
	if(enableOrDisable == ENABLE){
		RTC->CR |= (1 << RTC_CR_ALRAIE_Pos);
		EXTI->IMR1 |= (1 << EXTI_IMR_MR17_Pos);
		EXTI->EMR1 |= (1 << EXTI_EMR_MR17_Pos);
		EXTI->RTSR1 |= (1 << EXTI_RTSR_TR17_Pos);
	}
}
void RTC_AlarmBinterrupt_Control(uint8_t enableOrDisable){
	//by default disable interrupt
	RTC->CR &= ~(1 << RTC_CR_ALRBIE_Pos);
	if(enableOrDisable == ENABLE){
		RTC->CR |= (1 << RTC_CR_ALRBIE_Pos);
		EXTI->IMR1 |= (1 << EXTI_IMR_MR17_Pos);
		EXTI->EMR1 |= (1 << EXTI_EMR_MR17_Pos);
		EXTI->RTSR1 |= (1 << EXTI_RTSR_TR17_Pos);
	}
}

/*
 * Wakeup timer control
 */
void RTC_WakeupTimerInit(RTC_wakeup_clock_selection_t RTC_clock, uint16_t reloadValue){

	RTC_WakeupTimerControl(DISABLE);
	RTC_WaitUntilWakeUpTimerAllowedToWrite();
	RTC_SelectWakeupTimerClock(RTC_clock);
	RTC_ProgramWakeupTimerReload(reloadValue);
	RTC_WakeupTimerAinterrupt_Control(ENABLE);
	RTC_WakeupTimerControl(ENABLE);
}

void RTC_WakeupTimerControl(uint8_t EnableOrDisable){

	// by default disable wakeup timer
	RTC->CR &= ~(1 << RTC_CR_WUTE_Pos);
	if(EnableOrDisable == ENABLE){
		RTC->CR |= (1 << RTC_CR_WUTE_Pos);
	}
}

void RTC_SelectWakeupTimerClock(RTC_wakeup_clock_selection_t RTC_clock){

	// clear bits
	RTC->CR &= ~( RTC_CR_WUCKSEL_Msk );

	RTC->CR |= (RTC_clock << RTC_CR_WUCKSEL_Pos);
}

void RTC_ProgramWakeupTimerReload(uint16_t reloadValue){

	RTC->WUTR &= ~(RTC_WUTR_WUT_Msk);

	RTC->WUTR |= (reloadValue << RTC_WUTR_WUT_Pos);
}

void RTC_WakeupTimerAinterrupt_Control(uint8_t enableOrDisable){
	//by default disable interrupt
	RTC->CR &= ~(1 << RTC_CR_WUTIE_Pos);
	if(enableOrDisable == ENABLE){

		EXTI->IMR1 |= (1 << EXTI_IMR_MR20_Pos);
		EXTI->EMR1 |= (1 << EXTI_EMR_MR20_Pos);
		EXTI->RTSR1 |= (1 << EXTI_RTSR_TR20_Pos);
		RTC->CR |= (1 << RTC_CR_WUTIE_Pos);
	}
}
/*
 * Write protection control
 */
void RTC_BackUpDomainResetWriteProtectionControl(uint8_t EnableOrDisable){

	if(EnableOrDisable == DISABLE){
		RTC->WPR |= 0xCAU;
		RTC->WPR |= 0x53U;
	} else{
		RTC->WPR &= ~(RTC_WPR_KEY_Msk);
	}
}
/*
 * RTC IQR handling
 */
void RTC_IRQHandler(){

	if(RTC_CheckFlag(RTC_FLAG_WAKEUP)){
		RTC_ClearEXTI_InterruptPendingFlag(RTC_EVENT_WAKEUP_TIMER);
		RTC_ClearFlag(RTC_FLAG_WAKEUP);
		RTC_WakeUp_Callback();

	}else if(RTC_CheckFlag(RTC_FLAG_ALARM_A)){
		RTC_ClearEXTI_InterruptPendingFlag(RTC_EVENT_ALARM);
		RTC_ClearFlag(RTC_FLAG_ALARM_A);
		RTC_AlarmA_Callback();

	} else if(RTC_CheckFlag(RTC_FLAG_ALARM_B)){
		RTC_ClearEXTI_InterruptPendingFlag(RTC_EVENT_ALARM);
		RTC_ClearFlag(RTC_FLAG_ALARM_B);
		RTC_AlarmB_Callback();
	}
}
void RTC_ClearEXTI_InterruptPendingFlag(RTC_events_t EVENT){

	if( EXTI->PR1 & ( EVENT)){
		EXTI->PR1 |= ( EVENT );
	}
}

/*
 * Other APIs
 */

void RTC_ClearFlag(RTC_flags_t flag){
	RTC->ISR &= ~(flag);
}

uint8_t RTC_CheckFlag(RTC_flags_t flag){
	return (RTC->ISR & flag) > 0;
}

/*
 * application callbacks
 */

__weak void RTC_AlarmA_Callback(){
	//This is a weak implementation. The application may override this function
}

__weak void RTC_AlarmB_Callback(){
	//This is a weak implementation. The application may override this function
}

__weak void RTC_WakeUp_Callback(){
	//This is a weak implementation. The application may override this function
}



/* HELPER FUNCTIONS */
static void RTC_WaitUntilStartOfInitMode(){
	while((RTC->ISR & RTC_ISR_INITF) == RESET);
}

static void RTC_WaitUntilWakeUpTimerAllowedToWrite(){
	while((RTC->ISR & RTC_ISR_WUTWF) == RESET);
}

static void RTC_WaitForRegisterSynchronization(){
	while((RTC->ISR & RTC_ISR_RSF) == RESET);
}

