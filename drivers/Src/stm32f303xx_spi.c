/*
 * stm32f303xx_spi.c
 *
 *  Created on: 5 kwi 2020
 *      Author: marci
 */

#include "stm32f303xx_spi.h"

static uint8_t SPI_GetDataSizeFormat(SPI_RegDef_t *pSPIx);
static void spi_txe_interrupt_handle(SPI_Handle_t *pSPIh);
static void spi_rxne_interrupt_handle(SPI_Handle_t *pSPIh);
static void spi_ovr_interrupt_handle(SPI_Handle_t *pSPIh);

/******************************************************************************************
 *								Init and Deinit spi
 ******************************************************************************************/

void SPI_PeriphClockControl(SPI_RegDef_t *pSPIx, uint8_t ENorDI){
	if(ENorDI == ENABLE){

		if(pSPIx == SPI1){
			SPI1_PCLK_EN();
		}else if(pSPIx == SPI2){
			SPI2_PCLK_EN();
		}else if(pSPIx == SPI3){
			SPI3_PCLK_EN();
		}else if(pSPIx == SPI4){
			SPI4_PCLK_EN();
		}
	}else{
		if(pSPIx == SPI1){
			SPI1_PCLK_DI();
		}else if(pSPIx == SPI2){
			SPI2_PCLK_DI();
		}else if(pSPIx == SPI3){
			SPI3_PCLK_DI();
		}else if(pSPIx == SPI4){
			SPI4_PCLK_DI();
		}
	}
}

void SPI_Init(SPI_Handle_t *pSPIh){

	// Enable the peripheral clock by default

	SPI_PeriphClockControl(pSPIh->pSPIx, ENABLE);

	// Do all the configuration

	SPI_ConfigureDeviceMode(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_DeviceMode);
	SPI_ConfigureBus(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_BusConfig);
	SPI_ConfigureSpeed(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_SclkSpeed);
	SPI_ConfigureSoftwareSlaveManagement(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_SSM);
	SPI_ConfigureDataSize(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_DataSize);
	SPI_ConfigureCPOL(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_CPOL);
	SPI_ConfigureCPHA(pSPIh->pSPIx, pSPIh->SPIConfig.SPI_CPHA);
}

/******************************************************************************************
 *								SPI configuration function
 ******************************************************************************************/

void SPI_ConfigureBus(SPI_RegDef_t *pSPIx, uint8_t busConfig){

	uint32_t tempreg = 0;

	if(busConfig == SPI_BUS_FULLDUPLEX){
		// bidi mode should be cleared
		tempreg &= ~(SPI_CR1_BIDIMODE);
	}else if(busConfig== SPI_BUS_HALFDUPLEX){
		// bidi mode should be enabled
		tempreg |= (SPI_CR1_BIDIMODE);
	}else if(busConfig == SPI_BUS_SIMPLEX_RXONLY){
		// bidi mode should be cleared
		tempreg &= ~(SPI_CR1_BIDIMODE);
		// RXONLY bit has to be set
		tempreg |= (SPI_CR1_RXONLY);
	}

	pSPIx->CR1 &= ~(SPI_CR1_BIDIMODE_Msk | SPI_CR1_RXONLY_Msk);

	pSPIx->CR1 |= tempreg;
}

inline void SPI_ConfigureDeviceMode(SPI_RegDef_t *pSPIx, uint8_t deviceMode){
	// By default the mode is master
	pSPIx->CR1 |= SPI_DEVICE_MODE_MASTER << SPI_CR1_MSTR_Pos;
	if(deviceMode == SPI_DEVICE_MODE_SLAVE){
		pSPIx->CR1 &= ~( SPI_CR1_MSTR_Msk );
	}
}

void SPI_ConfigureDataSize(SPI_RegDef_t *pSPIx, uint8_t dataSize){

	pSPIx->CR2 &= ~(0x15 << SPI_CR2_DS_Pos);
	pSPIx->CR2 |= (dataSize << SPI_CR2_DS_Pos);
	if(dataSize == SPI_DFF_8BITS){
		// FRXTH bit must be set to force SPI to generate event after 8 bits run out of FIFO (it is caused by data packing feature)
		pSPIx->CR2 |= SPI_CR2_FRXTH;
	}
}

inline void SPI_ConfigureSoftwareSlaveManagement(SPI_RegDef_t *pSPIx, uint8_t spiSSM){

	// By default SSM is disabled
	pSPIx->CR1 &= ~(SPI_CR1_SSI_Msk);
	if(spiSSM == ENABLE){
		pSPIx->CR1 |= ENABLE << SPI_CR1_SSI_Pos;
	}
}

inline void SPI_ConfigureSpeed(SPI_RegDef_t *pSPIx, uint8_t spiSpeed){

	// By default speed is sclk divided by 2
	pSPIx->CR1 &= ~(SPI_CR1_BR_Msk);
	if( spiSpeed <= SPI_SPEED_DIV256){
		pSPIx->CR1 |= spiSpeed << SPI_CR1_BR_Pos;
	}
}

inline void SPI_ConfigureCPHA(SPI_RegDef_t *pSPIx, uint8_t spiCPHA){

	// By default CPHA is The first clock transition is the first data capture edge
	pSPIx->CR1 &= ~(SPI_CR1_CPHA_Msk);
	if( spiCPHA == ENABLE) {
		pSPIx->CR1 |= ENABLE << SPI_CR1_CPHA_Pos;
	}
}

inline void SPI_ConfigureCPOL(SPI_RegDef_t *pSPIx, uint8_t spiCHPA){

	// By default CK to 0 when idle
	pSPIx->CR1 &= ~(SPI_CR1_CPOL_Msk);
	if( spiCHPA == ENABLE ){
		pSPIx->CR1 |= ENABLE << SPI_CR1_CPOL_Pos;
	}
}

void SPI_DeInit(SPI_RegDef_t *pSPIx){

	if(pSPIx == SPI1){
		SPI1_REG_RESET();
	}else if(pSPIx == SPI2){
		SPI2_REG_RESET();
	}else if(pSPIx == SPI3){
		SPI3_REG_RESET();
	}else if(pSPIx == SPI4){
		SPI4_REG_RESET();
	}
}

/******************************************************************************************
 *								Data send and receive in blocking mode
 ******************************************************************************************/
static inline uint8_t SPI_GetDataSizeFormat(SPI_RegDef_t *pSPIx){

	uint32_t frameFormat = pSPIx->CR2 & SPI_CR2_DS;
	frameFormat = frameFormat >> SPI_CR2_DS_Pos;

	return (uint8_t)frameFormat;
}

void SPI_SendData(SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t len){

	while(len > 0){

		if( SPI_GetDataSizeFormat(pSPIx) == SPI_DFF_16BITS){
			// 16 bit data size
			SPI_Send16bits(pSPIx, *pTxBuffer);
			len = len - 2;
			(uint16_t*)pTxBuffer++;
		}
		else{
			// 8 bit data size
			SPI_Send8bits(pSPIx, *pTxBuffer);
			len--;
			pTxBuffer++;
		}
	}
}

inline void SPI_Send16bits(SPI_RegDef_t *pSPIx, uint16_t data){

	while( SPI_GetFlagStatus(pSPIx, SPI_TXE_FLAG) == FLAG_RESET );
	pSPIx->DR = ((uint16_t)data);

}

void SPI_Send8bits(SPI_RegDef_t *pSPIx, uint8_t data){

	while( SPI_GetFlagStatus(pSPIx, SPI_TXE_FLAG) == FLAG_RESET );
	// Register must be accessed in the 8-bit way (it is caused by data packing feature)
	*(volatile uint8_t*)&pSPIx->DR = data;

}

void SPI_ReceiveData(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t len){

	while(len > 0){

		if(SPI_GetDataSizeFormat(pSPIx) == SPI_DFF_16BITS){
			// 16 bit data frame format
			SPI_Recevive16bits(pSPIx, (uint16_t*)pRxBuffer);
			len = len - 2;
			(uint16_t*)pRxBuffer++;
		}
		else{
			// 8 bit dff
			SPI_Recevive8bits(pSPIx, pRxBuffer);
			len--;
			pRxBuffer++;
		}
	}
}

inline void SPI_Recevive8bits(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer){

	while( SPI_GetFlagStatus(pSPIx, SPI_RXNE_FLAG) == FLAG_RESET );
	*((uint8_t*)pRxBuffer) = pSPIx->DR;
}

inline void SPI_Recevive16bits(SPI_RegDef_t *pSPIx, uint16_t *pRxBuffer){

	while( SPI_GetFlagStatus(pSPIx, SPI_RXNE_FLAG) == FLAG_RESET );
	*pRxBuffer = pSPIx->DR;

}

/******************************************************************************************
 *								Data send and receive in interrupt mode
 ******************************************************************************************/

uint8_t SPI_SendDataIT(SPI_Handle_t *pSPIh, uint8_t *pTxBuffer, uint32_t len){

	uint8_t status = pSPIh->TxState;
	if(status != SPI_BUSY_IN_TX){

		// 1. Save the Tx Buffer address and len information in global variables
		pSPIh->pTxBuffer = pTxBuffer;
		pSPIh->TxLen = len;

		// 2. Mark the SPI state as busy in transsmision so that
		// no other code can take over same SPI peripheral until transsmission is over
		pSPIh->TxState = SPI_BUSY_IN_TX;

		// 3. Enable the TXEIE control bit to get interrupt whenever TXE flag is set in Status Register
		pSPIh->pSPIx->CR2 |= ( SPI_CR2_TXEIE);
	}
	// 4. Data transmission will be handled by the ISR code

	return status;
}

uint8_t SPI_ReceiveDataIT(SPI_Handle_t *pSPIh, uint8_t *pRxBuffer, uint32_t len){

	uint8_t status = pSPIh->RxState;
	if(status != SPI_BUSY_IN_RX){

		// 1. Save the Tx Buffer address and len information in global variables
		pSPIh->pRxBuffer = pRxBuffer;
		pSPIh->RxLen = len;

		// 2. Mark the SPI state as busy in transsmision so that
		// no other code can take over same SPI peripheral until transsmission is over
		pSPIh->RxState = SPI_BUSY_IN_RX;

		// 3. Enable the TXEIE control bit to get interrupt whenever TXE flag is set in Status Register
		pSPIh->pSPIx->CR2 |= ( SPI_CR2_RXNEIE);
	}
	// 4. Data transmission will be handled by the ISR code

	return status;

}

/******************************************************************************************
 *								Data send and receive in dma mode
 ******************************************************************************************/
void SPI_DMA_SendingControl(SPI_RegDef_t *pSPIx, uint8_t enableOrDisable){

	if(enableOrDisable == ENABLE)
		pSPIx->CR2 |= ( ENABLE << SPI_CR2_TXDMAEN_Pos);
	else
		pSPIx->CR2 &= ~( ENABLE << SPI_CR2_TXDMAEN_Pos);

}

void SPI_DMA_ReceivingControl(SPI_RegDef_t *pSPIx, uint8_t enableOrDisable){

	if(enableOrDisable == ENABLE)
		pSPIx->CR2 |= ( ENABLE << SPI_CR2_RXDMAEN_Pos);
	else
		pSPIx->CR2 &= ~( ENABLE << SPI_CR2_RXDMAEN_Pos);

}

void SPI_StartSendingDataDMA(SPI_Handle_t *pSPIh, DMA_Channel_RegDef_t * dmaChannel){

	if(pSPIh->TxState != SPI_BUSY_IN_TX){

		pSPIh->TxState = SPI_BUSY_IN_TX;
		DMA_ChannelControl(dmaChannel, ENABLE);
		SPI_DMA_SendingControl(pSPIh->pSPIx, ENABLE);
		SPI_PeripheralControl(pSPIh->pSPIx, ENABLE);

	}
}

void SPI_StartReceivingDataDMA(SPI_Handle_t *pSPIh, DMA_Channel_RegDef_t * dmaChannel){

	if(pSPIh->RxState != SPI_BUSY_IN_RX){

		pSPIh->RxState = SPI_BUSY_IN_RX;
		DMA_ChannelControl(dmaChannel, ENABLE);
		SPI_DMA_ReceivingControl(pSPIh->pSPIx, ENABLE);
		SPI_PeripheralControl(pSPIh->pSPIx, ENABLE);

	}
}

void SPI_SendDataDMA(SPI_Handle_t *pSPIh, uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel){

	if(pSPIh->TxState != SPI_BUSY_IN_TX){

		DMA_SPI_Config(len, pSPIh->SPIConfig.SPI_DataSize, (uint32_t)&(pSPIh->pSPIx->DR), (uint32_t)pTxBuffer, dmaChannel);
		SPI_StartSendingDataDMA(pSPIh, dmaChannel);

	}
}

void SPI_ReceiveDataDMA(SPI_Handle_t *pSPIh, uint8_t *pRxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel){

	if(pSPIh->RxState != SPI_BUSY_IN_RX){

		DMA_SPI_Config(len, pSPIh->SPIConfig.SPI_DataSize, (uint32_t)pRxBuffer, (uint32_t)&(pSPIh->pSPIx->CR2), dmaChannel);
		SPI_StartReceivingDataDMA(pSPIh, dmaChannel);

	}
}


/******************************************************************************************
 *								IRQ Configuration and ISR handling
 ******************************************************************************************/

void SPI_IRQHandling(SPI_Handle_t *pSPIh){

	uint8_t temp1, temp2;
	// first lets check for TXE
	temp1 = pSPIh->pSPIx->SR & ( SPI_SR_TXE);
	temp2 = pSPIh->pSPIx->CR2 & ( SPI_CR2_TXEIE);

	if( temp1 && temp2){
		// handle TXE
		spi_txe_interrupt_handle(pSPIh);
	}
	//  check for RXE
	temp1 = pSPIh->pSPIx->SR & ( SPI_SR_RXNE);
	temp2 = pSPIh->pSPIx->CR2 & (SPI_CR2_RXNEIE);
	if( temp1 && temp2){
		//handle RXNE
		spi_rxne_interrupt_handle(pSPIh);
	}
	//  check for ovr flag
	temp1 = pSPIh->pSPIx->SR & (SPI_SR_OVR);
	temp2 = pSPIh->pSPIx->CR2 & ( SPI_CR2_ERRIE);
	if( temp1 && temp2){
		//handle RXNE
		spi_ovr_interrupt_handle(pSPIh);
	}
}

/******************************************************************************************
 *								Other Peripheral Control APIs
 ******************************************************************************************/

void SPI_PeripheralControl(SPI_RegDef_t *pSPIx, uint8_t ENorDI){

	if(ENorDI == ENABLE){
		pSPIx->CR1 |= (SPI_CR1_SPE);
	}else {
		pSPIx->CR1 &= ~( SPI_CR1_SPE);
	}
}


void SPI_SSIConfig(SPI_RegDef_t *pSPIx, uint8_t ENorDI){

	if(ENorDI == ENABLE){
		pSPIx->CR1 |= ( SPI_CR1_SSI);
	}else {
		pSPIx->CR1 &= ~(  SPI_CR1_SSI);
	}
}

void SPI_SSOEConfig(SPI_RegDef_t *pSPIx, uint8_t ENorDI){

	if(ENorDI == ENABLE){
		pSPIx->CR2 |= ( SPI_CR2_SSOE);
	}else {
		pSPIx->CR2 &= ~(  SPI_CR2_SSOE);
	}
}

void SPI_CLearOVRFlag(SPI_RegDef_t *pSPIx){

	uint8_t temp;
	temp = pSPIx->DR;
	temp = pSPIx->SR;
	(void)temp;
}

uint8_t SPI_GetFlagStatus(SPI_RegDef_t *pSPIx, uint32_t FlagName){

	if(pSPIx->SR & FlagName){
		return FLAG_SET;
	}
	return FLAG_RESET;
}


void SPI_CloseTransmission(SPI_Handle_t *pSPIh){
	// this prevents interrupts form TXE flag
	pSPIh->pSPIx->CR2 &= ~( SPI_CR2_TXEIE);
	pSPIh->pTxBuffer = NULL;
	pSPIh->TxLen = 0;
	pSPIh->TxState = SPI_READY;
}


void SPI_CloseReception(SPI_Handle_t *pSPIh){
	// this prevents interrupts form RXNE flag
	pSPIh->pSPIx->CR2 &= ~( SPI_CR2_RXNEIE);
	pSPIh->pRxBuffer = NULL;
	pSPIh->RxLen = 0;
	pSPIh->RxState = SPI_READY;
}

/******************************************************************************************
 *								Weak application callbacks
 ******************************************************************************************/

__weak void SPI_ApplicationEventCallback(SPI_Handle_t *pSPIh, uint8_t AppEv){

	//This is a weak implementation. The application may override this function
}

/******************************************************************************************
 *								Static helper functions
 ******************************************************************************************/

static void spi_txe_interrupt_handle(SPI_Handle_t *pSPIh){

	// check the DATASIZE bit in CR2
	if(SPI_GetDataSizeFormat(pSPIh->pSPIx) == SPI_DFF_16BITS){
		// 16 bit data size
		// load data in to the Data Register
		pSPIh->pSPIx->DR = *((uint16_t*)pSPIh->pTxBuffer);
		pSPIh->TxLen--;
		pSPIh->TxLen--;
		(uint16_t*)pSPIh->pTxBuffer++;
	}
	else{
		// 8 bit data size
		// Register must be accessed in the 8-bit way (it is caused by data packing feature)
		*(volatile uint8_t*)&(pSPIh->pSPIx->DR) = *pSPIh->pTxBuffer;
		pSPIh->TxLen--;
		pSPIh->pTxBuffer++;
	}
	if( !pSPIh->TxLen){
		// TxLen is zero so close the transmission and inform the application that Tx is over
		SPI_CloseTransmission(pSPIh);

		SPI_ApplicationEventCallback(pSPIh, SPI_EVENT_TX_CMPLT);
	}
}
static void spi_rxne_interrupt_handle(SPI_Handle_t *pSPIh){

	// check the DATASIZE bit in CR2
	if(SPI_GetDataSizeFormat(pSPIh->pSPIx) == SPI_DFF_16BITS){
		// 16 bit data size
		// read data from the Data Register
		*((uint16_t*)pSPIh->pTxBuffer) = (uint16_t) pSPIh->pSPIx->DR;
		pSPIh->RxLen--;
		pSPIh->RxLen--;
		(uint16_t*)pSPIh->pRxBuffer++;
	}
	else{
		// 8 bit data size
		// Register must be accessed in the 8-bit way (it is caused by data packing feature)
		*(pSPIh->pRxBuffer) = (uint8_t) pSPIh->pSPIx->DR;
		pSPIh->RxLen--;
		pSPIh->pRxBuffer++;
	}
	if( !pSPIh->RxLen){
		// TxLen is zero so close the transmission and inform the application that Tx is over
		SPI_CloseReception(pSPIh);

		SPI_ApplicationEventCallback(pSPIh, SPI_EVENT_RX_CMPLT);
	}
}
static void spi_ovr_interrupt_handle(SPI_Handle_t *pSPIh){

	// 1. Clear the OVR flag
	if(pSPIh->TxState != SPI_BUSY_IN_TX){
		SPI_CLearOVRFlag(pSPIh->pSPIx);
	}

	// 2. Inform the application

	SPI_ApplicationEventCallback(pSPIh, SPI_EVENT_OVR_ERR);
}


