/*
 * stm32f303xx_iwdg.c
 *
 *  Created on: 29 lis 2020
 *      Author: marci
 */

#include "stm32f303xx_iwdg.h"

// PRIVATE FUNCTIONS

static inline void enableIWDG(){
	IWDG->KR |= IWDG_KEY_ENABLE;
}

static inline void enableIWDG_RegistersAccess(){
	IWDG->KR |= IWDG_KEY_REGISTER_ACCESS;
}

static inline void waitForTheRegistersToBeUpdated(){

	while( 0 != IWDG->SR ){

	}
}

static inline void configIWDG_Prescaler(uint8_t prescaler){

	// Clear prescaler register - default value is divided by 4
	IWDG->PR &= ~( IWDG_PR_PR_Msk);

	if( prescaler < IWDG_PRESCALER_DIV_256 ){
		IWDG->PR |= ( prescaler << IWDG_PR_PR_Pos );
	}
}

static inline void programIWDG_Reload( uint16_t reload ){

	// clear reload
	IWDG->RLR &= ~( IWDG_RLR_RL_Msk );
	// Write reload
	IWDG->RLR |= ( reload << IWDG_RLR_RL_Pos );
}

static inline void programIWDG_windowValue(uint16_t window){
	// note that this automatically refresh the IWDG counter on init sequence
	IWDG->WINR |= ( window << IWDG_WINR_WIN_Pos );
}

// PUBLIC FUNCTIONS

void IWDG_Config(uint16_t reload, uint8_t prescaler){
	enableIWDG();
	enableIWDG_RegistersAccess();
	configIWDG_Prescaler(prescaler);
	programIWDG_Reload(reload);
	waitForTheRegistersToBeUpdated();
	IWDG_Refresh();
}
void IWDG_ConfigWindowMode(uint16_t reload, uint8_t prescaler, uint16_t window ){
	enableIWDG();
	enableIWDG_RegistersAccess();
	configIWDG_Prescaler(prescaler);
	programIWDG_Reload(reload);
	waitForTheRegistersToBeUpdated();
	programIWDG_windowValue(window);
}
inline void IWDG_Refresh(){
	IWDG->KR |= IWDG_KEY_COUNTER_REFRESH;
}

