/*
 * stm32f303xx_dma.c
 *
 *  Created on: Apr 30, 2020
 *      Author: marci
 */

#include "stm32f303xx_dma.h"

static uint8_t dma_GetTransferCompleteFlagPos(uint8_t channelNumber);
static uint8_t dma_GetHalfTransferFlagPos(uint8_t channelNumber);
static uint8_t dma_GetTransferErrorPos(uint8_t channelNumber);
static void dma_ClearInterruptFlag(DMA_Handle_t *pDMAh, DMA_FlagTypedef dmaFlagType);
static void dma_TransferCompleteCallback(DMA_Handle_t *pDMAh);
static void dma_HalfTransferCompleteCallback(DMA_Handle_t *pDMAh);
static void dma_TransferErrorCallback(DMA_Handle_t *pDMAh);

/******************************************************************************************
 *		 					Peripheral Clock setup
 ******************************************************************************************/

void DMA_PeriphClockControl(DMA_RegDef_t *pDMAx, uint8_t EnableOrDisable){

	if(EnableOrDisable == ENABLE){
		if(pDMAx == DMA1){
			DMA1_PCLK_ENABLE();
		}else if(pDMAx == DMA2){
			DMA2_PCLK_ENABLE();
		}
	}else{
		if(pDMAx == DMA1){
			DMA1_PCLK_DISABLE();
		}else if(pDMAx == DMA2){
			DMA2_PCLK_DISABLE();
		}
	}
}

/******************************************************************************************
 *		 					Init and Deinit DMA
 ******************************************************************************************/

void DMA_Init(DMA_Handle_t *pDMAh){

	pDMAh->State = DMA_STATE_BUSY; // lock dma to prevent other actions during initialization

	pDMAh->channelNumber = GET_DMA_CHANNEL_NUMBER(pDMAh->DMAChannel);

	DMA_Configure_TransferAdresses(pDMAh->DMAChannel, pDMAh->transferConfig.memoryAddress, pDMAh->transferConfig.peripheralAddress);

	DMA_Configure_NumberOfDataToTransfer(pDMAh->DMAChannel, pDMAh->transferConfig.numberOfDataToTransfer);

	DMA_Configure_ChannelPriority(pDMAh, pDMAh->DMA_Config.dmaChannelPriority);

	DMA_Configure_TransferMode(pDMAh, pDMAh->DMA_Config.transferMode);

	DMA_Configure_TransferDirection(pDMAh->DMAChannel, pDMAh->DMA_Config.direction);

	DMA_Configure_IncrementalMode(pDMAh, pDMAh->DMA_Config.memoryIncrementMode, pDMAh->DMA_Config.peripheralIncrementMode);

	DMA_Configure_TransferDataSize(pDMAh->DMAChannel, pDMAh->DMA_Config.memoryDataSize, pDMAh->DMA_Config.peripheralDataSize);

	DMA_Configure_Interrupts(pDMAh, pDMAh->dmaInterruptsEnable);

	pDMAh->State = DMA_STATE_READY; // Unlock channel

}
void DMA_DeInit(DMA_Handle_t *pDMAh){

	  DMA_ChannelControl(pDMAh->DMAChannel, DISABLE);

	  /* Reset DMA Channel control register */
	  pDMAh->DMAChannel->CCR  = 0U;

	  /* Reset DMA Channel Number of Data to Transfer register */
	  pDMAh->DMAChannel->CNDTR = 0U;

	  /* Reset DMA Channel peripheral address register */
	  pDMAh->DMAChannel->CPAR  = 0U;

	  /* Reset DMA Channel memory address register */
	  pDMAh->DMAChannel->CMAR = 0U;

	  DMA_ClearAllFlags(pDMAh->DMAx);
	  pDMAh->State = DMA_STATE_RESET;

}
/******************************************************************************************
 *		 					Channel configuration APIs
 ******************************************************************************************/
void DMA_Configure_TransferAdresses(DMA_Channel_RegDef_t *DMAChannel, uint32_t memoryAddress, uint32_t peripheralAddrerss){

	DMAChannel->CMAR = memoryAddress;
	DMAChannel->CPAR = peripheralAddrerss;
}

void DMA_Configure_ChannelPriority(DMA_Handle_t *pDMAh, uint8_t priority){

	pDMAh->DMAChannel->CCR &= ~(DMA_CCR_PL_Msk); // By default priority is low

	if(priority <= DMA_CHANNEL_PRIORITY_VERY_HIGH){
		pDMAh->DMAChannel->CCR |= (priority << DMA_CCR_PL_Pos);
	}
}

void DMA_Configure_TransferDataSize(DMA_Channel_RegDef_t *DMAChannel, uint8_t memoryDataSize, uint8_t peripheralDataSize){

	// By default transfer data size is 8 bit
	DMAChannel->CCR &= ~(DMA_CCR_MSIZE_Msk | DMA_CCR_PSIZE_Msk);

	if(memoryDataSize <= DMA_DATA_SIZE_32BIT){
		DMAChannel->CCR |= (memoryDataSize << DMA_CCR_MSIZE_Pos);
	}
	if(peripheralDataSize <= DMA_DATA_SIZE_32BIT){
		DMAChannel->CCR |= (peripheralDataSize << DMA_CCR_PSIZE_Pos);
	}
}
void DMA_Configure_IncrementalMode(DMA_Handle_t *pDMAh, uint8_t memoryIncMode, uint8_t peripheralIncMode){

	// By default disable incremental mode
	pDMAh->DMAChannel->CCR &= ~(DMA_CCR_PINC_Msk | DMA_CCR_MINC_Msk);

	if(memoryIncMode == ENABLE){
		pDMAh->DMAChannel->CCR |= ( ENABLE << DMA_CCR_MINC_Pos);
	}
	if(peripheralIncMode == ENABLE){
		pDMAh->DMAChannel->CCR |= (ENABLE << DMA_CCR_PINC_Pos);
	}
}

void DMA_Configure_NumberOfDataToTransfer(DMA_Channel_RegDef_t *DMAChannel, uint16_t NumberOfData){

	DMAChannel->CNDTR = NumberOfData;
}
void DMA_Configure_Interrupts(DMA_Handle_t *pDMAh, DMA_InterruptsEnableControl dmaInterruptsEnable){

	// By default interrupts are disabled
	pDMAh->DMAChannel->CCR &= ~(DMA_CCR_TCIE_Msk | DMA_CCR_HTIE_Msk | DMA_CCR_TEIE_Msk);
	pDMAh->DMAChannel->CCR |= (dmaInterruptsEnable.transferComplete << DMA_CCR_TCIE_Pos);
	pDMAh->DMAChannel->CCR |= (dmaInterruptsEnable.transferHalfComplete << DMA_CCR_HTIE_Pos);
	pDMAh->DMAChannel->CCR |= (dmaInterruptsEnable.transferError << DMA_CCR_TEIE_Pos);
}
void DMA_Configure_TransferMode(DMA_Handle_t *pDMAh, uint8_t dmaMode){
	// By default mode is normal with direction from peripheral to memory
	pDMAh->DMAChannel->CCR &= ~(DMA_CCR_DIR_Msk);
	pDMAh->DMAChannel->CCR &= ~(DMA_CCR_CIRC_Msk);
	pDMAh->DMAChannel->CCR &= ~(DMA_CCR_MEM2MEM_Msk);

	switch(dmaMode){
		case DMA_MODE_CIRCULAR:
			pDMAh->DMAChannel->CCR |= (DMA_MODE_CIRCULAR << DMA_CCR_CIRC_Pos);
			break;
		case DMA_MODE_MEMORY_TO_MEMORY:
			pDMAh->DMAChannel->CCR |= (DMA_CCR_MEM2MEM);
			break;
		default:
			break;
	}
}

void DMA_Configure_TransferDirection(DMA_Channel_RegDef_t *DMAChannel, uint8_t transferDirection){

	if(transferDirection <= DMA_DIRECTION_MEMORY_TO_PERIPHERAL){
		DMAChannel->CCR |= (transferDirection << DMA_CCR_DIR_Pos);
	}
}
/******************************************************************************************
 *		 					APIs to control data transfer
 ******************************************************************************************/

void DMA_StartTransfer(DMA_Handle_t *pDMAh){

	pDMAh->State = DMA_STATE_BUSY;
	DMA_ChannelControl(pDMAh->DMAChannel, ENABLE);
}
void DMA_StartTransfer_IT(DMA_Handle_t *pDMAh){

	DMA_InterruptsEnableControl dmaInterrupt;
	dmaInterrupt.transferComplete = ENABLE;
	dmaInterrupt.transferError = ENABLE;
	dmaInterrupt.transferHalfComplete = DISABLE;

	pDMAh->State = DMA_STATE_BUSY;

	DMA_Configure_Interrupts(pDMAh, dmaInterrupt);

	DMA_ChannelControl(pDMAh->DMAChannel, ENABLE);
}

void DMA_AbortTransfer(DMA_Handle_t *pDMAh){

	DMA_InterruptsEnableControl dmaInterrupt;
	dmaInterrupt.transferComplete = DISABLE;
	dmaInterrupt.transferError = DISABLE;
	dmaInterrupt.transferHalfComplete = DISABLE;

	DMA_Configure_Interrupts(pDMAh, dmaInterrupt);

	DMA_ChannelControl(pDMAh->DMAChannel, DISABLE);

	DMA_ClearAllFlags(pDMAh->DMAx);

	pDMAh->State = DMA_STATE_READY;
}
void DMA_PollForTransferComplete(DMA_Handle_t *pDMAh, uint8_t maxDelay){

	if(pDMAh->State != DMA_STATE_BUSY){ // No transfer
		return;
	}

	if(pDMAh->DMAChannel->CCR & DMA_CCR_CIRC){ // polling is not supported in circular mode
		return;
	}

	uint8_t transferCompleteFlag = DMA_GetChannelFlagPosition(pDMAh->DMAChannel, DMA_TransferCompleteFlag);
	uint8_t time = 0;

	while(!(pDMAh->DMAx->IFCR & transferCompleteFlag)){
		time++;
		if(time>maxDelay) break;
	}
	pDMAh->State = DMA_STATE_READY;
}

/******************************************************************************************
 *		 					ISR handling
 ******************************************************************************************/

void DMA_IRQHandling(DMA_Handle_t *pDMAh){
	if(DMA_CheckTransferCompleteFlag(pDMAh)){
		// Interrupt was caused by Transfer Complete flag
		dma_TransferCompleteCallback(pDMAh);
	} else if(DMA_CheckTransferErrorFlag(pDMAh)){
		// Interrupt was caused by Transfer Error flag
		dma_TransferErrorCallback(pDMAh);
	} else if(DMA_CheckHalfTransferCompleteFlag(pDMAh)){
		// Interrupt was caused by Half Transfer Complete flag
		dma_HalfTransferCompleteCallback(pDMAh);
	}
}

static void dma_TransferCompleteCallback(DMA_Handle_t *pDMAh){
	dma_ClearInterruptFlag(pDMAh, DMA_TransferCompleteFlag);
	pDMAh->State = DMA_STATE_READY;
	DMA_ApplicationTransferCompleteCallback(pDMAh);
}

static void dma_HalfTransferCompleteCallback(DMA_Handle_t *pDMAh){
	dma_ClearInterruptFlag(pDMAh, DMA_TransferHalfCompleteFlag);
	DMA_ApplicationHalfTransferCompleteCallback(pDMAh);
}

static void dma_TransferErrorCallback(DMA_Handle_t *pDMAh){
	dma_ClearInterruptFlag(pDMAh, DMA_TransferErrorFlag);
	pDMAh->State = DMA_STATE_READY;
	DMA_ApplicationTransferErrorCallback(pDMAh);
}

/******************************************************************************************
 *		 					Flags control APIs
 ******************************************************************************************/

uint8_t DMA_CheckTransferCompleteFlag(DMA_Handle_t *pDMAh){
	uint8_t interruptSource;
	if(pDMAh->DMAChannel->CCR & DMA_CCR_TCIE){
		interruptSource  = dma_GetTransferCompleteFlagPos(pDMAh->channelNumber);
		if(pDMAh->DMAx->ISR & (1 << interruptSource)){
			return ENABLE;
		}
	}
	return DISABLE;
}
uint8_t DMA_CheckTransferErrorFlag(DMA_Handle_t *pDMAh){
	uint8_t interruptSource;
	if(pDMAh->DMAChannel->CCR & DMA_CCR_TEIE){
		interruptSource  = dma_GetTransferErrorPos(pDMAh->channelNumber);
		if(pDMAh->DMAx->ISR & (1 << interruptSource)){
			return ENABLE;
		}
	}
	return DISABLE;
}
uint8_t DMA_CheckHalfTransferCompleteFlag(DMA_Handle_t *pDMAh){
	uint8_t interruptSource;
	if(pDMAh->DMAChannel->CCR & DMA_CCR_TEIE){
		interruptSource  = dma_GetHalfTransferFlagPos(pDMAh->channelNumber);
		if(pDMAh->DMAx->ISR & (1 << interruptSource)){
			return ENABLE;
		}
	}
	return DISABLE;
}
uint8_t DMA_GetFlagStatus(DMA_RegDef_t *pDMAx, uint32_t FlagName){
	if(pDMAx->ISR & FlagName){
		return FLAG_SET;
	}
	return FLAG_RESET;
}
inline void DMA_ClearFlag(DMA_RegDef_t *pDMAx, uint32_t FlagName){

	pDMAx->IFCR |= FlagName;
}
inline void DMA_ClearAllFlags(DMA_RegDef_t *pDMAx){
	pDMAx->IFCR = DMA_FLAGS_ALL_MASK;
}

/******************************************************************************************
 *		 					Other Peripheral Control APIs
 ******************************************************************************************/

void DMA_ChannelControl(DMA_Channel_RegDef_t *pDMAx, uint8_t EnableOrDisable){
	if(EnableOrDisable == ENABLE){
		pDMAx->CCR |= ( 1 << DMA_CCR_EN_Pos);
	} else{
		pDMAx->CCR &= ~( 1 << DMA_CCR_EN_Pos);
	}
}

inline void DMA_SPI_Config(uint8_t len, uint8_t dataSize, uint32_t dataRegister, uint32_t pTxBuffer, DMA_Channel_RegDef_t * dmaChannel){

	DMA_Configure_NumberOfDataToTransfer(dmaChannel, len);

	dmaChannel->CMAR = pTxBuffer;

	//DMA_Configure_TransferAdresses(dmaChannel, pTxBuffer, dataRegister);

	//DMA_ChannelControl(dmaChannel, ENABLE);
}

void DMA_USART_Config(uint8_t len, uint32_t dataRegister, uint32_t pTxBuffer, DMA_Channel_RegDef_t * dmaChannel){

	DMA_Configure_NumberOfDataToTransfer(dmaChannel, len);
	dmaChannel->CMAR = pTxBuffer;
}
/******************************************************************************************
 *		 					Weak application callbacks
 ******************************************************************************************/

__weak void DMA_ApplicationTransferCompleteCallback(DMA_Handle_t *pDMAh){

	//This is a weak implementation. The application may override this function
}
__weak void DMA_ApplicationHalfTransferCompleteCallback(DMA_Handle_t *pDMAh){

	//This is a weak implementation. The application may override this function
}
__weak void DMA_ApplicationTransferErrorCallback(DMA_Handle_t *pDMAh){

	//This is a weak implementation. The application may override this function
}

/******************************************************************************************
 *		 					Static helper functions
 ******************************************************************************************/

static inline uint8_t dma_GetTransferCompleteFlagPos(uint8_t channelNumber){

	return (DMA_FLAG_TRANSFER_COMPLETE_POS + ((channelNumber-1)*4));
}
static inline uint8_t dma_GetHalfTransferFlagPos(uint8_t channelNumber){

	return (DMA_FLAG_TRANSFER_HALF_COMPLETE_POS + ((channelNumber-1)*4));
}
static inline uint8_t dma_GetTransferErrorPos(uint8_t channelNumber){

	return (DMA_FLAG_TRANSFER_ERROR_POS + ((channelNumber-1)*4));
}
static inline void dma_ClearInterruptFlag(DMA_Handle_t *pDMAh, DMA_FlagTypedef dmaFlagType){
	uint32_t flagType = (uint32_t)dmaFlagType;
	uint32_t flagShift = (pDMAh->channelNumber-1)*(DMA_NUMBER_OF_FLAGS);
	uint32_t flagToClear = ( flagType << flagShift);
	DMA_ClearFlag(pDMAh->DMAx, flagToClear);
}



