/*
 * stm32f303xx_i2c.c
 *
 *  Created on: 13 kwi 2020
 *      Author: marci
 */

#include "stm32f303xx_i2c.h"

typedef enum{
	I2C_WRITE,
	I2C_READ
}I2C_Transfer_direction_t;

static void I2C_GenerateStartCondition(I2C_RegDef_t *pI2Cx);
void I2C_GenerateStopCondition(I2C_RegDef_t *pI2Cx);
static void I2C_ConfigAddressPhase(I2C_RegDef_t *pI2Cx, uint8_t address, I2C_Transfer_direction_t direction);
static void I2C_SetNumberOfBytesToBeTransfered(I2C_RegDef_t *pI2Cx, uint32_t numberOfBytes);
static uint8_t I2C_CheckAutoEnd(I2C_RegDef_t *pI2C);
/*
 * Peripheral Clock setup
 */
void I2C_PeriphClockControl(I2C_RegDef_t *pI2Cx, uint8_t EnorDi){

	if(EnorDi == ENABLE){
		if(pI2Cx == I2C1){
			I2C1_PCLK_EN();
		}else if(pI2Cx == I2C2){
			I2C2_PCLK_EN();
		}else if(pI2Cx == I2C3){
			I2C3_PCLK_EN();
		}
	}else{
		if(pI2Cx == I2C1){
			I2C1_PCLK_DI();
		}else if(pI2Cx == I2C2){
			I2C2_PCLK_DI();
		}else if(pI2Cx == I2C3){
			I2C3_PCLK_DI();
		}
	}
}

/*
 * Init and De-init
 */
void I2C_MasterInit(I2C_Handle_t *pI2CHandle){

	I2C_Stop(pI2CHandle->pI2Cx);

	I2C_Config_AnalogFilter(pI2CHandle->pI2Cx, pI2CHandle->I2C_Config.I2C_AnalogFilter);

	I2C_Config_DigitalFilter(pI2CHandle->pI2Cx, pI2CHandle->I2C_Config.I2C_DigitalFilter);

	I2C_Config_Autoend(pI2CHandle->pI2Cx, pI2CHandle->I2C_Config.I2C_AutoEnd);

	I2C_Config_AddressingMode(pI2CHandle->pI2Cx, pI2CHandle->I2C_Config.I2C_AddressingMode);

	I2C_Config_Timings(pI2CHandle->pI2Cx, pI2CHandle->I2C_Config.I2C_Mode);

	I2C_Start(pI2CHandle->pI2Cx);
}

void I2C_DeInit(I2C_RegDef_t *pI2Cx){

	if(pI2Cx == I2C1){
		I2C1_REG_RESET();
	}else if(pI2Cx == I2C2){
		I2C2_REG_RESET();
	}else if(pI2Cx == I2C3){
		I2C3_REG_RESET();
	}

}

/*
 * I2C Configuration
 */

inline void I2C_Start(I2C_RegDef_t *pI2Cx){

	pI2Cx->CR1 |= I2C_CR1_PE;
}

inline void I2C_Stop(I2C_RegDef_t *pI2Cx){

	pI2Cx->CR1 &= ~( I2C_CR1_PE );
}

inline void I2C_Config_Autoend(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable){

	// By default autoend feature is diasbled
	pI2Cx->CR2 &= ~( I2C_CR2_AUTOEND );

	if( EnableOrDisable == ENABLE){
		pI2Cx->CR2 |= I2C_CR2_AUTOEND;
	}
}

inline void I2C_Config_AddressingMode(I2C_RegDef_t *pI2Cx, uint8_t addressingMode){

	// By default addressing mode is set to 7 bit mode

	pI2Cx->CR2 &= ~( I2C_CR2_ADD10 );

	if(addressingMode == I2C_ADDRESSING_MODE_10BIT){

		pI2Cx->CR2 |= I2C_CR2_ADD10;
	}
}

inline void I2C_Config_AnalogFilter(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable){

	// By default analog filter is enabled (ANOFF bit is cleared)
	pI2Cx->CR1 &= ~( I2C_CR1_ANFOFF );

	if(EnableOrDisable == DISABLE){

		pI2Cx->CR1 |= I2C_CR1_ANFOFF;
	}
}

/*	@fn - I2C_Config_DigitalFilter									*//*
 *
 * 	@param[in] clockCycles - Can be the value between 0 and 15
 * 	This number defines filtering capability from 0up to 15 i2c clk.
 *
 * 	@Note: Changing filter configuration is not
 * 	 allowed while i2c is enabled.
 * 																	*//*
 */
inline void I2C_Config_DigitalFilter(I2C_RegDef_t *pI2Cx, uint8_t clockCycles){

	pI2Cx->CR1 |= clockCycles << I2C_CR1_DNF_Pos;
}

void I2C_Config_Timings(I2C_RegDef_t *pI2Cx, uint8_t i2cMode){

	uint32_t timmingRegValue;

	if( i2cMode == I2C_MODE_FAST_PLUS){
		timmingRegValue = I2C_TIMING_REG_FAST_MODE_PLUS;

	}else if( i2cMode == I2C_MODE_FAST){
		timmingRegValue = I2C_TIMING_REG_FAST_MODE;

	}else{
		timmingRegValue = I2C_TIMING_REG_STANDARD_MODE;
	}

	pI2Cx->TIMINGR = timmingRegValue;
}

inline void I2C_Config_Stretching(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable){

	// By default clock stretching is enabled (NOSTRETCH bit is cleared)
	pI2Cx->CR1 &= ~( I2C_CR1_NOSTRETCH );

	if( EnableOrDisable == DISABLE){

		pI2Cx->CR1 |= I2C_CR1_NOSTRETCH;

	}
}


/*
 * Data Send and Receive
 */

void I2C_MasterSendData(I2C_Handle_t *pI2CHandle,uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddr){

	// Configure Address Phase
	I2C_ConfigAddressPhase(pI2CHandle->pI2Cx, SlaveAddr, I2C_WRITE);

	// Set number of bytes to be transfered
	I2C_SetNumberOfBytesToBeTransfered(pI2CHandle->pI2Cx, Len);

	// Generate Start condition
	I2C_GenerateStartCondition(pI2CHandle->pI2Cx);

	// if NACK flag set there is no slave on bus, return from function
	if( I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_NACK) ){

		return;
	}

	// Send data if TXE flag is set

	while( Len > 0){

		while( ! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_TXIS) );
		pI2CHandle->pI2Cx->TXDR = *pTxbuffer;
		pTxbuffer++;
		Len--;
	}

	// AutoEnd feature let to left this function now
	// (Stop condition is sent automatically)
	// If AutoEnd bit is not set STOP must be generate manually
	if( ! I2C_CheckAutoEnd(pI2CHandle->pI2Cx) ){
		// Wait for data t be send by checking the Transfer Complete flag
		while( ! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_TC) );
		I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
	}
}


void I2C_MasterReceiveData(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer, uint8_t Len, uint8_t SlaveAddr){

	// Configure Address Phase
	I2C_ConfigAddressPhase(pI2CHandle->pI2Cx, SlaveAddr, I2C_READ);

	// Set number of bytes to be transfered
	I2C_SetNumberOfBytesToBeTransfered(pI2CHandle->pI2Cx, Len);

	// Generate Start condition
	I2C_GenerateStartCondition(pI2CHandle->pI2Cx);

	// if NACK flag set there is no slave on bus, return from function
	if( I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_NACK) ){

		return;
	}

	// read data if RXNE flag is set

	while( Len > 0 ){

		while( ! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_RXNE) );
		*pRxBuffer = pI2CHandle->pI2Cx->RXDR;
		pRxBuffer++;
		Len--;
	}

	// AutoEnd feature let to left this function now
	// (Stop condition is sent automatically)
	// If AutoEnd bit is not set STOP must be generate manually
	if( ! I2C_CheckAutoEnd(pI2CHandle->pI2Cx) ){
		// Wait for data t be send by checking the Transfer Complete flag
		while( ! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_FLAG_TC) );
		I2C_GenerateStopCondition(pI2CHandle->pI2Cx);
	}

}

uint8_t I2C_MasterSendDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddr);
uint8_t I2C_MasterReceiveDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer, uint8_t Len, uint8_t SlaveAddr);

void I2C_CloseReceiveData(I2C_Handle_t *pI2CHandle);
void I2C_CloseSendData(I2C_Handle_t *pI2CHandle);


void I2C_SlaveSendData(I2C_RegDef_t *pI2C,uint8_t data);
uint8_t I2C_SlaveReceiveData(I2C_RegDef_t *pI2C);

/*
 * IRQ Configuration and ISR handling
 */

void I2C_EV_IRQHandling(I2C_Handle_t *pI2CHandle);
void I2C_ER_IRQHandling(I2C_Handle_t *pI2CHandle);


/*
 * Other Peripheral Control APIs
 */
void I2C_PeripheralControl(I2C_RegDef_t *pI2Cx, uint8_t EnOrDi);

uint8_t I2C_GetFlagStatus(I2C_RegDef_t *pI2Cx , uint32_t FlagName){

	if(pI2Cx->ISR & FlagName){
		return FLAG_SET;
	}
	return FLAG_RESET;
}

void I2C_ManageAcking(I2C_RegDef_t *pI2Cx, uint8_t EnorDi);
void I2C_GenerateStopCondition(I2C_RegDef_t *pI2Cx);

void I2C_SlaveEnableDisableCallbackEvents(I2C_RegDef_t *pI2Cx,uint8_t EnorDi);

/*
 * Application callback
 */
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandle,uint8_t AppEv);

/*
 * Static helper functions
 */

static inline void I2C_GenerateStartCondition(I2C_RegDef_t *pI2Cx){

	pI2Cx->CR2 |= (1 << I2C_CR2_START_Pos);
}

inline void I2C_GenerateStopCondition(I2C_RegDef_t *pI2Cx){

	pI2Cx->CR2 |= (1 << I2C_CR2_STOP_Pos);
}

static void I2C_ConfigAddressPhase(I2C_RegDef_t *pI2Cx, uint8_t address, I2C_Transfer_direction_t direction){

	// set address
	address = address;
	pI2Cx->CR2 &= ~(I2C_CR2_SADD_Msk); //clear slave address space
	pI2Cx->CR2 |= (address << 1) << (I2C_CR2_SADD_Pos);

	// set transfer direction (data read or data write)
	// By default request a write transfer
	pI2Cx->CR2 &= ~(I2C_CR2_RD_WRN_Msk);

	if( I2C_READ == direction ){
		pI2Cx->CR2 |= (1 << I2C_CR2_RD_WRN_Pos);
	}
}

static void I2C_SetNumberOfBytesToBeTransfered(I2C_RegDef_t *pI2Cx, uint32_t numberOfBytes){

	pI2Cx->CR2 &= ~(I2C_CR2_NBYTES_Msk);

	if(numberOfBytes > 255){
		pI2Cx->CR2 |= 0xFF;
	} else{
		pI2Cx->CR2 |= numberOfBytes << I2C_CR2_NBYTES_Pos;
	}
}

static inline uint8_t I2C_CheckAutoEnd(I2C_RegDef_t *pI2C){

	return ( (pI2C->CR2 & I2C_CR2_AUTOEND_Msk) >> I2C_CR2_AUTOEND_Pos );

}

