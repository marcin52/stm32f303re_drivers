/*
 * stm32f303xx_spi.h
 *
 *  Created on: 5 kwi 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_SPI_H_
#define INC_STM32F303XX_SPI_H_

#include "stm32f303xx.h"



/*
 *  Configuration structure for SPIx peripheral
 */
typedef struct
{
	uint8_t SPI_DeviceMode;
	uint8_t SPI_BusConfig;
	uint8_t SPI_SclkSpeed;
	uint8_t SPI_DataSize;
	uint8_t SPI_CPOL;
	uint8_t SPI_CPHA;
	uint8_t SPI_SSM;
}SPI_Config_t;

/*
 *Handle structure for SPIx peripheral
 */

typedef struct
{
	SPI_RegDef_t 	*pSPIx;  	/*!< This holds the base address of SPIx(x:1,2,3,4) peripheral >*/
	SPI_Config_t 	SPIConfig;
	uint8_t 		*pTxBuffer; /* !< To store the app. Tx buffer address 	> */
	uint8_t 		*pRxBuffer;	/* !< To store the app. Rx buffer address	> */
	uint32_t 		TxLen;		/* !< To store Tx len 						> */
	uint32_t 		RxLen;		/* !< To store Tx len 						> */
	uint8_t 		TxState;	/* !< To store Tx state						> */
	uint8_t 		RxState;	/* !< To store Rx state 					> */

}SPI_Handle_t;

/*
 * @SPI_DeviceMode
 */

#define SPI_DEVICE_MODE_MASTER					1
#define SPI_DEVICE_MODE_SLAVE					0

/*
 * @SPI_BusConfig
 */

#define SPI_BUS_FULLDUPLEX						1
#define SPI_BUS_HALFDUPLEX						2
#define SPI_BUS_SIMPLEX_TXONLY					3
#define SPI_BUS_SIMPLEX_RXONLY					4

/*
 * @SPI_SclkSpeed
 */

#define SPI_SPEED_DIV2							0
#define SPI_SPEED_DIV4							1
#define SPI_SPEED_DIV8							2
#define SPI_SPEED_DIV16							3
#define SPI_SPEED_DIV32							4
#define SPI_SPEED_DIV64							5
#define SPI_SPEED_DIV128						6
#define SPI_SPEED_DIV256						7

/*
 * @SPI_DFF
 */

#define SPI_DFF_8BITS							7
#define SPI_DFF_16BITS							15

/*
 * @CPOL
 */

#define SPI_CPOL_HIGH							1
#define SPI_CPOL_LOW							0

/*
 * @CPHA
 */

#define SPI_CPHA_HIGH							1
#define SPI_CPHA_LOW							0

/*
 * @SPI_SSM
 */

#define SPI_SSM_EN								1
#define SPI_SSM_DI								0

/*
 * SPI related status flag definitions
 */

#define SPI_RXNE_FLAG			( 1 << SPI_SR_RXNE_Pos)
#define SPI_TXE_FLAG			( 1 << SPI_SR_TXE_Pos)
#define SPI_CHSIDE_FLAG			( 1 << SPI_SR_CHSIDE_Pos)
#define SPI_UDR_FLAG			( 1 << SPI_SR_UDR_Pos)
#define SPI_CRCERR_FLAG			( 1 << SPI_SR_CRCERR_Pos)
#define SPI_MODF_FLAG			( 1 << SPI_SR_MODF_Pos)
#define SPI_OVR_FLAG			( 1 << SPI_SR_OVR_Pos)
#define SPI_BSY_FLAG			( 1 << SPI_SR_BSY_Pos)
#define SPI_FRE_FLAG			( 1 << SPI_SR_FRE_Pos)

/*
 *  SPI possible states
 */
#define SPI_READY				0
#define SPI_BUSY_IN_RX			1
#define SPI_BUSY_IN_TX			2

/*
 * Possible SPI application events
 */
#define SPI_EVENT_TX_CMPLT		1
#define SPI_EVENT_RX_CMPLT		2
#define SPI_EVENT_OVR_ERR		3
#define SPI_EVENT_CRC_ERR		4


/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/

/*
 * Clock Enable Macros for SPI peripherals
 */

#define SPI4_PCLK_EN()		(RCC->APB2ENR |= (1 << 15))
#define SPI3_PCLK_EN()		(RCC->APB1ENR |= (1 << 15))
#define SPI2_PCLK_EN()		(RCC->APB1ENR |= (1 << 14))
#define SPI1_PCLK_EN()		(RCC->APB2ENR |= (1 << 12))

/*
 * Clock Disable Macros for SPI peripherals
 */

#define SPI4_PCLK_DI()		(RCC->APB2ENR &= ~(1 << 15))
#define SPI3_PCLK_DI()		(RCC->APB1ENR &= ~(1 << 15))
#define SPI2_PCLK_DI()		(RCC->APB1ENR &= ~(1 << 14))
#define SPI1_PCLK_DI()		(RCC->APB2ENR &= ~(1 << 12))

/*
 * Macros to reset SPIx peripheral
 */
#define SPI1_REG_RESET()				do{(RCC->APB2RSTR |= (1 << 12));	(RCC->APB2RSTR &= ~(1 << 12)); }while(0)
#define SPI2_REG_RESET()				do{(RCC->APB1RSTR |= (1 << 14));	(RCC->APB1RSTR &= ~(1 << 14)); }while(0)
#define SPI3_REG_RESET()				do{(RCC->APB1RSTR |= (1 << 15));	(RCC->APB1RSTR &= ~(1 << 15)); }while(0)
#define SPI4_REG_RESET()				do{(RCC->APB2RSTR |= (1 << 15));	(RCC->APB2RSTR &= ~(1 << 15)); }while(0)

/*
 * Some generic macros
 */

#define SPI_GET_CORRESPONDING_DMA(x)					((x == SPI1)?DMA1:\
														(x == SPI2)?DMA1:\
														(x == SPI3)?DMA2:\
														(x == SPI4)?DMA2:0)

#define SPI_CHECK_IF_16BIT_FRAME_FORMAT(SPI)			SPI->CR2 & (SPI_DFF_16BITS << SPI_CR2_DS_Pos)

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/

/*
 * Init and Deinit spi
 */
void SPI_PeriphClockControl(SPI_RegDef_t *pSPIx, uint8_t ENorDI);
void SPI_Init(SPI_Handle_t *pSPIh);
void SPI_DeInit(SPI_RegDef_t *pSPIx);

/*
 * SPI configuration function
 */

void SPI_ConfigureCPHA(SPI_RegDef_t *pSPIx, uint8_t spiCPHA);
void SPI_ConfigureCPOL(SPI_RegDef_t *pSPIx, uint8_t spiCHPA);
void SPI_ConfigureDataSize(SPI_RegDef_t *pSPIx, uint8_t dataSize);
void SPI_ConfigureSpeed(SPI_RegDef_t *pSPIx, uint8_t spiSpeed);
void SPI_ConfigureSoftwareSlaveManagement(SPI_RegDef_t *pSPIx, uint8_t spiSSM);
void SPI_ConfigureDeviceMode(SPI_RegDef_t *pSPIx, uint8_t deviceMode);
void SPI_ConfigureBus(SPI_RegDef_t *pSPIx, uint8_t busConfig);

/*
 * Data send and receive in blocking mode
 */

void SPI_SendData(SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t len);
void SPI_Send16bits(SPI_RegDef_t *pSPIx, uint16_t data);
void SPI_Send8bits(SPI_RegDef_t *pSPIx, uint8_t data);
void SPI_ReceiveData(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t len);
void SPI_Recevive8bits(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer);
void SPI_Recevive16bits(SPI_RegDef_t *pSPIx, uint16_t *pRxBuffer);

/*
 * Data send and receive in interrupt mode
 */

uint8_t SPI_SendDataIT(SPI_Handle_t *pSPIh, uint8_t *pTxBuffer, uint32_t len);
uint8_t SPI_ReceiveDataIT(SPI_Handle_t *pSPIh, uint8_t *pRxBuffer, uint32_t len);

/*
 * Data send and receive in dma mode
 */
void SPI_DMA_SendingControl(SPI_RegDef_t *pSPIx, uint8_t enableOrDisable);
void SPI_DMA_ReceivingControl(SPI_RegDef_t *pSPIx, uint8_t enableOrDisable);
void SPI_StartSendingDataDMA(SPI_Handle_t *pSPIh, DMA_Channel_RegDef_t * dmaChannel);
void SPI_StartReceivingDataDMA(SPI_Handle_t *pSPIh, DMA_Channel_RegDef_t * dmaChannel);
void SPI_SendDataDMA(SPI_Handle_t *pSPIh, uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel);
void SPI_ReceiveDataDMA(SPI_Handle_t *pSPIh, uint8_t *pRxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel);

/*
 * IRQ Configuration and ISR handling
 */

void SPI_IRQHandling(SPI_Handle_t *pSPIh);

/*
 *  Other Peripheral Control APIs
 */

void SPI_PeripheralControl(SPI_RegDef_t *pSPIx, uint8_t ENorDI);
void SPI_SSIConfig(SPI_RegDef_t *pSPIx, uint8_t ENorDI);
uint8_t SPI_GetFlagStatus(SPI_RegDef_t *pSPIx, uint32_t FlagName);
void SPI_SSOEConfig(SPI_RegDef_t *pSPIx, uint8_t ENorDI);
void SPI_CLearOVRFlag(SPI_RegDef_t *pSPIx);
void SPI_CloseTransmission(SPI_Handle_t *pSPIh);
void SPI_CloseReception(SPI_Handle_t *pSPIh);

/*
 * Weak application callbacks
 */

void SPI_ApplicationEventCallback(SPI_Handle_t *pSPIh, uint8_t AppEv);

#endif /* INC_STM32F303XX_SPI_H_ */

