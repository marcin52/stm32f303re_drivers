/*
 * stm32f303xx_rtc.h
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_RTC_H_
#define INC_STM32F303XX_RTC_H_

#include "stm32f303xx.h"
#include "stm32f303cc_rcc.h"
#include "stm32f303xx_pwr.h"

/******************************************************************************************
 *								Definitions supported by this driver
 *
 ******************************************************************************************/
#define RTC_TR_RESERVED_MASK    0x007F7F7FU
#define RTC_DR_RESERVED_MASK    	0x00FFFF3FU

typedef enum{
	RTC_ALARM_A,
	RTC_ALARM_B
}RTC_alarms_t;

typedef enum{
	RTC_FLAG_ALARM_A = 1 << 8,
	RTC_FLAG_ALARM_B = 1 << 9,
	RTC_FLAG_WAKEUP = 1 << 10
}RTC_flags_t;

typedef enum{
	RTC_EVENT_ALARM = 1 << 17,
	RTC_EVENT_TAMPER_AND_TIMESTAMP = 1 << 19,
	RTC_EVENT_WAKEUP_TIMER = 1 << 20
}RTC_events_t;

typedef enum{
	RTC_24_HOUR_FORMAT,
	RTC_AM_PM_HOUR_FORMAT
}RTC_HourFormat_t;

typedef enum{
	RTC_HOUR_AM,
	RTC_HOUR_PM
}RTC_HOUR_AM_PM_t;

typedef struct{
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
	RTC_HourFormat_t hourFormat;
	RTC_HOUR_AM_PM_t am_or_pm;
}RTC_time_t;

typedef struct{
	uint8_t day;
	uint8_t weekDay;
	uint8_t month;
	uint16_t year;
}RTC_date_t;

typedef struct{
	uint8_t dateMaskEnable;
	uint8_t weekDayMaskEnable;
	uint8_t hoursMaskEnable;
	uint8_t minutesMaskEnable;
	uint8_t secondsMaskEnable;
	RTC_date_t date;
	RTC_time_t time;
}RTC_alarm_t;

typedef enum{
	RTC_clock_div_16,
	RTC_clock_div_8,
	RTC_clock_div_4,
	RTC_clock_div_2,
	RTC_ck_spre,
	RTC_ck_spre_and_add_2_to_power_of_16
}RTC_wakeup_clock_selection_t;


/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/

/*
 * Clock Enable Macro for RTC peripheral
 */

#define RTC_PCLK_EN()		(RCC->BDCR |= (1 << RCC_BDCR_RTCEN_Pos))

/*
 * Clock Disable Macro for RTC peripheral
 */

#define RTC_PCLK_DI()		(RCC->BDCR &= ~(1 << RCC_BDCR_RTCEN_Pos))

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/

/*
 * Init and Deinit RTC
 */

void RTC_PeriphClockControl(uint8_t EnableOrDisable);
void RTC_StartInitMode();
void RTC_StopInitMode();
void RTC_ProgramPrescalers(uint8_t APrescaler, uint16_t SPrescaler);
void RTC_Config_HourFormat(RTC_HourFormat_t HourFormat);

/*
 * Seting time and date
 */
void RTC_Set_AM_PM(RTC_HOUR_AM_PM_t AM_or_PM);
void RTC_SetTime(RTC_time_t * time);
void RTC_SetDate(RTC_date_t * date);
void RTC_GetTime(RTC_time_t * time);
void RTC_GetDate(RTC_date_t * date);
/*
 * Alarms control
 */
void RTC_SetAlarm(RTC_alarm_t * alarmConfig, RTC_alarms_t alarm);
void RTC_AlarmA_Control(uint8_t enableOrDisable);
void RTC_AlarmB_Control(uint8_t enableOrDisable);
void RTC_AlarmAinterrupt_Control(uint8_t enableOrDisable);
void RTC_AlarmBinterrupt_Control(uint8_t enableOrDisable);

/*
 * Wakeup timer control
 */
void RTC_WakeupTimerInit(RTC_wakeup_clock_selection_t RTC_clock, uint16_t reloadValue);
void RTC_WakeupTimerAinterrupt_Control(uint8_t enableOrDisable);
void RTC_WakeupTimerControl(uint8_t EnableOrDisable);
void RTC_SelectWakeupTimerClock(RTC_wakeup_clock_selection_t RTC_clock);
void RTC_ProgramWakeupTimerReload(uint16_t reloadValue);
/*
 * Write protection control
 */
void RTC_BackUpDomainResetWriteProtectionControl(uint8_t EnableOrDisable);

/*
 * RTC IQR handling
 */
void RTC_IRQHandler();
void RTC_ClearEXTI_InterruptPendingFlag(RTC_events_t EVENT);

/*
 * Other APIs
 */
uint8_t RTC_CheckFlag(RTC_flags_t flag);
void RTC_ClearFlag(RTC_flags_t flag);

/*
 * application callbacks
 */

__weak void RTC_AlarmA_Callback();
__weak void RTC_AlarmB_Callback();
__weak void RTC_WakeUp_Callback();

#endif /* INC_STM32F303XX_RTC_H_ */
