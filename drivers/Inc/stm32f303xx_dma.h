/*
 * stm32f303xx_dma.h
 *
 *  Created on: 29 kwi 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_DMA_H_
#define INC_STM32F303XX_DMA_H_

#include "stm32f303xx.h"

typedef enum{
	DMA_STATE_BUSY,
	DMA_STATE_READY,
	DMA_STATE_RESET,
}DMA_State_t;

typedef struct{
	 uint8_t transferComplete;					/*! Possible values: ENABLE, DISABLE	 */
	 uint8_t transferHalfComplete;				/*! Possible values: ENABLE, DISABLE	 */
	 uint8_t transferError;						/*! Possible values: ENABLE, DISABLE	 */
}DMA_InterruptsEnableControl;

typedef struct{
	uint32_t memoryAddress;						/*! Address to some register			 */
	uint32_t peripheralAddress;					/*! Address to some register			 */
	uint16_t numberOfDataToTransfer;			/*! Number from 0x1 to 0xFFFF			 */
}DMA_TransferConfig_t;

typedef struct{
	uint8_t memoryDataSize;						/*! Refer to @DMA_DataSize 				 */
	uint8_t peripheralDataSize;					/*! Refer to @DMA_DataSize 		 		 */
	uint8_t memoryIncrementMode;				/*! Possible values: ENABLE, DISABLE	 */
	uint8_t peripheralIncrementMode;			/*! Possible values: ENABLE, DISABLE	 */
	uint8_t direction;							/*! Refer to @DMA_Direction				 */
	uint8_t dmaChannelPriority;					/*! Refer to @DMA_ChannelPriority	 	 */
	uint8_t transferMode;						/*! Refer to @DMA_Mode	 			 	 */
}DMA_ChannelConfig_t;

typedef struct{
	DMA_RegDef_t *DMAx;
	DMA_Channel_RegDef_t *DMAChannel;
	uint8_t channelNumber;
	DMA_ChannelConfig_t DMA_Config;
	DMA_State_t State;
	DMA_InterruptsEnableControl dmaInterruptsEnable;
	DMA_TransferConfig_t transferConfig;
}DMA_Handle_t;

/*
 * @DMA_DataSize
 */
#define DMA_DATA_SIZE_8BIT						0
#define DMA_DATA_SIZE_16BIT						1
#define DMA_DATA_SIZE_32BIT						2

/*
 * @DMA_ChannelPriority
 */
#define DMA_CHANNEL_PRIORITY_LOW				0
#define DMA_CHANNEL_PRIORITY_MEDIUM				1
#define DMA_CHANNEL_PRIORITY_HIGH				2
#define DMA_CHANNEL_PRIORITY_VERY_HIGH			3

/*
 * @DMA_Mode
 */

#define DMA_MODE_NORMAL							0
#define DMA_MODE_CIRCULAR						1
#define DMA_MODE_MEMORY_TO_MEMORY				2

/*
 * @DMA_Direction
 */

#define DMA_DIRECTION_PRIPHERAL_TO_MEMORY		0
#define DMA_DIRECTION_MEMORY_TO_PERIPHERAL		1

/*
 * @DMA_FlagDefinitions
 */
typedef enum{
	DMA_TransferCompleteFlag = 2,
	DMA_TransferHalfCompleteFlag = 4,
	DMA_TransferErrorFlag = 8,
}DMA_FlagTypedef;

#define DMA_NUMBER_OF_FLAGS						4

#define DMA_FLAG_GLOBAL_INTERRUPT_POS			0
#define DMA_FLAG_TRANSFER_COMPLETE_POS			1
#define DMA_FLAG_TRANSFER_HALF_COMPLETE_POS		2
#define DMA_FLAG_TRANSFER_ERROR_POS				3


#define DMA_FLAG_CHANNEL1_GLOBAL_INTERRUPT		DMA_ISR_GIF1
#define DMA_FLAG_CHANNEL2_GLOBAL_INTERRUPT		DMA_ISR_GIF2
#define DMA_FLAG_CHANNEL3_GLOBAL_INTERRUPT		DMA_ISR_GIF3
#define DMA_FLAG_CHANNEL4_GLOBAL_INTERRUPT		DMA_ISR_GIF4
#define DMA_FLAG_CHANNEL5_GLOBAL_INTERRUPT		DMA_ISR_GIF5
#define DMA_FLAG_CHANNEL6_GLOBAL_INTERRUPT		DMA_ISR_GIF6
#define DMA_FLAG_CHANNEL7_GLOBAL_INTERRUPT		DMA_ISR_GIF7

#define DMA_FLAG_CHANNEL1_TRANSFER_COMPLETE		DMA_ISR_TCIF1
#define DMA_FLAG_CHANNEL2_TRANSFER_COMPLETE		DMA_ISR_TCIF2
#define DMA_FLAG_CHANNEL3_TRANSFER_COMPLETE		DMA_ISR_TCIF3
#define DMA_FLAG_CHANNEL4_TRANSFER_COMPLETE		DMA_ISR_TCIF4
#define DMA_FLAG_CHANNEL5_TRANSFER_COMPLETE		DMA_ISR_TCIF5
#define DMA_FLAG_CHANNEL6_TRANSFER_COMPLETE		DMA_ISR_TCIF6
#define DMA_FLAG_CHANNEL7_TRANSFER_COMPLETE		DMA_ISR_TCIF7


#define DMA_FLAG_CHANNEL1_HALF_TRANSFER			DMA_ISR_HTIF1
#define DMA_FLAG_CHANNEL2_HALF_TRANSFER			DMA_ISR_HTIF2
#define DMA_FLAG_CHANNEL3_HALF_TRANSFER			DMA_ISR_HTIF3
#define DMA_FLAG_CHANNEL4_HALF_TRANSFER			DMA_ISR_HTIF4
#define DMA_FLAG_CHANNEL5_HALF_TRANSFER			DMA_ISR_HTIF5
#define DMA_FLAG_CHANNEL6_HALF_TRANSFER			DMA_ISR_HTIF6
#define DMA_FLAG_CHANNEL7_HALF_TRANSFER			DMA_ISR_HTIF7


#define DMA_FLAG_CHANNEL1_TRANSFER_ERROR		DMA_ISR_TEIF1
#define DMA_FLAG_CHANNEL2_TRANSFER_ERROR		DMA_ISR_TEIF2
#define DMA_FLAG_CHANNEL3_TRANSFER_ERROR		DMA_ISR_TEIF3
#define DMA_FLAG_CHANNEL4_TRANSFER_ERROR		DMA_ISR_TEIF4
#define DMA_FLAG_CHANNEL5_TRANSFER_ERROR		DMA_ISR_TEIF5
#define DMA_FLAG_CHANNEL6_TRANSFER_ERROR		DMA_ISR_TEIF6
#define DMA_FLAG_CHANNEL7_TRANSFER_ERROR		DMA_ISR_TEIF7

#define DMA_FLAGS_ALL_MASK						0x07FFFFFF

/*
 * SPI DMA Channels definitions
 */

#define SPI1_DMA_RX_CHANNEL			DMA1_Channel2
#define SPI1_DMA_TX_CHANNEL			DMA1_Channel3
#define SPI2_DMA_RX_CHANNEL			DMA1_Channel4
#define SPI2_DMA_TX_CHANNEL			DMA1_Channel5
#define SPI3_DMA_RX_CHANNEL			DMA2_Channel1
#define SPI3_DMA_TX_CHANNEL			DMA2_Channel2
#define SPI4_DMA_RX_CHANNEL			DMA2_Channel4
#define SPI4_DMA_TX_CHANNEL			DMA2_Channel5

/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/

/*
 * Clock Enable Macros for DMA peripherals
 */

#define DMA1_PCLK_ENABLE()		(RCC->AHBENR |= (1 << RCC_AHBENR_DMA1EN_Pos))
#define DMA2_PCLK_ENABLE()		(RCC->AHBENR |= (1 << RCC_AHBENR_DMA2EN_Pos ))


/*
 * Clock Disable Macros for DMA peripherals
 */

#define DMA1_PCLK_DISABLE()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_DMA1EN_Pos))
#define DMA2_PCLK_DISABLE()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_DMA2EN_Pos ))

/*
 * TODO
 */
#define DMA_GET_COUNTER(__HANDLE__) ((__HANDLE__)->DMAChannel->CNDTR)

/*
 * Macro to get uint8_t value of the dma channel number
 */

#define GET_DMA_CHANNEL_NUMBER(x)		((x == DMA1_Channel1)?1:\
										(x == DMA1_Channel2)?2:\
										(x == DMA1_Channel3)?3:\
										(x == DMA1_Channel4)?4:\
										(x == DMA1_Channel5)?5:\
										(x == DMA1_Channel6)?6:\
										(x == DMA1_Channel7)?7:\
										(x == DMA2_Channel1)?1:\
										(x == DMA2_Channel2)?2:\
										(x == DMA2_Channel3)?3:\
										(x == DMA2_Channel4)?4:\
										(x == DMA2_Channel5)?5:0)

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/

/*
 * Peripheral Clock setup
 */
void DMA_PeriphClockControl(DMA_RegDef_t *pDMAx, uint8_t EnableOrDisable);

/*
 * Init and Deinit DMA
 */
void DMA_Init(DMA_Handle_t *pDMAh);
void DMA_DeInit(DMA_Handle_t *pDMAh);

/*
 * Channel configuration APIs
 */
void DMA_Configure_TransferAdresses(DMA_Channel_RegDef_t *DMAChannel, uint32_t memoryAddress, uint32_t peripheralAddrerss);
void DMA_Configure_IncrementalMode(DMA_Handle_t *pDMAx, uint8_t memoryIncMode, uint8_t peripheralIncMode);
void DMA_Configure_ChannelPriority(DMA_Handle_t *pDMAx, uint8_t priority);
void DMA_Configure_TransferDataSize(DMA_Channel_RegDef_t *DMAChannel, uint8_t memoryDataSize, uint8_t peripheralDataSize);
void DMA_Configure_NumberOfDataToTransfer(DMA_Channel_RegDef_t *DMAChannel, uint16_t NumberOfData);
void DMA_Configure_TransferMode(DMA_Handle_t *pDMAx, uint8_t dmaMode);
void DMA_Configure_TransferDirection(DMA_Channel_RegDef_t *DMAChannel, uint8_t transferDirection);
void DMA_Configure_Interrupts(DMA_Handle_t *pDMAx, DMA_InterruptsEnableControl dmaInterruptsEnable);

/*
 * APIs to control data transfer
 */
void DMA_StartTransfer(DMA_Handle_t *pDMAh);
void DMA_StartTransfer_IT(DMA_Handle_t *pDMAh);
void DMA_AbortTransfer(DMA_Handle_t *pDMAh);
void DMA_PollForTransferComplete(DMA_Handle_t *pDMAh, uint8_t maxDelay);

/*
 * IRQ Configuration and ISR handling
 */

void DMA_IRQHandling(DMA_Handle_t *pDMAh);

/*
 * Flags control APIs
 */

uint8_t DMA_GetFlagStatus(DMA_RegDef_t *pDMAx, uint32_t FlagName);
void DMA_ClearFlag(DMA_RegDef_t *pDMAx, uint32_t FlagName);
void DMA_ClearAllFlags(DMA_RegDef_t *pDMAx);
uint8_t DMA_GetChannelFlagPosition(DMA_Channel_RegDef_t *pDMAchannel, DMA_FlagTypedef dmaFlagType);
uint8_t DMA_CheckTransferCompleteFlag(DMA_Handle_t *pDMAh);
uint8_t DMA_CheckTransferErrorFlag(DMA_Handle_t *pDMAh);
uint8_t DMA_CheckHalfTransferCompleteFlag(DMA_Handle_t *pDMAh);

/*
 *  Other Peripheral Control APIs
 */

void DMA_ChannelControl(DMA_Channel_RegDef_t *pDMAx, uint8_t EnableOrDisable);
void DMA_SPI_Config(uint8_t len, uint8_t dataSize, uint32_t dataRegister, uint32_t pTxBuffer, DMA_Channel_RegDef_t * dmaChannel);
void DMA_USART_Config(uint8_t len, uint32_t dataRegister, uint32_t pTxBuffer, DMA_Channel_RegDef_t * dmaChannel);
/*
 * Weak application callbacks
 */

__weak void DMA_ApplicationTransferCompleteCallback(DMA_Handle_t *pDMAh);
__weak void DMA_ApplicationHalfTransferCompleteCallback(DMA_Handle_t *pDMAh);
__weak void DMA_ApplicationTransferErrorCallback(DMA_Handle_t *pDMAh);

#endif /* INC_STM32F303XX_DMA_H_ */
