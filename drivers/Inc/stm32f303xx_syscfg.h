/*
 * stm32f303xx_syscfg.h
 *
 *  Created on: Apr 18, 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_SYSCFG_H_
#define INC_STM32F303XX_SYSCFG_H_

#include "stm32f303xx.h"

/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/


/*
 * Clock Enable Macros for SYSCFG peripheral
 */

#define SYSCFG_PCLK_EN()	(RCC->APB2ENR |= (RCC_APB2ENR_SYSCFGEN))

/*
 * Clock Disable Macros for SYSCFG peripheral
 */

#define SYSCFG_PCLK_DI()	(RCC->APB2ENR &= ~(RCC_APB2ENR_SYSCFGEN))


#endif /* INC_STM32F303XX_SYSCFG_H_ */
