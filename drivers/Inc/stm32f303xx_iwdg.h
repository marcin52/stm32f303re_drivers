/*
 * stm32f303xx_iwdg.h
 *
 *  Created on: 29 lis 2020
 *      Author: marci
 */

#include "stm32f303xx.h"

#ifndef INC_STM32F303XX_IWDG_H_
#define INC_STM32F303XX_IWDG_H_

#define IWDG_KEY_ENABLE						0x0000CCCC
#define IWDG_KEY_REGISTER_ACCESS			0x00005555
#define IWDG_KEY_COUNTER_REFRESH			0x0000AAAA

// IWDG Clock prescalers
#define IWDG_PRESCALER_DIV_4				0
#define IWDG_PRESCALER_DIV_8				1
#define IWDG_PRESCALER_DIV_16				2
#define IWDG_PRESCALER_DIV_32				3
#define IWDG_PRESCALER_DIV_64				4
#define IWDG_PRESCALER_DIV_128				5
#define IWDG_PRESCALER_DIV_256				7

// IWDG Flags
#define IWDG_FLAG_WINDOW_VALUE_UPDATE		( 1 << IWDG_SR_WVU_Pos );
#define IWDG_FLAG_RELOAD_VALUE_UPDATE		( 1 << IWDG_SR_RVU_Pos );
#define IWDG_FLAG_PRESCALER_VALUE_UPDATE	( 1 << IWDG_SR_PVU_Pos );

void IWDG_Config(uint16_t reload, uint8_t prescaler);
void IWDG_ConfigWindowMode(uint16_t reload, uint8_t prescaler, uint16_t window );
void IWDG_Refresh();

#endif /* INC_STM32F303XX_IWDG_H_ */
