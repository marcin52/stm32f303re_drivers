/*
 * stm32f303xx_gpio.h
 *
 *  Created on: 2 kwi 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_GPIO_H_
#define INC_STM32F303XX_GPIO_H_

#include "stm32f303xx.h"

typedef struct
{
	GPIO_RegDef_t *pGPIOx;			/*!< This holds the base address of the 	>*/
			 	 	 	 	 	 	/*   GPIO port to which the pins belong	 	>*/

	uint8_t GPIO_PinMode;			/*!< possible values from @GPIO_PIN_MODES   >*/
	uint8_t GPIO_PinSpeed;			/*!< possible values from @GPIO_PIN_SPEED   >*/
	uint8_t GPIO_PinPuPdControl;	/*!< possible values from @GPIO_PIN_PUPD    >*/
	uint8_t GPIO_PinOPType;			/*!< possible values from @GPIO_PIN_OTYPE   >*/
	uint8_t GPIO_PinAltFunMode;		/*!< possible values from @GPIO_PIN_SPEED   >*/

}GPIO_PinConfig_t;


typedef struct{

	uint8_t PinNumber[15];				/*!< possible values from @GPIO_PIN_NUMBERS >*/
										/* note that this is a table of pins		>*/
										/* which may contains max 15 elements		>*/

	uint8_t numberOfPinsToConfig;		/*!< Number in range from 1 to 15			>*/

	GPIO_PinConfig_t GPIO_PinConfig; 	/*!< This holds GPIO configuration settings >*/

}GPIO_Handle_t;

/*
 * @GPIO_PIN_NUMBERS
 * GPIO pin numbers
 */
#define GPIO_PIN_0  				0
#define GPIO_PIN_1  				1
#define GPIO_PIN_2  				2
#define GPIO_PIN_3  				3
#define GPIO_PIN_4  				4
#define GPIO_PIN_5  				5
#define GPIO_PIN_6  				6
#define GPIO_PIN_7  				7
#define GPIO_PIN_8  				8
#define GPIO_PIN_9  				9
#define GPIO_PIN_10  				10
#define GPIO_PIN_11 				11
#define GPIO_PIN_12  				12
#define GPIO_PIN_13 				13
#define GPIO_PIN_14 				14
#define GPIO_PIN_15 				15

/* GPIO_PIN_MODES
 * GPIO pin possible mode
 */

#define GPIO_MODE_IN 							0
#define GPIO_MODE_OUT							1
#define GPIO_MODE_AF							2
#define GPIO_MODE_ANALOG						3
#define GPIO_MODE_IT_FALLING_EDGE				4
#define GPIO_MODE_IT_RAISING_EDGE				5
#define GPIO_MODE_IT_RAISING_FALLING_EDGE		6

/* GPIO_PIN_OTYPE
 * GPIO pin possible output types
 */

#define GPIO_OP_TYPE_PP					0
#define GPIO_OP_TYPE_OD					1

/* GPIO_PIN_SPEED
 * GPIO pin possible output speeds
 */

#define GPIO_SPEED_LOW					0
#define GPIO_SPEED_MEDIUM				1
#define GPIO_SPEED_HIGH					2

/* GPIO_PIN_PUPD
 * GPIO pin possible pull-up, pull-down configuration
 */

#define GPIO_PIN_NO_PULL				0
#define GPIO_PIN_PULL_UP				1
#define GPIO_PIN_PULL_DOWN				2

/******************************************************************************************
 *						Macro functions supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/
/*
 * Clock Enable Macros for GPIOx peripherals
 */

#define GPIOA_PCLK_EN()    	(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOAEN_Pos))
#define GPIOB_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOBEN_Pos))
#define GPIOC_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOCEN_Pos))
#define GPIOD_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIODEN_Pos))
#define GPIOE_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOEEN_Pos))
#define GPIOF_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOFEN_Pos))
#define GPIOG_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOGEN_Pos))
#define GPIOH_PCLK_EN()		(RCC->AHBENR |= (1 << RCC_AHBENR_GPIOHEN_Pos))

/*
 * Clock Disable Macros for GPIOx peripherals
 */

#define GPIOA_PCLK_DI()    	(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOAEN_Pos))
#define GPIOB_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOBEN_Pos))
#define GPIOC_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOCEN_Pos))
#define GPIOD_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIODEN_Pos))
#define GPIOE_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOEEN_Pos))
#define GPIOF_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOFEN_Pos))
#define GPIOG_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOGEN_Pos))
#define GPIOH_PCLK_DI()		(RCC->AHBENR &= ~(1 << RCC_AHBENR_GPIOHEN_Pos))

/*
 * Macros to reset GPIOx peripheral
 */

#define GPIOA_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOARST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOARST_Pos)); }while(0)
#define GPIOB_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOBRST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOBRST_Pos)); }while(0)
#define GPIOC_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOCRST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOCRST_Pos)); }while(0)
#define GPIOD_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIODRST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIODRST_Pos)); }while(0)
#define GPIOE_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOERST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOERST_Pos)); }while(0)
#define GPIOF_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOFRST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOFRST_Pos)); }while(0)
#define GPIOG_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOGRST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOGRST_Pos)); }while(0)
#define GPIOH_REG_RESET()				do{(RCC->AHBRSTR |= (1 << RCC_AHBRSTR_GPIOHRST_Pos));	(RCC->AHBRSTR &= ~(1 << RCC_AHBRSTR_GPIOHRST_Pos)); }while(0)

/*
 * This macro calculates the value to push it into EXTICR
 */

#define GPIO_VALUE_TO_EXTICR(x)			   ((x == GPIOA)?0:\
											(x == GPIOB)?1:\
											(x == GPIOC)?2:\
											(x == GPIOD)?3:\
											(x == GPIOE)?4:\
											(x == GPIOF)?5:\
											(x == GPIOG)?6:\
											(x == GPIOH)?7:0)

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 *		 Note that documentation to function is added only when necessarily
 ******************************************************************************************/

/*
 * Peripheral Clock setup
 */
void GPIO_PeriphClockControl(GPIO_RegDef_t *pGPIOx, uint8_t ENorDI);

/*
 * Init and Deinit GPIO
 */
void GPIO_Init(GPIO_Handle_t *pGPIO);
void GPIO_DeInit(GPIO_RegDef_t *pGPIOx);

/*
 * Data read and write
 */

uint8_t GPIO_ReadPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);
uint16_t GPIO_ReadPort(GPIO_RegDef_t *pGPIOx);
void GPIO_WritePin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t value);
void GPIO_WritePort(GPIO_RegDef_t *pGPIOx, uint16_t value);
void GPIO_TogglePin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);

/*
 * IRQ Configuration and ISR handling
 */
void GPIO_IRQHandling(uint8_t PinNumber);
void GPIO_ClearEXTI_InterruptPendingFlag(uint8_t PinNumber);


#endif /* INC_STM32F303XX_GPIO_H_ */
