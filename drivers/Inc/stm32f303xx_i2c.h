/*
 * stm32f303xx_i2c.h
 *
 *  Created on: 13 kwi 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_I2C_H_
#define INC_STM32F303XX_I2C_H_

#include "stm32f303xx.h"

/*
 * Configuration structure for I2Cx peripheral
 */
typedef struct
{
	uint8_t	 I2C_AnalogFilter;			/* !< Can be the value ENABLE or DISABLE >  */
	uint8_t  I2C_DigitalFilter;			/* !< Can be the value between 0 and 15  >  */
	uint8_t	 I2C_AutoEnd;				/* !< Can be the value ENABLE or DISABLE >  */
	uint8_t  I2C_AddressingMode;		/* !< Value refer to @I2C_AddressingMode >  */
	uint8_t  I2C_Mode;
}I2C_Config_t;

/*
 *Handle structure for I2Cx peripheral
 */
typedef struct
{
	I2C_RegDef_t 	*pI2Cx;
	I2C_Config_t 	I2C_Config;
	uint8_t 		*pTxBuffer; /* !< To store the app. Tx buffer address > */
	uint8_t 		*pRxBuffer;	/* !< To store the app. Rx buffer address > */
	uint32_t 		TxLen;		/* !< To store Tx len > */
	uint32_t 		RxLen;		/* !< To store Tx len > */
	uint8_t 		TxRxState;	/* !< To store Communication state > */
	uint8_t 		DevAddr;	/* !< To store slave/device address > */
    uint32_t        RxSize;		/* !< To store Rx size  > */
    uint8_t         Sr;			/* !< To store repeated start value  > */
}I2C_Handle_t;

/*
 * @I2C_MODE
 */
#define I2C_MODE_STANDARD						0
#define I2C_MODE_FAST							1
#define I2C_MODE_FAST_PLUS						2

/*
 * @I2C_ACKControl
 */
#define I2C_ACK_ENABLE
#define I2C_ACK_DISABLE

/*
 * @I2C_AddressingMode
 */
#define I2C_ADDRESSING_MODE_7BIT				0
#define I2C_ADDRESSING_MODE_10BIT				1
/*
 * @I2C_FMDutyCycle	TODO check it
 */

/*
 * @I2C_Flag_definition
 */
#define I2C_FLAG_TXE                    (1 << I2C_ISR_TXE_Pos)
#define I2C_FLAG_TXIS                   (1 << I2C_ISR_TXIS_Pos)
#define I2C_FLAG_RXNE                   (1 << I2C_ISR_RXNE_Pos)
#define I2C_FLAG_ADDR                   (1 << I2C_ISR_ADDR_Pos)
#define I2C_FLAG_AF                     (1 << I2C_ISR_NACKF_Pos)
#define I2C_FLAG_STOPF                  (1 << I2C_ISR_STOPF_Pos)
#define I2C_FLAG_TC                     (1 << I2C_ISR_TC_Pos)
#define I2C_FLAG_TCR                    (1 << I2C_ISR_TCR_Pos)
#define I2C_FLAG_BERR                   (1 << I2C_ISR_BERR_Pos)
#define I2C_FLAG_ARLO                   (1 << I2C_ISR_ARLO_Pos)
#define I2C_FLAG_OVR                    (1 << I2C_ISR_OVR_Pos)
#define I2C_FLAG_PECERR                 (1 << I2C_ISR_PECERR_Pos)
#define I2C_FLAG_TIMEOUT                (1 << I2C_ISR_TIMEOUT_Pos)
#define I2C_FLAG_ALERT                  (1 << I2C_ISR_ALERT_Pos)
#define I2C_FLAG_BUSY                   (1 << I2C_ISR_BUSY_Pos)
#define I2C_FLAG_DIR                    (1 << I2C_ISR_DIR_Pos)
#define I2C_FLAG_NACK					(1 << I2C_ISR_NACKF_Pos)

/*
 * @I2C_TIMMING_REGISTER_VALUES
 * @Note: This values are calculated for following conditions:
 * 		- Analog filter: ON
 * 		- Rise time: 100 ns
 * 		- Fall time: 10 ns
 * 		- Cofficeint of digital filter: 0
 */
#define I2C_TIMING_REG_STANDARD_MODE		0x00201D2BU
#define I2C_TIMING_REG_FAST_MODE			0x00100444U
#define I2C_TIMING_REG_FAST_MODE_PLUS		0x00100048U


/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/

/*
 * Clock Enable Macros for I2C peripherals
 */

#define I2C1_PCLK_EN()					(RCC->APB1ENR |= (1 << 21))
#define I2C2_PCLK_EN()					(RCC->APB1ENR |= (1 << 22))
#define I2C3_PCLK_EN()					(RCC->APB1ENR |= (1 << 30))

/*
 * Clock Disable Macros for I2C peripherals
 */

#define I2C1_PCLK_DI()					(RCC->APB1ENR &= ~(1 << 21))
#define I2C2_PCLK_DI()					(RCC->APB1ENR &= ~(1 << 22))
#define I2C3_PCLK_DI()					(RCC->APB1ENR &= ~(1 << 30))

/*
 * Macros to reset I2Cx peripheral
 */
#define I2C1_REG_RESET()				do{(RCC->APB1RSTR |= (1 << 21));	(RCC->APB1RSTR &= ~(1 << 21)); }while(0)
#define I2C2_REG_RESET()				do{(RCC->APB1RSTR |= (1 << 22));	(RCC->APB1RSTR &= ~(1 << 22)); }while(0)
#define I2C3_REG_RESET()				do{(RCC->APB1RSTR |= (1 << 30));	(RCC->APB1RSTR &= ~(1 << 30)); }while(0)

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/
/*
 * Peripheral Clock setup
 */
void I2C_PeriphClockControl(I2C_RegDef_t *pI2Cx, uint8_t EnorDi);

/*
 * Init and De-init
 */
void I2C_MasterInit(I2C_Handle_t *pI2CHandle);
void I2C_DeInit(I2C_RegDef_t *pI2Cx);

/*
 * I2C Configuration
 */

void I2C_Start(I2C_RegDef_t *pI2Cx);
void I2C_Stop(I2C_RegDef_t *pI2Cx);
void I2C_Config_Autoend(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable);
void I2C_Config_AnalogFilter(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable);
void I2C_Config_DigitalFilter(I2C_RegDef_t *pI2Cx, uint8_t clockCycles);
void I2C_Config_Timings(I2C_RegDef_t *pI2Cx, uint8_t i2cMode);
void I2C_Config_Stretching(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable);
void I2C_Config_Autoend(I2C_RegDef_t *pI2Cx, uint8_t EnableOrDisable);
void I2C_Config_AddressingMode(I2C_RegDef_t *pI2Cx, uint8_t addressingMode);

/*
 * Data Send and Receive
 */
void I2C_MasterSendData(I2C_Handle_t *pI2CHandle,uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddr);
void I2C_MasterReceiveData(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer, uint8_t Len, uint8_t SlaveAddr);
uint8_t I2C_MasterSendDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pTxbuffer, uint32_t Len, uint8_t SlaveAddr);
uint8_t I2C_MasterReceiveDataIT(I2C_Handle_t *pI2CHandle,uint8_t *pRxBuffer, uint8_t Len, uint8_t SlaveAddr);

void I2C_CloseReceiveData(I2C_Handle_t *pI2CHandle);
void I2C_CloseSendData(I2C_Handle_t *pI2CHandle);


void I2C_SlaveSendData(I2C_RegDef_t *pI2C,uint8_t data);
uint8_t I2C_SlaveReceiveData(I2C_RegDef_t *pI2C);

/*
 * IRQ Configuration and ISR handling
 */
void I2C_IRQInterruptConfig(uint8_t IRQNumber, uint8_t EnorDi);
void I2C_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority);
void I2C_EV_IRQHandling(I2C_Handle_t *pI2CHandle);
void I2C_ER_IRQHandling(I2C_Handle_t *pI2CHandle);


/*
 * Other Peripheral Control APIs
 */
void I2C_PeripheralControl(I2C_RegDef_t *pI2Cx, uint8_t EnOrDi);
uint8_t I2C_GetFlagStatus(I2C_RegDef_t *pI2Cx , uint32_t FlagName);
void I2C_ManageAcking(I2C_RegDef_t *pI2Cx, uint8_t EnorDi);
void I2C_GenerateStopCondition(I2C_RegDef_t *pI2Cx);

void I2C_SlaveEnableDisableCallbackEvents(I2C_RegDef_t *pI2Cx,uint8_t EnorDi);

/*
 * Application callback
 */
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandle,uint8_t AppEv);


#endif /* INC_STM32F303XX_I2C_H_ */
