/*
 * stm32f303xx_uart.h
 *
 *  Created on: Apr 18, 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_UART_H_
#define INC_STM32F303XX_UART_H_

#include "stm32f303xx.h"

/******************************************************************************************
 *					    Typedef's implemented by this driver
 *
 ******************************************************************************************/

/*
 * Configuration structure for USARTx peripheral
 */
typedef struct
{
	uint8_t USART_Mode;
	uint32_t USART_Baud;
	uint8_t USART_NoOfStopBits;
	uint8_t USART_WordLength;
	uint8_t USART_ParityControl;
	uint8_t USART_HWFlowControl;
}USART_Config_t;


/*
 * Handle structure for USARTx peripheral
 */
typedef struct
{
	USART_RegDef_t *pUSARTx;
	USART_Config_t   USART_Config;
	uint8_t *pTxBuffer;
	uint8_t *pRxBuffer;
	uint32_t TxLen;
	uint32_t RxLen;
	uint8_t TxBusyState;
	uint8_t RxBusyState;
}USART_Handle_t;

/******************************************************************************************
 *					USART configuration options and other define's
 *
 ******************************************************************************************/

/*
 *@USART_Mode
 *Possible options for USART_Mode
 */
#define USART_MODE_ONLY_TX 	0
#define USART_MODE_ONLY_RX 	1
#define USART_MODE_TXRX  	2

/*
 *@USART_Baud
 *Possible options for USART_Baud
 */
#define USART_STD_BAUD_1200					1200
#define USART_STD_BAUD_2400					400
#define USART_STD_BAUD_9600					9600
#define USART_STD_BAUD_19200 				19200
#define USART_STD_BAUD_38400 				38400
#define USART_STD_BAUD_57600 				57600
#define USART_STD_BAUD_115200 				115200
#define USART_STD_BAUD_230400 				230400
#define USART_STD_BAUD_460800 				460800
#define USART_STD_BAUD_921600 				921600
#define USART_STD_BAUD_2M 					2000000
#define USART_STD_BAUD_3M 					3000000


/*
 *@USART_ParityControl
 *Possible options for USART_ParityControl
 */
#define USART_PARITY_EN_ODD   2
#define USART_PARITY_EN_EVEN  1
#define USART_PARITY_DISABLE   0

/*
 *@USART_WordLength
 *Possible options for USART_WordLength
 */
#define USART_WORDLEN_8BITS  0
#define USART_WORDLEN_9BITS  1
#define USART_WORDLEN_7BITS  2

/*
 *@USART_NoOfStopBits
 *Possible options for USART_NoOfStopBits
 */
#define USART_STOPBITS_1     0
#define USART_STOPBITS_0_5   1
#define USART_STOPBITS_2     2
#define USART_STOPBITS_1_5   3


/*
 *@USART_HWFlowControl
 *Possible options for USART_HWFlowControl
 */
#define USART_HW_FLOW_CTRL_NONE    	0
#define USART_HW_FLOW_CTRL_CTS    	1
#define USART_HW_FLOW_CTRL_RTS    	2
#define USART_HW_FLOW_CTRL_CTS_RTS	3


/*
 * USART flags
 */

#define USART_FLAG_TXE 			( 1 << USART_ISR_TXE_Pos)
#define USART_FLAG_RXNE 		( 1 << USART_ISR_RXNE_Pos)
#define USART_FLAG_TC 			( 1 << USART_ISR_TC_Pos)
#define USART_FLAG_CTS			( 1 << USART_ISR_CTS_Pos)
#define USART_FLAG_IDLE			( 1 << USART_ISR_IDLE_Pos)
#define USART_FLAG_OVERRUN		( 1 << USART_ISR_ORE_Pos)

/*
 * Application states
 */
#define USART_BUSY_IN_RX 			1
#define USART_BUSY_IN_TX 			2
#define USART_READY 				0


#define 	USART_EVENT_TX_CMPLT   	0
#define		USART_EVENT_RX_CMPLT   	1
#define		USART_EVENT_IDLE      	2
#define		USART_EVENT_CTS      	3
#define		USART_EVENT_PE       	4
#define		USART_ERR_FE     		5
#define		USART_ERR_NE    	 	6
#define		USART_ERR_ORE    		7

/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/

/*
 * Clock Enable Macros for USART peripherals
 */
#define USART1_PCLK_EN()	(RCC->APB2ENR |= (1 << 14))
#define USART2_PCLK_EN()	(RCC->APB1ENR |= (1 << 17))
#define USART3_PCLK_EN()	(RCC->APB1ENR |= (1 << 18))
#define UART4_PCLK_EN()		(RCC->APB1ENR |= (1 << 19))
#define UART5_PCLK_EN()		(RCC->APB1ENR |= (1 << 20))


/*
 * Clock Disable Macros for USART peripherals
 */
#define USART1_PCLK_DI()	(RCC->APB2ENR &= ~(1 << 14))
#define USART2_PCLK_DI()	(RCC->APB1ENR &= ~(1 << 17))
#define USART3_PCLK_DI()	(RCC->APB1ENR &= ~(1 << 18))
#define UART4_PCLK_DI()		(RCC->APB1ENR &= ~(1 << 19))
#define UART5_PCLK_DI()		(RCC->APB1ENR &= ~(1 << 20))

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/


/*
 * Peripheral Clock setup
 */
void USART_PeriphClockControl(USART_RegDef_t *pUSARTx, uint8_t EnableOrDisable);

/*
 * Init and De-init
 */
void USART_Init(USART_Handle_t *pUSARTHandle);
void USART_DeInit(USART_Handle_t *pUSARTHandle);

/*
 * Data Send and Receive
 */
void USART_SendData(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer, uint32_t Len);
void  USART_ReceiveData(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer, uint32_t Len);
uint8_t USART_SendDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t Len);
uint8_t USART_ReceiveDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer, uint32_t Len);
uint8_t USART_StartSendingDataDMA(USART_Handle_t *pUSARTHandle, DMA_Channel_RegDef_t * dmaChannel);
uint8_t USART_StartReceivingDataDMA(USART_Handle_t *pUSARTHandle, DMA_Channel_RegDef_t * dmaChannel);
uint8_t USART_SendDataDMA(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel);
uint8_t USART_ReceiveDataDMA(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel);
/*
 * IRQ Configuration and ISR handling
 */
void USART_IRQHandling(USART_Handle_t *pUSARTHandle);

/*
 * Other Peripheral Control APIs
 */

uint8_t USART_GetFlagStatus(USART_RegDef_t *pUSARTx, uint8_t StatusFlagName);
void USART_ClearFlag(USART_RegDef_t *pUSARTx, uint16_t StatusFlagName);
void USART_PeripheralControl(USART_RegDef_t *pUSARTx, uint8_t EnableOrDisable);
void USART_SetBaudRate(USART_RegDef_t *pUSARTx, uint32_t BaudRate);


/*
 * Application Callbacks
 */
void USART_TransferCpltCallback(USART_Handle_t *pUSARTHandle);
void USART_ReadDataRegisterEmptyCallback(USART_Handle_t *pUSARTHandle);
void USART_CTS_EventCallback(USART_Handle_t *pUSARTHandle);
void USART_IdleEventCallback(USART_Handle_t *pUSARTHandle);
void USART_OverrunErrorCallback(USART_Handle_t *pUSARTHandle);
void USART_NoiseErrorCallback(USART_Handle_t *pUSARTHandle);
void USART_FramingErrorCallback(USART_Handle_t *pUSARTHandle);


#endif /* INC_STM32F303XX_UART_H_ */
