################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/Src/stm32f303xx_dma.c \
../drivers/Src/stm32f303xx_flash.c \
../drivers/Src/stm32f303xx_gpio.c \
../drivers/Src/stm32f303xx_i2c.c \
../drivers/Src/stm32f303xx_pwr.c \
../drivers/Src/stm32f303xx_rcc.c \
../drivers/Src/stm32f303xx_rtc.c \
../drivers/Src/stm32f303xx_spi.c \
../drivers/Src/stm32f303xx_system.c \
../drivers/Src/stm32f303xx_tim.c \
../drivers/Src/stm32f303xx_uart.c 

OBJS += \
./drivers/Src/stm32f303xx_dma.o \
./drivers/Src/stm32f303xx_flash.o \
./drivers/Src/stm32f303xx_gpio.o \
./drivers/Src/stm32f303xx_i2c.o \
./drivers/Src/stm32f303xx_pwr.o \
./drivers/Src/stm32f303xx_rcc.o \
./drivers/Src/stm32f303xx_rtc.o \
./drivers/Src/stm32f303xx_spi.o \
./drivers/Src/stm32f303xx_system.o \
./drivers/Src/stm32f303xx_tim.o \
./drivers/Src/stm32f303xx_uart.o 

C_DEPS += \
./drivers/Src/stm32f303xx_dma.d \
./drivers/Src/stm32f303xx_flash.d \
./drivers/Src/stm32f303xx_gpio.d \
./drivers/Src/stm32f303xx_i2c.d \
./drivers/Src/stm32f303xx_pwr.d \
./drivers/Src/stm32f303xx_rcc.d \
./drivers/Src/stm32f303xx_rtc.d \
./drivers/Src/stm32f303xx_spi.d \
./drivers/Src/stm32f303xx_system.d \
./drivers/Src/stm32f303xx_tim.d \
./drivers/Src/stm32f303xx_uart.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/Src/stm32f303xx_dma.o: ../drivers/Src/stm32f303xx_dma.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_dma.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_flash.o: ../drivers/Src/stm32f303xx_flash.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_flash.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_gpio.o: ../drivers/Src/stm32f303xx_gpio.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_gpio.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_i2c.o: ../drivers/Src/stm32f303xx_i2c.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_i2c.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_pwr.o: ../drivers/Src/stm32f303xx_pwr.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_pwr.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_rcc.o: ../drivers/Src/stm32f303xx_rcc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_rcc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_rtc.o: ../drivers/Src/stm32f303xx_rtc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_rtc.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_spi.o: ../drivers/Src/stm32f303xx_spi.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_spi.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_system.o: ../drivers/Src/stm32f303xx_system.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_system.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_tim.o: ../drivers/Src/stm32f303xx_tim.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_tim.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
drivers/Src/stm32f303xx_uart.o: ../drivers/Src/stm32f303xx_uart.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DSTM32 -DSTM32F3 -DDEBUG -DSTM32F303RETx -DNUCLEO_F303RE -c -I../Inc -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/Inc" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Device/ST/STM32F3xx/Include" -I"C:/Users/marci/STM32CubeIDE/workspace_1.3.0/stm32f303xx_drivers/drivers/CMSIS/Include" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"drivers/Src/stm32f303xx_uart.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

