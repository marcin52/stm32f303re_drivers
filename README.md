
Library designed for microcontrollers described in ST Reference Manual RM0316. But please note that at this moment all functions are specificlly handling STM32F303RE registers. Some functions may not be working for other microcontrolles.

TODO list:

1. Add support for all microcontrollers from RM0316
2. Finish I2C driver
3. Add CAN driver

This project is developed for educational purposes and may contain errors.
